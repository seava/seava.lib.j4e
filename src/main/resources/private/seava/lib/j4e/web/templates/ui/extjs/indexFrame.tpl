<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>{{itemSimpleName}} | {{productName}} </title>
	
	<script type="text/javascript">
		__ITEM__ = "{{item}}";
		__LANGUAGE__ = "{{shortLanguage}}";
		__THEME__ = "{{theme}}";
	</script>
	
  <script type="text/javascript">
  {% autoescape false %}
    {{constantsJsFragment}}
  {% endautoescape %} 
  </script>

	<!-- Theme -->
	<link rel="stylesheet" type="text/css"
        href="{{urlUiExtjsThemes}}/{{theme}}/{{theme}}-all.css" />
    <link rel="stylesheet" type="text/css"
        href="{{urlUiExtjsThemes}}/{{theme}}/{{theme}}-custom.css" />    
		
</head>
<body>

  {% include "seava/lib/j4e/web/templates/ui/extjs/_loading_mask" %}

	<script type="text/javascript">
		if (document && document.getElementById("n21-loading-msg")) {
			document.getElementById("n21-loading-msg").innerHTML = "...";
		}
	</script>

  {% if (sysCfg_workingMode == "dev") %} 
       {% include "seava/lib/j4e/web/templates/ui/extjs/_includes_dev" %} 
  {% else %}
       {%  include "seava/lib/j4e/web/templates/ui/extjs/_includes_prod" %} 
  {% endif %}

  {% include "seava/lib/j4e/web/templates/ui/extjs/_params" %}

   {% if sysCfg_workingMode == "dev"  %}
    
    {% for key in frameDependenciesTrl  %}
			   <script type="text/javascript" src="{{key}}"></script>
		{% endfor %}		 
		
		{% for key in frameDependenciesCmp  %}
			   <script type="text/javascript" src="{{key}}"></script>
		{% endfor %}
		
  {% endif %}
 
  {% if sysCfg_workingMode == "prod" %} 
		<script type="text/javascript"
			src="{{ctxpath}}/ui-extjs/frame/{{bundle}}/{{shortLanguage}}/{{item}}.js"></script>
		<script type="text/javascript"
			src="{{ctxpath}}/ui-extjs/frame/{{bundle}}/{{item}}.js"></script>
  {% endif %}

  {% autoescape false %}
    {{ extensions }}
  {% endautoescape %} 

	<script type="text/javascript">
		if (document && document.getElementById("n21-loading-msg")) {
			document.getElementById("n21-loading-msg").innerHTML = Main
					.translate("msg", "initialize")
					+ " {{itemSimpleName}}...";
		}
	</script>

	<script>
		var theFrameInstance = null;
		var __theViewport__ = null;
		
		Ext.onReady(function() {
			if (getApplication().getSession().useFocusManager) {
				Ext.FocusManager.enable(true);
			}
		
    {% include "seava/lib/j4e/web/templates/ui/extjs/_on_ready" %}

		var frameReports = [];

    {% autoescape false %}
      {{ extensionsContent }}
    {% endautoescape %} 

			var cfg = {
				layout : "fit",
				xtype : "container",
				items : [ {
					xtype : "{{itemSimpleName}}",
					border : false,
					_reports_ : frameReports,
					listeners : {
						afterrender : {
							fn : function(p) {
								theFrameInstance = this;
							}
						}
					}
				} ]
			};
			__theViewport__ = new Ext.Viewport(cfg);

			var map = KeyBindings.createFrameKeyMap(theFrameInstance);

		});

    {% include "seava/lib/j4e/web/templates/ui/extjs/_loading_mask_remove" %}
	

	</script>
</body>
</html>