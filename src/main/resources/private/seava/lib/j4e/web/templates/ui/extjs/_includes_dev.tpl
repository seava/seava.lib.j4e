
	<!-- Extjs  -->	 
	
	<script type="text/javascript" src="{{urlUiExtjsLib}}/ext-all-debug.js"></script>

	<!-- Locale -->

	<script type="text/javascript"
		src="{{urlUiExtjsCoreI18n}}/seava/lib/e4e/i18n/{{shortLanguage}}/extjs.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCoreI18n}}/seava/lib/e4e/i18n/{{shortLanguage}}/e4e.js"></script>
	
	<!-- Framework -->

	<script type="text/javascript" src="{{urlUiExtjsCore}}/seava/lib/e4e/js/Main.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/extjs-extend.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/extjs-ux-extend.js"></script>

	<!-- Base -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/NavigationTree.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/TemplateRepository.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/Session.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/Application.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/ApplicationMenu.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/LoginWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/ChangePasswordWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/SelectCompanyWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/FrameInspector.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/UserPreferences.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/Abstract_View.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/DisplayField.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/KeyboardShortcutsWindow.js"></script>		
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/KeyBindings.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/HomePanel.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/FrameNavigatorWithIframe.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/FileUploadWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/WorkflowFormWithHtmlWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/WfAbstractFormWindowExtjs.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/WfStartFormWindowExtjs.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/WfTaskFormWindowExtjs.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/base/WorkflowFormFactory.js"></script>
	
	<!-- asgn -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/asgn/AbstractAsgn.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/asgn/AbstractAsgnGrid.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/asgn/AbstractAsgnUi.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/asgn/AsgnGridBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/asgn/AsgnUiBuilder.js"></script>
	
	<!-- dc -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/DcState.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/AbstractDcController.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/DcActionsFactory.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/DcCommandFactory.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/DcContext.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/FlowContext.js"></script>	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/DcActionsStateManager.js"></script>
	
	<!-- dc-view -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDc_View.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDc_Grid.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDc_Form.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDc_PropGrid.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvGrid.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvEditableGrid.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvEditForm.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvEditPropGrid.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvTree.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvFilterForm.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/AbstractDcvFilterPropGrid.js"></script>
	
	<!-- dc-view-builder -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/DcvFilterFormBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/DcvEditFormBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/DcvFilterPropGridBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/DcvEditPropGridBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/DcvGridBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/view/DcvEditableGridBuilder.js"></script>
	
	<!-- dc-commands -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/AbstractDcCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/AbstractDcAsyncCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/AbstractDcSyncCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcQueryCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcClearQueryCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcEnterQueryCommand.js"></script>	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcNewCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcCopyCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcSaveCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcCancelCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcEditInCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcEditOutCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcDeleteCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcReloadRecCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcReloadPageCommand.js"></script>		
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcPrevRecCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcNextRecCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcRpcDataCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcRpcDataListCommand.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcRpcIdListCommand.js"></script>		
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/command/DcRpcFilterCommand.js"></script>
	
	<!-- dc-tools -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcReport.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcBulkEditWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcImportWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcExportWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcPrintWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcSortWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcFilterWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcGridLayoutWindow.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/dc/tools/DcChartWindow.js"></script>	
	
	<!-- frame -->
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/frame/FrameBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/frame/ActionBuilder.js"></script>
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/frame/FrameButtonStateManager.js"></script>
	
	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/frame/AbstractFrame.js"></script>

	<!-- lov -->

	<script type="text/javascript"
		src="{{urlUiExtjsCore}}/seava/lib/e4e/js/lov/AbstractCombo.js"></script>

	 
