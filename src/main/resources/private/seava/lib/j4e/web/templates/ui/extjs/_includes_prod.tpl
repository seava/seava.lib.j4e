
	<!-- Extjs  -->
	
	<script type="text/javascript" src="${urlUiExtjsLib}/ext-all.js"></script>
	
	<!-- Locale -->
	
	<script type="text/javascript"
		src="${urlUiExtjsCoreI18n}/${shortLanguage}/seava/lib/e4e/i18n/extjs.js"></script>
	<script type="text/javascript"
		src="${urlUiExtjsCoreI18n}/${shortLanguage}/seava/lib/e4e/i18n/e4e.js"></script>
	
	<!-- Framework -->
	
	<script type="text/javascript"
		src="${urlUiExtjsCore}/seava/lib/e4e/js/e4e-all.js"></script>
