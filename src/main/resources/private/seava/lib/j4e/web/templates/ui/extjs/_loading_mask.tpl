	<div id="e4e-loading-mask" style=""></div>
	<div id="e4e-loading">
		<div class="e4e-loading-indicator">
			<span id="e4e-loading-logo-text">{{productName}}</span>
			<br>
			<img src="{{urlUiExtjsThemes}}/{{theme}}/images/_my/loading.gif" width="150"/>
			<br>
			<span id="e4e-loading-msg">Loading...</span>
		</div>
	</div>