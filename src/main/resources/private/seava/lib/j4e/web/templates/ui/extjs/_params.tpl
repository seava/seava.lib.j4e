	<script>
	
		/* product info */
	
		Main.productInfo.name = "{{productName}}";
		Main.productInfo.description = "{{productDescription}}";
		Main.productInfo.version = "{{productVersion}}";
		Main.productInfo.vendor = "{{productVendor}}";
		Main.productInfo.url = "{{productUrl}}";
		Main.logo = "{{logo}}";

		/* application urls */

		Main.urlHost = "{{hostUrl}}";
		Main.urlWeb = "{{urlWeb}}";
    Main.urlData = "{{urlData}}";
    Main.urlUi = "{{urlUi}}";
    Main.urlSession = "{{urlSession}}";
    Main.urlUpload = "{{urlUpload}}";
	  Main.urlDownload = "{{urlDownload}}";
		Main.urlDs = "{{urlDs}}";
		Main.urlAsgn = "{{urlAsgn}}";
		Main.urlUiExtjs ="{{urlUiExtjs}}";
		Main.urlUiStouch ="{{urlUiStouch}}";
 
		/* ui-extjs paths */

		Main.urlStaticCore = "{{urlUiExtjsCore}}";
		Main.urlStaticCoreI18n = "{{urlUiExtjsCoreI18n}}";
		Main.urlStaticModules = "{{urlUiExtjsModules}}";
		Main.urlStaticModuleSubpath = "{{urlUiExtjsModuleSubpath}}";
		Main.urlStaticModuleUseBundle = {{urlUiExtjsModuleUseBundle}};

		/* date format masks */
	 
	  {% for e in dateFormatMasks.entrySet %}
	  Main.{{e.key}} = "{{e.value}}";
	  {% endfor %}
		Main.MODEL_DATE_FORMAT = "{{modelDateFormat}}";
	
		/* number format */
	
		Main.DECIMAL_SEP = "{{decimalSeparator}}";
		Main.THOUSAND_SEP = "{{thousandSeparator}}";
	
	</script>
