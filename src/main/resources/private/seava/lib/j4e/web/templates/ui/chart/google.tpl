<html>
  <head>
  	<title>{{title}}</title>	
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
		  ['x', 'y1' ],		  
		  {% for d in dataList  %}
	      
	       [ '{{d.eventDate}}', 
	          {{d.closeValue}} 
	       ],		 
		  {% endfor %}  
      ]);

      var options = {
        title: '{{title}}'
      };

      var chart = new {{chart}}(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
    </script>
  </head>

  <body>
 
    <div id="chart_div" style="width:1000; height:660"></div>
  </body>
</html>