<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title> {{ productName }} | {{ productVersion }} </title>
	<link rel="shortcut icon" href="{{ ctxpath }}/statics/seava/lib/j4e/branding/images/favicon.ico">

	<script type="text/javascript">
	{% autoescape false %}
		{{constantsJsFragment}}
	{% endautoescape %}	
	</script>

	<!-- Theme /-->	
	<link rel="stylesheet" type="text/css"
        href="{{urlUiExtjsThemes}}/{{theme}}/{{theme}}-all.css" />
    <link rel="stylesheet" type="text/css"
        href="{{urlUiExtjsThemes}}/{{theme}}/{{theme}}-custom.css" />    
</head>
<body>

	{% include "seava/lib/j4e/web/templates/ui/extjs/_loading_mask" %}

	<script type="text/javascript">
    	if(document &&  document.getElementById("n21-loading-msg")) {
        	document.getElementById("n21-loading-msg").innerHTML = "...";
        }
	</script>

	{% if (sysCfg_workingMode == "dev") %} 
	     {% include "seava/lib/j4e/web/templates/ui/extjs/_includes_dev" %} 
  {% else %}
	     {%  include "seava/lib/j4e/web/templates/ui/extjs/_includes_prod" %} 
  {% endif %}
  
  
  {% include "seava/lib/j4e/web/templates/ui/extjs/_params" %}

  {% autoescape false %}
    {{ extensions }}
  {% endautoescape %} 
 
	<script type="text/javascript">
  		if(document && document.getElementById("n21-loading-msg")) {
  	  		document.getElementById("n21-loading-msg").innerHTML = Main.translate("msg", "initialize");
  	  	}
	</script>

	<script>

    Ext.onReady(function(){

		{% include "seava/lib/j4e/web/templates/ui/extjs/_on_ready" %}

    var _session = seava.lib.e4e.js.base.Session;
    
	_session.user.code = "{{user.code}}";
	_session.user.loginName = "{{user.loginName}}";
	_session.user.name = "{{user.name}}";
    _session.user.systemUser = {{user.systemUser}};
		
  	_session.client.id = "{{user.client.id}}";
  	_session.client.code = "{{user.client.code}}";
  	_session.client.name = "{{user.client.name}}";  		
		
	_session.locked = false;
	
	{% autoescape false %}
		_session.roles = [{{userRolesStr}}];
    {% endautoescape %} 

    {% autoescape false %}
      {{ extensionsContent }}
    {% endautoescape %} 
  
      
    var tr = seava.lib.e4e.js.base.TemplateRepository;

		__application__ = seava.lib.e4e.js.base.Application;
		__application__.menu = new seava.lib.e4e.js.base.ApplicationMenu({ region:"north" });
		__application__.view = new Ext.Viewport({
			 layout:"card", 
			 activeItem:0 ,
			 forceLayout:true,
			 items:[
				  { html:"" } ,
			   	  {  padding:0,
			    	layout:"border",
				    forceLayout:true,
				    items:[{
						region:"center",
					   	enableTabScroll : true,
					   	xtype:"tabpanel",
						deferredRender:false,
						activeTab:0,
						plain : true,
						cls: "dnet-home",
						plugins: Ext.create("Ext.ux.TabCloseMenu", {}),
				   		id:"dnet-application-view-body",
				    	items:[{
				    		xtype:"dnetHomePanel",
							id:"dnet-application-view-home"}
				    	]},
				    	__application__.menu
				    ]}
			]
      	});

		__application__.run();
		var map = KeyBindings.createMainKeyMap();

    });

    {% include "seava/lib/j4e/web/templates/ui/extjs/_loading_mask_remove" %}

    var confirm_leave = true;
    var leave_message = "Are you sure you want to leave?"
    function _leave_page(e) {
            if( confirm_leave ===true)
            {
                if(!e) e = window.event;
                e.cancelBubble = true;
                e.returnValue = leave_message;
                if (e.stopPropagation) 
                {
                    e.stopPropagation();
                    e.preventDefault();
                }
                return leave_message;
            }
        }   
    window.onbeforeunload=_leave_page;

  </script>
</body>
</html>