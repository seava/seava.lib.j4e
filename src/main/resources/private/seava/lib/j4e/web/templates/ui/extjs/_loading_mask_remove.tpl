	if (!Ext.isEmpty(Ext.get('e4e-loading'))) {
		setTimeout(function(){
			if (!Ext.isEmpty(Ext.get('e4e-loading-msg'))) {
				Ext.get('e4e-loading-msg').remove();
			}
			if (!Ext.isEmpty(Ext.get('e4e-loading-indicator'))) {
				Ext.get('e4e-loading-indicator').remove();
			}
			if (!Ext.isEmpty(Ext.get('e4e-loading'))) {
				Ext.get('e4e-loading').remove();
			}
			if (!Ext.isEmpty(Ext.get('e4e-loading-mask'))) {
				Ext.get('e4e-loading-mask').fadeOut({remove:true});
			}
		}, 250);
	}