/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business.service;

import seava.lib.j4e.api.base.descriptor.IImportDataPackage;
import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IInitializeClientService {

	/**
	 * Initialize a new client.<br>
	 * Create an administrator user and import some initial data.
	 * 
	 * @param clientId
	 * @param userCode
	 * @param userName
	 * @param loginName
	 * @param password
	 * @param dataPackage
	 * @throws ManagedException
	 */
	public void execute(String clientId, String clientCode, String userCode,
			String userName, String loginName, String password,
			IImportDataPackage dataPackage) throws ManagedException;
}
