/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business.service;

import seava.lib.j4e.api.base.descriptor.IImportDataPackage;

/**
 * Interface for a transactional service to import an {@link IImportDataPackage}
 * . Execution must be started in a transaction, so that all data-files imported
 * from this package can participate in it.
 * 
 * @author amathe
 * 
 */
public interface IImportDataPackageService {

	/**
	 * 
	 * @param dataPackage
	 * @throws Exception
	 */
	public void doExecute(IImportDataPackage dataPackage) throws Exception;

	/**
	 * 
	 * @param xmlString
	 * @param dsName
	 * @param ukFieldName
	 * @throws Exception
	 */
	public void doImportDataFile(String xmlString, String dsName,
			String ukFieldName) throws Exception;

	/**
	 * 
	 * @param xmlString
	 * @throws Exception
	 */
	public void doImportDataPackage(String xmlString) throws Exception;
}
