/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business.service;

import java.util.List;
import java.util.Map;

import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IEntityWriteService<E> extends IEntityReadService<E> {

	/**
	 * Insert a list of new entities. This should be a transactional method.
	 * 
	 * @param list
	 * @throws ManagedException
	 */
	public void insert(List<E> list) throws ManagedException;

	/**
	 * Helper insert method for one single entity. It creates a list with this
	 * single entity and delegates to the <code> insert(List<E> list)</code>
	 * method
	 */
	public void insert(E e) throws ManagedException;

	/**
	 * Update a list of existing entities. This should be a transactional
	 * method.
	 * 
	 * @param list
	 * @throws ManagedException
	 */
	public void update(List<E> list) throws ManagedException;

	/**
	 * Helper update method for one single entity. It creates a list with this
	 * single entity and delegates to the <code> update(List<E> list)</code>
	 * method
	 */
	public void update(E e) throws ManagedException;

	/**
	 * Execute an update JPQL statement. This should be a transactional method.
	 * 
	 * @param jpqlStatement
	 * @param parameters
	 * @return
	 * @throws ManagedException
	 */
	public int update(String jpqlStatement, Map<String, Object> parameters)
			throws ManagedException;

	public void deleteById(Object id) throws ManagedException;

	public void deleteByIds(List<Object> ids) throws ManagedException;

	public void delete(List<E> list) throws ManagedException;

}
