/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business.descriptor;

public interface IAsgnContext {

	public String getLeftTable();

	public String getLeftPkField();

	public String getRightTable();

	public String getRightObjectIdField();

	public String getRightItemIdField();

}
