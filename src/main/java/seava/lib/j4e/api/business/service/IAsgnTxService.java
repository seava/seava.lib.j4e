/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business.service;

import java.util.List;

import javax.persistence.EntityManager;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.descriptor.IAsgnContext;

public interface IAsgnTxService<E> {

	/**
	 * Saves the selection(s).
	 * 
	 * @throws ManagedException
	 */
	public void doSave(IAsgnContext ctx, String selectionId, String objectId)
			throws ManagedException;

	/**
	 * Restores all the changes made by the user to the initial state.
	 * 
	 * @throws ManagedException
	 */
	public void doReset(IAsgnContext ctx, String selectionId, String objectId)
			throws ManagedException;

	/**
	 * Add the specified list of IDs to the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	public void doMoveRight(IAsgnContext ctx, String selectionId,
			List<String> ids) throws ManagedException;

	/**
	 * Add all the available values to the selected ones.
	 * 
	 * @throws ManagedException
	 */
	public void doMoveRightAll(IAsgnContext ctx, String selectionId)
			throws ManagedException;

	/**
	 * Remove the specified list of IDs from the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	public void doMoveLeft(IAsgnContext ctx, String selectionId,
			List<String> ids) throws ManagedException;

	/**
	 * Remove all the selected values.
	 * 
	 * @throws ManagedException
	 */
	public void doMoveLeftAll(IAsgnContext ctx, String selectionId)
			throws ManagedException;

	/**
	 * Initialize the temporary table with the existing selection. Creates a
	 * record in the TEMP_ASGN table and the existing selections in
	 * TEMP_ASGN_LINE.
	 * 
	 * @return the UUID of the selection
	 * @throws ManagedException
	 */
	public String doSetup(IAsgnContext ctx, String asgnName, String objectId)
			throws ManagedException;

	/**
	 * Clean-up the temporary tables.
	 * 
	 * @throws ManagedException
	 */
	public void doCleanup(IAsgnContext ctx, String selectionId)
			throws ManagedException;

	public EntityManager getEntityManager();

	public void setEntityManager(EntityManager em);

	public Class<E> getEntityClass();

	public void setEntityClass(Class<E> entityClass);

}