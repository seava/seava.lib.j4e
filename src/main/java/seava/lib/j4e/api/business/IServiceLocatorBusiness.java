/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business;

import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;

public interface IServiceLocatorBusiness extends ApplicationContextAware {

	/**
	 * Find an entity service given the entity class.
	 * 
	 * @param <E>
	 * @param entityClass
	 * @return
	 * @throws Exception
	 */
	public abstract <E> IEntityService<E> findEntityService(Class<E> entityClass)
			throws ManagedException;

}