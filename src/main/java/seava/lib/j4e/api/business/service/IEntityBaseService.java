/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.business.service;

import javax.persistence.EntityManager;

import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IEntityBaseService<E> {

	/**
	 * Get EntityManager
	 * 
	 * @return
	 */
	public EntityManager getEntityManager();

	/**
	 * Set EntityManager
	 * 
	 * @param em
	 */
	public void setEntityManager(EntityManager em);

	public E create() throws ManagedException;

	public ISettings getSettings();

	public void setSettings(ISettings settings);

	// public void doStartWfProcessInstanceByKey(String processDefinitionKey,
	// String businessKey, Map<String, Object> variables)
	// throws ManagedException;
	//
	// public void doStartWfProcessInstanceById(String processDefinitionId,
	// String businessKey, Map<String, Object> variables)
	// throws ManagedException;
	//
	// public void doStartWfProcessInstanceByMessage(String messageName,
	// String businessKey, Map<String, Object> processVariables)
	// throws ManagedException;
}
