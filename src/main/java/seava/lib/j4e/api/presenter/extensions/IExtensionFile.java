/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.extensions;

public interface IExtensionFile {

	/**
	 * Returns true if this is a .js file
	 */
	public abstract boolean isJs();

	/**
	 * Returns true if this is a .css file
	 */
	public abstract boolean isCss();

	public abstract String getFileExtension();

	public abstract String getLocation();

	public abstract void setLocation(String location);

	public abstract boolean isRelativePath();

	public abstract void setRelativePath(boolean relativePath);

}