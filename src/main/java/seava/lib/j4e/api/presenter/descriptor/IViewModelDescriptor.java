/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

import java.util.Map;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 */
public interface IViewModelDescriptor<M> {

	/**
	 * 
	 * @return
	 */
	public Class<M> getModelClass();

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getRefPaths();

	/**
	 * 
	 * @return
	 */
	public boolean isWorksWithJpql();

	/**
	 * 
	 * @return
	 */
	public String getJpqlDefaultWhere();

	/**
	 * 
	 * @return
	 */
	public String getJpqlDefaultSort();

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getJpqlFieldFilterRules();

	/**
	 * 
	 * @return
	 */
	public Map<String, String> getFetchJoins();

	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getQueryHints();

	/**
	 * 
	 * @return
	 */
	public Map<String, String[]> getOrderBys();
}
