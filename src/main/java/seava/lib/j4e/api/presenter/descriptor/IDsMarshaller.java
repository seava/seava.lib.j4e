/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

import java.io.OutputStream;
import java.util.List;

import seava.lib.j4e.api.presenter.result.IActionResultDelete;
import seava.lib.j4e.api.presenter.result.IActionResultFind;
import seava.lib.j4e.api.presenter.result.IActionResultRpc;
import seava.lib.j4e.api.presenter.result.IActionResultSave;
import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IDsMarshaller<M, F, P> {

	public static final String XML = "xml";
	public static final String JSON = "json";
	public static final String CSV = "csv";

	/**
	 * 
	 * @return
	 */
	public Object getDelegate();

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public IExportInfo readExportInfo(String source) throws ManagedException;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public List<ISortRule> readSortRules(String source) throws ManagedException;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public List<IFilterRule> readFilterRules(String source)
			throws ManagedException;

	/**
	 * 
	 * @param source
	 * @param type
	 * @return
	 * @throws ManagedException
	 */
	public <T> T readDataFromString(String source, Class<T> type)
			throws ManagedException;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public M readDataFromString(String source) throws ManagedException;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public List<M> readListFromString(String source) throws ManagedException;

	/**
	 * 
	 * @param source
	 * @param type
	 * @return
	 * @throws ManagedException
	 */
	public <T> List<T> readListFromString(String source, Class<T> type)
			throws ManagedException;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public F readFilterFromString(String source) throws ManagedException;

	/**
	 * 
	 * @param source
	 * @return
	 * @throws ManagedException
	 */
	public P readParamsFromString(String source) throws ManagedException;

	/**
	 * 
	 * @param m
	 * @return
	 * @throws ManagedException
	 */
	public String writeDataToString(M m) throws ManagedException;

	/**
	 * 
	 * @param list
	 * @return
	 * @throws ManagedException
	 */
	public String writeListToString(List<M> list) throws ManagedException;

	/**
	 * 
	 * @param f
	 * @return
	 * @throws ManagedException
	 */
	public String writeFilterToString(F f) throws ManagedException;

	/**
	 * 
	 * @param p
	 * @return
	 * @throws ManagedException
	 */
	public String writeParamsToString(P p) throws ManagedException;

	/**
	 * 
	 * @param result
	 * @return
	 * @throws ManagedException
	 */
	public String writeResultToString(IActionResultFind result)
			throws ManagedException;

	/**
	 * 
	 * @param result
	 * @return
	 * @throws ManagedException
	 */
	public String writeResultToString(IActionResultSave result)
			throws ManagedException;

	/**
	 * 
	 * @param result
	 * @return
	 * @throws ManagedException
	 */
	public String writeResultToString(IActionResultDelete result)
			throws ManagedException;

	/**
	 * 
	 * @param result
	 * @return
	 * @throws ManagedException
	 */
	public String writeResultToString(IActionResultRpc result)
			throws ManagedException;

	/**
	 * 
	 * @param m
	 * @param out
	 * @throws ManagedException
	 */
	public void writeDataToStream(M m, OutputStream out)
			throws ManagedException;

	/**
	 * 
	 * @param list
	 * @param out
	 * @throws ManagedException
	 */
	public void writeListToStream(List<M> list, OutputStream out)
			throws ManagedException;

	/**
	 * 
	 * @param f
	 * @param out
	 * @throws ManagedException
	 */
	public void writeFilterToStream(F f, OutputStream out)
			throws ManagedException;

	/**
	 * 
	 * @param p
	 * @param out
	 * @throws ManagedException
	 */
	public void writeParamsToStream(P p, OutputStream out)
			throws ManagedException;

	/**
	 * 
	 * @param result
	 * @param out
	 * @throws ManagedException
	 */
	public void writeResultToStream(IActionResultFind result, OutputStream out)
			throws ManagedException;

	/**
	 * 
	 * @param result
	 * @param out
	 * @throws ManagedException
	 */
	public void writeResultToStream(IActionResultSave result, OutputStream out)
			throws ManagedException;

	/**
	 * 
	 * @param result
	 * @param out
	 * @throws ManagedException
	 */
	public void writeResultToStream(IActionResultRpc result, OutputStream out)
			throws ManagedException;

}
