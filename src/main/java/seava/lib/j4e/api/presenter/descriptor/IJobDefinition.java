/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

public interface IJobDefinition {

	public String getName();

	public void setName(String name);

	public Class<?> getJavaClass();

	public void setJavaClass(Class<?> javaClass);
}
