/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.util.Date;
import java.util.Map;

import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface ISysParamValueProvider {

	public Map<String, String> getParamValues(Date validAt)
			throws ManagedException;
}
