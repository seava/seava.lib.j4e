/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

/**
 * Export descriptor. Includes columns and filter information for the export and
 * print operations
 * 
 * @author amathe
 * 
 */
public interface IExportInfo {

	public void prepare(Class<?> modelClass);

}
