/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import seava.lib.j4e.api.base.descriptor.IImportDataFile;

public interface IImportDataFileService {

	public void execute(IImportDataFile dataFile) throws Exception;

}
