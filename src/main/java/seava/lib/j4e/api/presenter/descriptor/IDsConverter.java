/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

import java.util.List;

import javax.persistence.EntityManager;

import seava.lib.j4e.api.base.exceptions.ManagedException;

/**
 * Interface to be implemented by a view-object entity converter
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <E>
 */
public interface IDsConverter<M, E> {

	/**
	 * 
	 * @param m
	 * @param e
	 * @param isInsert
	 * @param em
	 * @throws ManagedException
	 */
	public void modelToEntity(M m, E e, boolean isInsert, EntityManager em)
			throws ManagedException;

	/**
	 * 
	 * @param e
	 * @param m
	 * @param em
	 * @param fieldNames
	 * @throws ManagedException
	 */
	public void entityToModel(E e, M m, EntityManager em,
			List<String> fieldNames) throws ManagedException;

	/**
	 * 
	 * @param entities
	 * @param em
	 * @param fieldNames
	 * @return
	 * @throws ManagedException
	 */
	public List<M> entitiesToModels(List<E> entities, EntityManager em,
			List<String> fieldNames) throws ManagedException;

}
