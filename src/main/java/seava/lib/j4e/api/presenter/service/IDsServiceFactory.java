/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

/**
 * 
 * @author amathe
 * 
 */
public interface IDsServiceFactory {

	/**
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * 
	 * @param key
	 * @return
	 */
	public <M, F, P> IDsService<M, F, P> create(String key);
}
