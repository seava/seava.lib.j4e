/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

public interface IAsgnServiceFactory {

	public <M, F, P> IAsgnService<M, F, P> create(String key);
}
