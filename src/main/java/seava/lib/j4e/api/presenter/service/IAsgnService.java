/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.util.List;

import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IAsgnTxServiceFactory;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;

public interface IAsgnService<M, F, P> extends IAsgnWriteService<M, F, P> {

	/**
	 * Saves the selection(s).
	 * 
	 * @throws ManagedException
	 */
	public void save(String selectionId, String objectId)
			throws ManagedException;

	/**
	 * Restores all the changes made by the user to the initial state.
	 * 
	 * @throws ManagedException
	 */
	public void reset(String selectionId, String objectId)
			throws ManagedException;

	/**
	 * Add the specified list of IDs to the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	public void moveRight(String selectionId, List<String> ids)
			throws ManagedException;

	/**
	 * Add all the available values to the selected ones.
	 * 
	 * @throws ManagedException
	 */
	public void moveRightAll(String selectionId, F filter, P params)
			throws ManagedException;

	/**
	 * Remove the specified list of IDs from the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	public void moveLeft(String selectionId, List<String> ids)
			throws ManagedException;

	/**
	 * Remove all the selected values.
	 * 
	 * @throws ManagedException
	 */
	public void moveLeftAll(String selectionId, F filter, P params)
			throws ManagedException;

	/**
	 * Initialize the temporary table with the existing selection. Creates a
	 * record in the TEMP_ASGN table and the existing selections in
	 * TEMP_ASGN_LINE.
	 * 
	 * @return the UUID of the selection
	 * @throws ManagedException
	 */
	public String setup(String asgnName, String objectId)
			throws ManagedException;

	/**
	 * Clean-up the temporary tables.
	 * 
	 * @throws ManagedException
	 */
	public void cleanup(String selectionId) throws ManagedException;

	public List<M> findLeft(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	public List<M> findRight(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	public Long countLeft(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	public Long countRight(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	public IQueryBuilder<M, F, P> createLeftQueryBuilder()
			throws ManagedException;

	public IQueryBuilder<M, F, P> createRightQueryBuilder()
			throws ManagedException;

	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat)
			throws ManagedException;

	public List<IAsgnTxServiceFactory> getAsgnTxServiceFactories();

	public void setAsgnTxServiceFactories(
			List<IAsgnTxServiceFactory> asgnTxServiceFactories);

	public ISettings getSettings();

	public void setSettings(ISettings settings);
}
