/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IAsgnReadService<M, F, P> extends
		IPresenterBaseService<M, F, P> {

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public List<M> findLeft(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public List<M> findRight(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Long countLeft(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Long countRight(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException;

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> createLeftQueryBuilder()
			throws ManagedException;

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> createRightQueryBuilder()
			throws ManagedException;

	/**
	 * 
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat)
			throws ManagedException;

	// public List<IAsgnTxServiceFactory> getAsgnTxServiceFactories();
	//
	// public void setAsgnTxServiceFactories(
	// List<IAsgnTxServiceFactory> asgnTxServiceFactories);

}
