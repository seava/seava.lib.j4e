/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.result;

public interface IActionResultRpc extends IActionResult {
	public Object getData();

	public void setData(Object data);

	public Object getParams();

	public void setParams(Object params);

}
