/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

public interface IReportServiceFactory {

	public String getName();

	public IReportService create(String key);
}
