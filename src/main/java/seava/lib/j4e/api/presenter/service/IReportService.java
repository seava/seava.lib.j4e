/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import seava.lib.j4e.api.presenter.descriptor.IReportExecutionContext;

/**
 * 
 * @author amathe
 * 
 */
public interface IReportService {

	public Object getReport(String reportFqn) throws Exception;

	public void runReport(IReportExecutionContext context) throws Exception;

}
