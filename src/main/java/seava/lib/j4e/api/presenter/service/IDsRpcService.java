/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.io.InputStream;
import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsRpcService<M, F, P> extends IDsBaseService<M, F, P> {

	/**
	 * 
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcFilter(String procedureName, F filter, P params)
			throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param ds
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcData(String procedureName, M ds, P params)
			throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcData(String procedureName, List<M> list, P params)
			throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcIds(String procedureName, List<Object> list, P params)
			throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcFilterStream(String procedureName, F filter, P params)
			throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param ds
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcDataStream(String procedureName, M ds, P params)
			throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcDataStream(String procedureName, List<M> list,
			P params) throws ManagedException;

	/**
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcIdsStream(String procedureName, List<Object> list,
			P params) throws ManagedException;

}
