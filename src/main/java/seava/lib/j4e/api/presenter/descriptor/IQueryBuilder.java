/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

import java.util.List;

import seava.lib.j4e.api.presenter.descriptor.IViewModelDescriptor;
import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IQueryBuilder<M, F, P> {

	/**
	 * Add fetch limit constraint. <br>
	 * Fetch <code>resultSize</code> number of results starting from
	 * <code>resultStart</code> position.
	 * 
	 * @param resultStart
	 * @param resultSize
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addFetchLimit(int resultStart, int resultSize);

	/**
	 * Add sort information. The number of elements in the two arrays must be
	 * the same.
	 * 
	 * @param columnList
	 *            Array of field names E.g. {"name", "code"}
	 * @param senseList
	 *            Array of sense E.g. {"asc", "desc"}
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addSortInfo(String[] columnList,
			String[] senseList);

	/**
	 * Add sort information. The number of elements in the strings must be the
	 * same.
	 * 
	 * @param columns
	 *            Comma delimited column names E.g. "name,code"
	 * @param sense
	 *            Comma delimited column names E.g. "asc,desc"
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addSortInfo(String columns, String sense);

	/**
	 * Add sort information.
	 * 
	 * @param sortTokens
	 *            The sort tokens E.g. {"name", "code desc", "id asc"}
	 * @return this
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> addSortInfo(String[] sortTokens)
			throws ManagedException;

	/**
	 * Add sort information.
	 * 
	 * @param sortTokens
	 * @return this
	 */
	public IQueryBuilder<M, F, P> addSortInfo(
			List<? extends ISortRule> sortTokens);

	public IQueryBuilder<M, F, P> addFilterRules(
			List<? extends IFilterRule> filterRules);

	public Class<M> getModelClass();

	public void setModelClass(Class<M> modelClass);

	public Class<F> getFilterClass();

	public void setFilterClass(Class<F> filterClass);

	public Class<P> getParamClass();

	public void setParamClass(Class<P> paramClass);

	public F getFilter();

	public void setFilter(F filter);

	public List<IFilterRule> getFilterRules();

	public void setFilterRules(List<IFilterRule> filterRules);

	public IQueryBuilder<M, F, P> addFilter(F filter) throws ManagedException;

	public P getParams();

	public void setParams(P params);

	public IQueryBuilder<M, F, P> addParams(P params) throws ManagedException;

	public IViewModelDescriptor<M> getDescriptor();

	public void setDescriptor(IViewModelDescriptor<M> descriptor);

	public ISettings getSettings();

	public void setSettings(ISettings settings);

}
