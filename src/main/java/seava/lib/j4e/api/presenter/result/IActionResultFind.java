/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.result;

import java.util.List;
import java.util.Map;

public interface IActionResultFind extends IActionResult {

	public Long getTotalCount();

	public void setTotalCount(Long totalCount);

	public List<?> getData();

	public void setData(List<?> data);

	public Object getParams();

	public void setParams(Object params);

	public Map<String, Object> getSummaries();

	public void setSummaries(Map<String, Object> summaries);

}
