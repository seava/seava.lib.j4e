/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

/**
 * 
 * @author amathe
 * 
 */
public interface IUploadedFileDescriptor {

	/**
	 * 
	 * @return
	 */
	public String getOriginalName();

	/**
	 * 
	 * @param originalName
	 */
	public void setOriginalName(String originalName);

	/**
	 * 
	 * @return
	 */
	public String getNewName();

	/**
	 * 
	 * @param newName
	 */
	public void setNewName(String newName);

	/**
	 * 
	 * @return
	 */
	public String getContentType();

	/**
	 * 
	 * @param contentType
	 */
	public void setContentType(String contentType);

	/**
	 * 
	 * @return
	 */
	public long getSize();

	/**
	 * 
	 * @param size
	 */
	public void setSize(long size);
}
