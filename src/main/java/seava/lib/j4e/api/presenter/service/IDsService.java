/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

/**
 * Interface to be implemented by any data-source service.
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <P>
 */
public interface IDsService<M, F, P> extends IDsWriteService<M, F, P> {

}
