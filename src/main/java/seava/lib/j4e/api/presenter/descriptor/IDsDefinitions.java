/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

import java.util.Collection;

import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IDsDefinitions {

	/**
	 * Check if this context contains the given data-source definition.
	 * 
	 * @param name
	 * @return
	 */
	public boolean containsDs(String name);

	/**
	 * Returns the definition for the given data-source name.
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public IDsDefinition getDsDefinition(String name) throws ManagedException;

	/**
	 * Returns a collection with all the data-source definitions from this
	 * context.
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection<IDsDefinition> getDsDefinitions() throws ManagedException;
}
