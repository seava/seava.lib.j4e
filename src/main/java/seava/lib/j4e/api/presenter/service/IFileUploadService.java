/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import seava.lib.j4e.api.presenter.descriptor.IUploadedFileDescriptor;

public interface IFileUploadService {

	public List<String> getParamNames();

	public Map<String, Object> execute(IUploadedFileDescriptor fileDescriptor,
			InputStream inputStream, Map<String, String> params)
			throws Exception;

}
