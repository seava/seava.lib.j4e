/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;

/**
 * @author amathe
 * 
 */
public interface IPresenterBaseService<M, F, P> {

	/**
	 * 
	 * @param dataFormat
	 * @return
	 * @throws Exception
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat)
			throws ManagedException;

	/**
	 * 
	 * @return
	 */
	public Class<M> getModelClass();

	/**
	 * 
	 * @return
	 */
	public Class<F> getFilterClass();

	/**
	 * 
	 * @return
	 */
	public Class<P> getParamClass();

	/**
	 * 
	 * @return
	 */
	public ISettings getSettings();

	/**
	 * 
	 * @param settings
	 */
	public void setSettings(ISettings settings);

}
