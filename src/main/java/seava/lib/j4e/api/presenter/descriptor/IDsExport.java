/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.descriptor;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import seava.lib.j4e.api.base.exceptions.ManagedException;

public interface IDsExport<M> {

	/**
	 * 
	 * @return
	 */
	public IExportInfo getExportInfo();

	/**
	 * 
	 * @param exportInfo
	 */
	public void setExportInfo(IExportInfo exportInfo);

	/**
	 * 
	 * @throws ManagedException
	 */
	public void begin() throws ManagedException;

	/**
	 * 
	 * @throws ManagedException
	 */
	public void end() throws ManagedException;

	/**
	 * 
	 * @param data
	 * @param isFirst
	 * @throws ManagedException
	 */
	public void write(M data, boolean isFirst) throws ManagedException;

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public File getOutFile() throws ManagedException;

	/**
	 * 
	 * @param outFile
	 */
	public void setOutFile(File outFile);

	/**
	 * 
	 * @return
	 */
	public String getOutFilePath();

	/**
	 * 
	 * @param outFilePath
	 */
	public void setOutFilePath(String outFilePath);

	/**
	 * 
	 * @return
	 */
	public String getOutFileName();

	/**
	 * 
	 * @param outFileName
	 */
	public void setOutFileName(String outFileName);

	/**
	 * 
	 * @return
	 */
	public String getOutFileExtension();

	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getProperties();

	/**
	 * 
	 * @param properties
	 */
	public void setProperties(Map<String, Object> properties);

}
