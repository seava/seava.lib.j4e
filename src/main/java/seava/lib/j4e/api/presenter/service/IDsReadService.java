/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.util.List;
import java.util.Map;

import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsExport;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IDsReadService<M, F, P> extends IDsRpcService<M, F, P> {

	/**
	 * 
	 * @param id
	 * @return
	 * @throws ManagedException
	 */
	public M findById(Object id) throws ManagedException;

	/**
	 * 
	 * @param id
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public M findById(Object id, P params) throws ManagedException;

	/**
	 * 
	 * @param ids
	 * @return
	 * @throws ManagedException
	 */
	public List<M> findByIds(List<Object> ids) throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter) throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, int resultStart, int resultSize)
			throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, P params) throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param params
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, P params, int resultStart, int resultSize)
			throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param filterRules
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, List<IFilterRule> filterRules)
			throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param filterRules
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, List<IFilterRule> filterRules,
			int resultStart, int resultSize) throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules)
			throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @param resultStart
	 * @param resultSize
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules,
			int resultStart, int resultSize) throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @param sortTokens
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules,
			List<ISortRule> sortTokens) throws ManagedException;

	/**
	 * 
	 * @param filter
	 * @param params
	 * @param filterRules
	 * @param resultStart
	 * @param resultSize
	 * @param sortTokens
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(F filter, P params, List<IFilterRule> filterRules,
			int resultStart, int resultSize, List<ISortRule> sortTokens)
			throws ManagedException;

	/**
	 * 
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(IQueryBuilder<M, F, P> builder) throws ManagedException;

	/**
	 * 
	 * @param builder
	 * @param fieldNames
	 * @return
	 * @throws ManagedException
	 */
	public List<M> find(IQueryBuilder<M, F, P> builder, List<String> fieldNames)
			throws ManagedException;

	/**
	 * Get query result count
	 * 
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Long count(IQueryBuilder<M, F, P> builder) throws ManagedException;

	/**
	 * Get aggregate values
	 * 
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Map<String, Object> summaries(IQueryBuilder<M, F, P> builder)
			throws ManagedException;

	/**
	 * 
	 * @param builder
	 * @param writer
	 * @throws ManagedException
	 */
	public void doExport(IQueryBuilder<M, F, P> builder, IDsExport<M> writer)
			throws ManagedException;

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> createQueryBuilder() throws ManagedException;

	/**
	 * 
	 * @param dataFormat
	 * @return
	 * @throws ManagedException
	 */
	public IDsExport<M> createExporter(String dataFormat)
			throws ManagedException;

}
