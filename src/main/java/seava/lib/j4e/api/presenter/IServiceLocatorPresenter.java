/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter;

import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IAsgnTxService;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.api.presenter.service.IDsService;

/**
 * 
 * @author amathe
 * 
 */
public interface IServiceLocatorPresenter extends ApplicationContextAware {

	// /**
	// * Find a report service given its spring bean alias.
	// *
	// * @param reportServiceAlias
	// * @return
	// * @throws ManagedException
	// */
	// public IReportService findReportService(String reportServiceAlias)
	// throws ManagedException;

	/**
	 * Find a data-source service given the data-source name.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param dsName
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IDsService<M, F, P> findDsService(String dsName)
			throws ManagedException;

	/**
	 * Find a data-source service given the data-source model class.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param modelClass
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IDsService<M, F, P> findDsService(Class<?> modelClass)
			throws ManagedException;

	/**
	 * Find an entity service given the entity class.
	 * 
	 * @param <E>
	 * @param entityClass
	 * @return
	 * @throws ManagedException
	 */
	public <E> IEntityService<E> findEntityService(Class<E> entityClass)
			throws ManagedException;

	/**
	 * Find an assignment service given the service name.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param asgnName
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IAsgnService<M, F, P> findAsgnService(String asgnName)
			throws ManagedException;

	/**
	 * Find an business assignment service given the service name and factory
	 * name.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param asgnName
	 * @return
	 * @throws ManagedException
	 */
	public <E> IAsgnTxService<E> findAsgnTxService(String asgnName,
			String factoryName) throws ManagedException;

	// /**
	// * Find an business assignment service given the service name.
	// *
	// * @param <M>
	// * @param <P>
	// * @param asgnName
	// * @return
	// * @throws ManagedException
	// */
	// public <E> IAsgnTxService<E> findAsgnTxService(String asgnName)
	// throws ManagedException;

}