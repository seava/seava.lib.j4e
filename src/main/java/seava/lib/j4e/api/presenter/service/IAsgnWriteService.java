/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.presenter.service;

import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public interface IAsgnWriteService<M, F, P> extends IAsgnReadService<M, F, P> {

	/**
	 * Saves the selection(s).
	 * 
	 * @throws Exception
	 */
	public void save(String selectionId, String objectId)
			throws ManagedException;

	/**
	 * Restores all the changes made by the user to the initial state.
	 * 
	 * @throws Exception
	 */
	public void reset(String selectionId, String objectId)
			throws ManagedException;

	/**
	 * Add the specified list of IDs to the selected ones.
	 * 
	 * @param ids
	 * @throws Exception
	 */
	public void moveRight(String selectionId, List<String> ids)
			throws ManagedException;

	/**
	 * Add all the available values to the selected ones.
	 * 
	 * @throws Exception
	 */
	public void moveRightAll(String selectionId, F filter, P params)
			throws ManagedException;

	/**
	 * Remove the specified list of IDs from the selected ones.
	 * 
	 * @param ids
	 * @throws Exception
	 */
	public void moveLeft(String selectionId, List<String> ids)
			throws ManagedException;

	/**
	 * Remove all the selected values.
	 * 
	 * @throws Exception
	 */
	public void moveLeftAll(String selectionId, F filter, P params)
			throws ManagedException;

	/**
	 * Initialize the temporary table with the existing selection. Creates a
	 * record in the TEMP_ASGN table and the existing selections in
	 * TEMP_ASGN_LINE.
	 * 
	 * @return the UUID of the selection
	 * @throws Exception
	 */
	public String setup(String asgnName, String objectId)
			throws ManagedException;

	/**
	 * Clean-up the temporary tables.
	 * 
	 * @throws Exception
	 */
	public void cleanup(String selectionId) throws ManagedException;

}
