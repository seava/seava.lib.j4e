/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.web;

import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.api.presenter.service.IReportService;

/**
 * 
 * @author amathe
 * 
 */
public interface IServiceLocatorWeb extends ApplicationContextAware {

	/**
	 * Find a report service given its spring bean alias.
	 * 
	 * @param reportServiceAlias
	 * @return
	 * @throws ManagedException
	 */
	public IReportService findReportService(String reportServiceAlias)
			throws ManagedException;

	/**
	 * Find a data-source service given the data-source name.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param dsName
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IDsService<M, F, P> findDsService(String dsName)
			throws ManagedException;

	/**
	 * Find a data-source service given the data-source model class.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param modelClass
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IDsService<M, F, P> findDsService(Class<?> modelClass)
			throws ManagedException;

	/**
	 * Find an assignment service given the service name.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param asgnName
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IAsgnService<M, F, P> findAsgnService(String asgnName)
			throws ManagedException;

}