/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.web.security;

/**
 * ThreadLocal variable holder used to send extra login parameters to the
 * authentication service.
 */
public class LoginParamsHolder {
	public static ThreadLocal<ILoginParams> params = new ThreadLocal<ILoginParams>();
}
