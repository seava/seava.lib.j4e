/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.web.security;

public interface IChangePasswordService {

	public void doChangePassword(String userCode, String newPassword,
			String oldPassword, String clientId, String clientCode)
			throws Exception;
}
