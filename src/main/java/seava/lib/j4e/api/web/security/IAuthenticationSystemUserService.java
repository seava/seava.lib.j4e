/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.web.security;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface IAuthenticationSystemUserService extends UserDetailsService {

}
