/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.web.security;

public interface IPasswordValidator {

	public void validate(String passwordToValidate) throws Exception;
}
