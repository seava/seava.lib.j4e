/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.session;

public interface IWorkspace {

	public String getWorkspacePath();

	public String getImportPath();

	public String getExportPath();

	public String getTempPath();

}
