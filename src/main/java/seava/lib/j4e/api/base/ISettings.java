/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base;

import java.util.List;

import seava.lib.j4e.api.base.descriptor.ISysParamDefinitions;
import seava.lib.j4e.api.base.exceptions.ManagedException;

/**
 * 
 * @author amathe
 * 
 */
public interface ISettings {

	/**
	 * Get a system property value as string
	 * 
	 * @param key
	 * @return
	 */
	public String get(String key);

	/**
	 * Get a system property value as boolean
	 * 
	 * @param key
	 * @return
	 */
	public boolean getAsBoolean(String key);

	/**
	 * Get the value of a system parameter as string
	 * 
	 * @param paramName
	 * @return
	 * @throws ManagedException
	 */
	public String getParam(String paramName) throws ManagedException;

	/**
	 * Get the value of a system parameter as boolean
	 * 
	 * @param paramName
	 * @return
	 * @throws ManagedException
	 */
	public boolean getParamAsBoolean(String paramName) throws ManagedException;

	/**
	 * 
	 * @return
	 */
	public String getProductName();

	/**
	 * 
	 * @return
	 */
	public String getProductDescription();

	/**
	 * 
	 * @return
	 */
	public String getProductVendor();

	/**
	 * 
	 * @return
	 */
	public String getProductUrl();

	/**
	 * 
	 * @return
	 */
	public String getProductVersion();

	/**
	 * Reload the parameter definitions.
	 */
	public void reloadParams();

	/**
	 * Reload the parameter values for the current client.
	 */
	public void reloadParamValues() throws ManagedException;

	public List<ISysParamDefinitions> getParamDefinitions();

}
