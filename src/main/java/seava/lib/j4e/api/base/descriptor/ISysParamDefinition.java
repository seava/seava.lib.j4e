/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

public interface ISysParamDefinition {

	/**
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * 
	 * @return
	 */
	public String getTitle();

	/**
	 * 
	 * @return
	 */
	public String getDescription();

	/**
	 * 
	 * @return
	 */
	public String getDataType();

	/**
	 * 
	 * @return
	 */
	public String getDefaultValue();

	/**
	 * 
	 * @return
	 */
	public String getListOfValues();

}