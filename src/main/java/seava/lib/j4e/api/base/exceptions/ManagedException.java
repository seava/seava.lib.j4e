package seava.lib.j4e.api.base.exceptions;

public class ManagedException extends Exception {

	private static final long serialVersionUID = 1L;

	private IErrorCode errorCode;
	private Object[] values;

	public ManagedException(IErrorCode errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public ManagedException(IErrorCode errorCode, String message,
			Object[] values) {
		super(message);
		this.errorCode = errorCode;
		this.values = values;
	}

	public ManagedException(IErrorCode errorCode, String message,
			Throwable exception) {
		this.errorCode = errorCode;
		this.initCause(exception);
	}

	public ManagedException(IErrorCode errorCode, String message,
			Throwable exception, Object[] values) {
		this.errorCode = errorCode;
		this.initCause(exception);
	}

	public IErrorCode getErrorCode() {
		return this.errorCode;
	}

	public Object[] getValues() {
		return this.values;
	}
}
