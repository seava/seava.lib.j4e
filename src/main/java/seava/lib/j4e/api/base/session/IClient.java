/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.session;

/**
 * Client(Tenant) information
 * 
 * @author amathe
 * 
 */
public interface IClient {

	public String getId();

	public String getCode();

	public String getName();

}
