/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

import java.util.List;

public interface IImportDataPackage {

	/**
	 * 
	 * @return
	 */
	public String getLocation();

	/**
	 * 
	 * @param location
	 */
	public void setLocation(String location);

	/**
	 * 
	 * @param dataSet
	 */
	public void addToList(IImportDataSet dataSet);

	/**
	 * 
	 * @return
	 */
	public List<IImportDataSet> getDataSets();

	/**
	 * 
	 * @param dataSets
	 */
	public void setDataSets(List<IImportDataSet> dataSets);

}
