/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.exceptions;

/**
 * 
 * @author amathe
 * 
 */
public interface IErrorCode {

	/**
	 * 
	 * @return
	 */
	public String getErrGroup();

	/**
	 * 
	 * @return
	 */
	public int getErrNo();

	/**
	 * 
	 * @return
	 */
	public String getErrMsg();
}
