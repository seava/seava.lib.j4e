/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

/**
 * Interface to be implemented by all domain entities which belong to a
 * client(tenant) . A client is an isolated self-contained environment.
 * 
 * @author AMATHE
 * 
 */
public interface IModelWithClientId {

	public String getClientId();

	public void setClientId(String clientId);
}
