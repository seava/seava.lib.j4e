/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

/**
 * Represents a single sort rule
 * 
 * @author amathe
 * 
 */
public interface ISortRule {

	public String getProperty();

	public void setProperty(String property);

	public String getDirection();

	public void setDirection(String direction);

}
