/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.session;

/**
 * Session object to expose through thread-local variables session related
 * information in order to be accessible throughout the entire application.
 * 
 * @author amathe
 * 
 */
public class Session {

	/**
	 * Authenticated user
	 */
	public static ThreadLocal<IUser> user = new ThreadLocal<IUser>();

}
