/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

import java.util.List;

/**
 * 
 * @author amathe
 * 
 */
public interface IImportDataSet {

	/**
	 * 
	 * @return
	 */
	public List<IImportDataFile> getDataFiles();

	/**
	 * 
	 * @param dataFiles
	 */
	public void setDataFiles(List<IImportDataFile> dataFiles);
}
