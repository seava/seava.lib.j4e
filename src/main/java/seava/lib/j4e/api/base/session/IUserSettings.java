/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.session;

import java.text.SimpleDateFormat;

import seava.lib.j4e.api.base.enums.DateFormatAttribute;

/**
 * Authenticated user preferences
 * 
 * @author amathe
 * 
 */
public interface IUserSettings {

	public String getLanguage();

	public void setLanguage(String language);

	public String getNumberFormat();

	public String getDecimalSeparator();

	public String getThousandSeparator();

	public String getDateFormatMask(String key);

	public SimpleDateFormat getDateFormat(DateFormatAttribute dfa);

	public SimpleDateFormat getDateFormat(String key);
}