/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

public interface IImportDataFile {

	public String getDs();

	public void setDs(String ds);

	public String getFile();

	public void setFile(String file);

	public String getUkFieldName();

	public void setUkFieldName(String ukFieldName);
}
