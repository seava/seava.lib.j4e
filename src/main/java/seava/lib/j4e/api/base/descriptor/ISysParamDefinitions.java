/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

import java.util.Collection;

/**
 * 
 * @author amathe
 * 
 */
public interface ISysParamDefinitions {

	/**
	 * 
	 * @return
	 */
	public Collection<ISysParamDefinition> getSysParamDefinitions();
}
