/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

/**
 * Interface to be implemented by all models(entities and view-objects) which
 * have an <code>id</code> primary key.
 * 
 * @author amathe
 * 
 */
public interface IModelWithId<T> {

	public T getId();

	public void setId(T id);
}
