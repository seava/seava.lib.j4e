/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.api.base.descriptor;

public interface IFieldDefinition {

	public abstract String getName();

	public abstract String getClassName();

}