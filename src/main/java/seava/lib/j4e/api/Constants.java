package seava.lib.j4e.api;

public class Constants {

	/* =========================================================== */
	/* ======================== data type ======================== */
	/* =========================================================== */

	public static final String DATA_TYPE_STRING = "string";
	public static final String DATA_TYPE_BOOLEAN = "boolean";
	public static final String DATA_TYPE_NUMBER = "number";
	public static final String DATA_TYPE_DATE = "date";

	/* =========================================================== */
	/* ======================= data format ======================= */
	/* =========================================================== */

	public static final String DATA_FORMAT_CSV = "csv";
	public static final String DATA_FORMAT_JSON = "json";
	public static final String DATA_FORMAT_XML = "xml";
	public static final String DATA_FORMAT_HTML = "html";
	public static final String DATA_FORMAT_PDF = "pdf";
	public static final String DATA_FORMAT_XLS = "xls";

	/* =========================================================== */
	/* ==================== default values ======================= */
	/* =========================================================== */

	public static final String DEFAULT_CACHE_FOLDER = "~/j4e/cache";

	public static final String DEFAULT_LANGUAGE = "en_US";
	public static final String DEFAULT_NUMBER_FORMAT = "0,000.00";

	private static String DEFAULT_SERVER_DATE_FORMAT = "yyyy-MM-dd";
	private static String DEFAULT_SERVER_TIME_FORMAT = "HH:mm:ss";
	private static String DEFAULT_SERVER_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static String DEFAULT_SERVER_ALT_FORMATS = "yyyy-MM-dd'T'HH:mm:ss;yyyy-MM-dd HH:mm:ss;yyyy-MM-dd'T'HH:mm;yyyy-MM-dd HH:mm;yyyy-MM-dd";
	private static String DEFAULT_EXTJS_MODEL_DATE_FORMAT = "Y-m-d\\TH:i:s";

	public static final String DEFAULT_ENCRYPTION_ALGORITHM = "MD5";

	/* =========================================================== */
	/* ================== spring bean aliases ==================== */
	/* =========================================================== */

	public static final String SPRING_OSGI_DS_DEFINITIONS = "osgiDsDefinitions";
	public static final String SPRING_OSGI_SYSPARAM_DEFINITIONS = "osgiSysParamDefinitions";
	public static final String SPRING_OSGI_FILE_UPLOAD_SERVICE_FACTORIES = "osgiFileUploadServiceFactories";
	public static final String SPRING_OSGI_EXTENSION_PROVIDERS = "osgiExtensionProviders";
	public static final String SPRING_OSGI_EXTENSION_CONTENT_PROVIDERS = "osgiExtensionContentProviders";

	public static final String SPRING_MSG_IMPORT_DATA_FILE = "msgImportDataFile";
	public static final String SPRING_DEFAULT_ASGN_TX_SERVICE = "defaultAsgnTxService";
	public static final String SPRING_AUTH_MANAGER = "authenticationManager";

	public static final String SPRING_DSEXPORT_WRITER_CSV = "dsExportWriterCsv";
	public static final String SPRING_DSEXPORT_WRITER_XML = "dsExportWriterXml";
	public static final String SPRING_DSEXPORT_WRITER_JSON = "dsExportWriterJson";
	public static final String SPRING_DSEXPORT_WRITER_HTML = "dsExportWriterHtml";

	/* =========================================================== */
	/* ==================== application roles ==================== */
	/* =========================================================== */

	public static final String ROLE_ADMIN_CODE = "ADMIN";
	public static final String ROLE_ADMIN_NAME = "Administrator";
	public static final String ROLE_ADMIN_DESC = "Administrator role for un-restricted access to business functions";

	public static final String ROLE_USER_CODE = "CONNECT";
	public static final String ROLE_USER_NAME = "Application access";
	public static final String ROLE_USER_DESC = "Application role which allows a user to access the application";

	/* =========================================================== */
	/* =================== system properties ===================== */
	/* =========================================================== */

	public final static String PROP_WORKSPACE = "workspace";

	public final static String PROP_WORKING_MODE = "workingMode"; // dev, prod
	public static final String PROP_WORKING_MODE_DEV = "dev";
	public static final String PROP_WORKING_MODE_PROD = "prod";
	public static final String PROP_DEPLOYMENT = "deployment"; // jee, virgo
	public static final String PROP_DEPLOYMENT_JEE = "jee";
	public static final String PROP_DEPLOYMENT_VIRGO = "virgo";
	public static final String PROP_CTXPATH = "ctxpath";

	public final static String PROP_LOGIN_PAGE = "loginPage";
	public static final String PROP_LOGIN_PAGE_LOGO = "loginPageLogo";
	public static final String PROP_LOGIN_PAGE_CSS = "loginPageCss";

	public final static String PROP_SERVER_DATE_FORMAT = "serverDateFormat";
	public final static String PROP_SERVER_TIME_FORMAT = "serverTimeFormat";
	public final static String PROP_SERVER_DATETIME_FORMAT = "serverDateTimeFormat";
	public final static String PROP_SERVER_ALT_FORMATS = "serverAltFormats";
	public final static String PROP_EXTJS_MODEL_DATE_FORMAT = "extjsModelDateFormat";

	public final static String PROP_EXTJS_DATE_FORMAT = "extjsDateFormat";
	public final static String PROP_EXTJS_TIME_FORMAT = "extjsTimeFormat";
	public final static String PROP_EXTJS_DATETIME_FORMAT = "extjsDateTimeFormat";
	public final static String PROP_EXTJS_DATETIMESEC_FORMAT = "extjsDateTimeSecFormat";
	public final static String PROP_EXTJS_MONTH_FORMAT = "extjsMonthFormat";
	public final static String PROP_EXTJS_ALT_FORMATS = "extjsAltFormats";

	public final static String PROP_JAVA_DATE_FORMAT = "javaDateFormat";
	public final static String PROP_JAVA_TIME_FORMAT = "javaTimeFormat";
	public final static String PROP_JAVA_DATETIME_FORMAT = "javaDateTimeFormat";
	public final static String PROP_JAVA_DATETIMESEC_FORMAT = "javaDateTimeSecFormat";
	public final static String PROP_JAVA_MONTH_FORMAT = "javaMonthFormat";
	public final static String PROP_JAVA_ALT_FORMATS = "javaAltFormats";

	public final static String PROP_LANGUAGE = "language";
	public final static String PROP_NUMBER_FORMAT = "numberFormat";

	public final static String PROP_DISABLE_FETCH_GROUPS = "disableFetchGroups";

	/* =========================================================== */
	/* ========================= others ========================== */
	/* =========================================================== */

	public static final String UUID_GENERATOR_NAME = "system-uuid";

	/**
	 * Hack to expose the server date format masks as statics for property
	 * editors.
	 * 
	 * The values are set by the {@link Settings} constructor
	 */
	public static final String get_server_date_format() {
		return DEFAULT_SERVER_DATE_FORMAT;
	}

	public static final String get_server_time_format() {
		return DEFAULT_SERVER_TIME_FORMAT;
	}

	public static final String get_server_datetime_format() {
		return DEFAULT_SERVER_DATETIME_FORMAT;
	}

	public static final String get_server_alt_formats() {
		return DEFAULT_SERVER_ALT_FORMATS;
	}

	public static final String get_extjs_model_date_format() {
		return DEFAULT_EXTJS_MODEL_DATE_FORMAT;
	}

	public static final void set_server_date_format(String v) {
		DEFAULT_SERVER_DATE_FORMAT = v;
	}

	public static final void set_server_time_format(String v) {
		DEFAULT_SERVER_TIME_FORMAT = v;
	}

	public static final void set_server_datetime_format(String v) {
		DEFAULT_SERVER_DATETIME_FORMAT = v;
	}

	public static final void set_server_alt_formats(String v) {
		DEFAULT_SERVER_ALT_FORMATS = v;
	}

	public static final void set_extjs_model_date_format(String v) {
		DEFAULT_EXTJS_MODEL_DATE_FORMAT = v;
	}
}
