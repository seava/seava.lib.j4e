package seava.lib.j4e.web.controller.data;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.api.presenter.result.IActionResultFind;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.web.ConstantsWeb;
import seava.lib.j4e.web.result.ActionResultFind;

public abstract class AbstractAsgnReadController<M, F, P> extends
		AbstractAsgnBaseController<M, F, P> {

	private final static Logger logger = LoggerFactory
			.getLogger(AbstractAsgnReadController.class);

	/**
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_SETUP)
	@ResponseBody
	public String setup(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_SETUP });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={}",
						new Object[] { objectId, selectionId });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "find");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			return service.setup(resourceName, objectId);
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * Default handler for find action.
	 * 
	 * @param resourceName
	 * @param dataformat
	 * @param dataString
	 * @param paramString
	 * @param resultStart
	 * @param resultSize
	 * @param orderByCol
	 * @param orderBySense
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_QUERY_LEFT)
	@ResponseBody
	public String findLeft(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String dataString,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_START, required = false, defaultValue = "0") int resultStart,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_SIZE, required = false, defaultValue = "500") int resultSize,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_QUERY_LEFT });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={} data={}",
						new Object[] { objectId, selectionId, dataString });
				logger.debug("  --> request-params: {} ",
						new Object[] { paramString });
				logger.debug(
						"  --> request-orderBy: sort={}, sense={}, orderBy={}",
						new Object[] { orderByCol, orderBySense, orderBy });
				logger.debug("  --> request-result-range: {} ", new Object[] {
						resultStart + "", (resultStart + resultSize) + "" });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "find");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));
			IDsMarshaller<M, F, P> marshaller = service
					.createMarshaller(dataFormat);

			IQueryBuilder<M, F, P> builder = service.createLeftQueryBuilder()
					.addFetchLimit(resultStart, resultSize);

			if (orderBy != null && !orderBy.equals("")) {
				List<ISortRule> sortTokens = marshaller.readSortRules(orderBy);
				builder.addSortInfo(sortTokens);
			} else {
				builder.addSortInfo(orderByCol, orderBySense);
			}

			F filter = marshaller.readFilterFromString(dataString);
			P params = marshaller.readParamsFromString(paramString);

			List<M> list = service.findLeft(selectionId, filter, params,
					builder);
			long totalCount = service.countLeft(selectionId, filter, params,
					builder);

			IActionResultFind result = this.packfindResult(list, params,
					totalCount);
			stopWatch.stop();
			result.setExecutionTime(stopWatch.getTime());

			return marshaller.writeResultToString(result);
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * Default handler for find action.
	 * 
	 * @param resourceName
	 * @param dataformat
	 * @param dataString
	 * @param paramString
	 * @param resultStart
	 * @param resultSize
	 * @param orderByCol
	 * @param orderBySense
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_QUERY_RIGHT)
	@ResponseBody
	public String findRight(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_FILTER, required = false, defaultValue = "{}") String dataString,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_PARAMS, required = false, defaultValue = "{}") String paramString,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_START, required = false, defaultValue = "0") int resultStart,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_SIZE, required = false, defaultValue = "500") int resultSize,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_SORT, required = false, defaultValue = "") String orderByCol,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_SENSE, required = false, defaultValue = "") String orderBySense,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ORDERBY, required = false, defaultValue = "") String orderBy,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_QUERY_RIGHT });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={} data={}",
						new Object[] { objectId, selectionId, dataString });
				logger.debug("  --> request-params: {} ",
						new Object[] { paramString });
				logger.debug(
						"  --> request-orderBy: sort={}, sense={}, orderBy={}",
						new Object[] { orderByCol, orderBySense, orderBy });
				logger.debug("  --> request-result-range: {} ", new Object[] {
						resultStart + "", (resultStart + resultSize) + "" });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "find");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			IDsMarshaller<M, F, P> marshaller = service
					.createMarshaller(dataFormat);

			IQueryBuilder<M, F, P> builder = service.createRightQueryBuilder()
					.addFetchLimit(resultStart, resultSize);

			if (orderBy != null && !orderBy.equals("")) {
				List<ISortRule> sortTokens = marshaller.readSortRules(orderBy);
				builder.addSortInfo(sortTokens);
			} else {
				builder.addSortInfo(orderByCol, orderBySense);
			}

			F filter = marshaller.readFilterFromString(dataString);
			P params = marshaller.readParamsFromString(paramString);

			List<M> list = service.findRight(selectionId, filter, params,
					builder);
			long totalCount = service.countRight(selectionId, filter, params,
					builder);

			IActionResultFind result = this.packfindResult(list, params,
					totalCount);
			stopWatch.stop();
			result.setExecutionTime(stopWatch.getTime());

			return marshaller.writeResultToString(result);
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * 
	 * @param selectedIds
	 * @return
	 */
	protected List<String> selectedIdsAsList(String selectedIds) {
		String[] tmp = selectedIds.split(",");
		List<String> ids = new ArrayList<String>();
		for (String i : tmp) {
			ids.add(i);
		}
		return ids;
	}

	/**
	 * 
	 * @param data
	 * @param params
	 * @param totalCount
	 * @return
	 */
	public IActionResultFind packfindResult(List<M> data, P params,
			long totalCount) {
		IActionResultFind pack = new ActionResultFind();
		pack.setData(data);
		pack.setParams(params);
		pack.setTotalCount(totalCount);
		return pack;
	}
}
