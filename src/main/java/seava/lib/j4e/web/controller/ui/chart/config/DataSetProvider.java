package seava.lib.j4e.web.controller.ui.chart.config;

public class DataSetProvider {

	private String resourceName;
	private String filterString;
	private String paramString;
	private String orderBy;

	private String xField;
	private String yField;

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getFilterString() {
		return filterString;
	}

	public void setFilterString(String filterString) {
		this.filterString = filterString;
	}

	public String getParamString() {
		return paramString;
	}

	public void setParamString(String paramString) {
		this.paramString = paramString;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getxField() {
		return xField;
	}

	public void setxField(String xField) {
		this.xField = xField;
	}

	public String getyField() {
		return yField;
	}

	public void setyField(String yField) {
		this.yField = yField;
	}

}
