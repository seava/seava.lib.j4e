package seava.lib.j4e.web.controller.ui.chart;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.marshall.JsonMarshallerFactory;
import seava.lib.j4e.web.controller.ui.chart.config.DataSet;
import seava.lib.j4e.web.controller.ui.chart.config.DataSetProvider;
import seava.lib.j4e.web.controller.ui.chart.config.DefaultChartConfig;

@Controller
@RequestMapping(value = "/chart/google")
public class GoogleChartController extends AbstractChartController {

	@RequestMapping(value = "/{chartType}")
	protected ModelAndView home(@PathVariable String chartType,
			@RequestParam(value = "config", required = true) String config,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Map<String, Object> model = new HashMap<String, Object>();
		this.prepareRequest(request, response);

		// -------------------------------

		// this.authorizeDsAction(resourceName, Constants.DS_ACTION_QUERY,
		// null);
		DefaultChartConfig cfg;
		try {
			cfg = JsonMarshallerFactory.getInstance().readValue(config,
					DefaultChartConfig.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Invalid json expression", e);

		}

		String title = cfg.getTitle();

		// String resourceName = "SimbolValue_Ds";
		// String filterString =
		// "{\"simbol\":\"GOLD\",\"eventDate_From\":\"2001-01-01 00:00:00\",\"eventDate_To\":\"2002-01-01 00:00:00\"}";
		// String paramString = "";
		// String orderBy =
		// "[{\"property\":\"eventDate\",\"direction\":\"ASC\"}]";
		// String orderByCol = "";
		// String orderBySense = "";
		// String filterRulesString = "";
		// String xField = "eventDate";
		// String yField = "closeValue";

		Map<String, DataSet> result = new HashMap<String, DataSet>();

		SimpleDateFormat dateFormat = new SimpleDateFormat(
				Constants.get_server_datetime_format());
		int i = 0;
		for (DataSetProvider dsp : cfg.getDataSetProviders()) {

			i++;
			IDsService<Object, Object, Object> service = this
					.getServiceLocator().findDsService(dsp.getResourceName());

			IDsMarshaller<Object, Object, Object> marshaller = service
					.createMarshaller(IDsMarshaller.JSON);

			Object filter = marshaller.readFilterFromString(dsp
					.getFilterString());
			Object params = marshaller.readParamsFromString(dsp
					.getParamString());

			IQueryBuilder<Object, Object, Object> builder;

			builder = service.createQueryBuilder().addFetchLimit(0, 4500)
					.addFilter(filter).addParams(params);

			if (dsp.getOrderBy() != null && !dsp.getOrderBy().equals("")) {
				List<ISortRule> sortTokens = marshaller.readSortRules(dsp
						.getOrderBy());
				builder.addSortInfo(sortTokens);
			}

			List<?> list = service.find(builder);

			// String xfName = dsp.getxField();
			// String yfName = dsp.getyField();

			Class<?> modelClass = service.getModelClass();

			// Field xf = modelClass.getField(xfName);
			// Field yf = modelClass.getField(yfName);

			Method xgetter = modelClass.getDeclaredMethod("getEventDate",
					new Class<?>[] {});
			Method ygetter = modelClass.getDeclaredMethod("getCloseValue",
					new Class<?>[] {});

			for (Object o : list) {
				Object _xVal = xgetter.invoke(o);
				String xval = _xVal.toString();
				if (_xVal instanceof Date) {
					xval = dateFormat.format(_xVal);
				}

				if (!result.containsKey(xval)) {
					result.put(xval, new DataSet());
				}

				DataSet d = result.get(xval);
				d.setXval(xval);

				Method m = DataSet.class.getDeclaredMethod("setYval" + i,
						Object.class);
				BigDecimal y = (BigDecimal) ygetter.invoke(o);

				if (i == 1 && y != null) {
					y = y.divide(new BigDecimal(50));
				}
				m.invoke(d, y);
			}

		}

		List<DataSet> dataList = new ArrayList<DataSet>();
		for (Map.Entry<String, DataSet> e : result.entrySet()) {
			dataList.add(e.getValue());
		}

		model.put("dataList", dataList);

		model.put("yFields", new String[] { "yval1", "yval2" });

		model.put("title", title);
		// ['${d["x"]?string("yyyy-MM-dd")}', ${d[yField]?string("0.######")},
		// ${d[yField]?string("0.######")} + 10],
		// -------------------------------

		if (chartType.equals("line")) {
			model.put("chart", "google.visualization.LineChart");
		} else if (chartType.equals("pie")) {
			model.put("chart", "google.visualization.PieChart");
		} else if (chartType.equals("bar")) {
			model.put("chart", "google.visualization.BarChart");
		} else if (chartType.equals("column")) {
			model.put("chart", "google.visualization.ColumnChart");
		} else if (chartType.equals("candlestick")) {
			model.put("chart", "google.visualization.CandlestickChart");
		} else if (chartType.equals("geo")) {
			model.put("chart", "google.visualization.GeoChart");
		}

		// -------------------------------

		this.finishRequest();
		return new ModelAndView(this.getViewName(), model);

	}

}
