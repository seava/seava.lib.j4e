/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.web.controller.data;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.web.controller.AbstractBaseController;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public class AbstractDsBaseController<M, F, P> extends AbstractBaseController {

	/**
	 * Lookup a data-source service.
	 * 
	 * @param dsName
	 * @return
	 * @throws ManagedException
	 */
	public IDsService<M, F, P> findDsService(String dsName)
			throws ManagedException {
		return this.getServiceLocator().findDsService(dsName);
	}

	/**
	 * Authorize a DS action.
	 * 
	 * @param dsName
	 * @param action
	 * @throws ManagedException
	 */
	protected void authorizeDsAction(String dsName, String action,
			String rpcName) throws ManagedException {
		this.getAuthorizationFactory().getDsAuthorizationProvider()
				.authorize(dsName, action, rpcName);
	}

}
