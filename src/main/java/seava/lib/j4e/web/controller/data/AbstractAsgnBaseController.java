/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.web.controller.data;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.web.controller.AbstractBaseController;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public abstract class AbstractAsgnBaseController<M, F, P> extends
		AbstractBaseController {

	/**
	 * 
	 * @param resourceName
	 * @return
	 */
	protected String serviceNameFromResourceName(String resourceName) {
		return resourceName;
	}

	/**
	 * 
	 * @param asgnName
	 * @return
	 * @throws ManagedException
	 */
	public IAsgnService<M, F, P> findAsgnService(String asgnName)
			throws ManagedException {
		return this.getServiceLocator().findAsgnService(asgnName);
	}

	/**
	 * Authorize an assignment action.
	 * 
	 * @param asgnName
	 * @param action
	 * @throws Exception
	 */
	protected void authorizeAsgnAction(String asgnName, String action)
			throws ManagedException {
		this.getAuthorizationFactory().getAsgnAuthorizationProvider()
				.authorize(asgnName, action, null);
	}

}
