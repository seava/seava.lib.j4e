package seava.lib.j4e.web.controller.ui.chart;

import seava.lib.j4e.web.controller.AbstractBaseController;

public abstract class AbstractChartController extends AbstractBaseController {

	/**
	 * Html view name
	 */
	protected String viewName;

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}
}
