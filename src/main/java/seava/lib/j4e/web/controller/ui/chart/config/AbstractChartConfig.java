package seava.lib.j4e.web.controller.ui.chart.config;

import java.util.List;
import java.util.Map;

public abstract class AbstractChartConfig {

	private String type;

	private String title;

	private Map<String, Object> attributes;

	private List<DataSetProvider> dataSetProviders;

	public List<DataSetProvider> getDataSetProviders() {
		return dataSetProviders;
	}

	public void setDataSetProviders(List<DataSetProvider> dataSetProviders) {
		this.dataSetProviders = dataSetProviders;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

}
