package seava.lib.j4e.web.controller.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.base.AbstractBase;
import seava.lib.j4e.web.ConstantsWeb;

/**
 * Main user interface controller.
 * 
 * This class will act as a dispatcher for different UI technologies,
 * redirecting to the proper URL based on the client user agent.
 * 
 * Currently it sends a redirect to the Extjs based UI.
 * 
 * @author amathe
 * 
 */
@Controller
public class UiMainController extends AbstractBase {

	/**
	 * Handle root request
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "*", method = RequestMethod.GET)
	protected void home(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.sendRedirect(this.getSettings().get(Constants.PROP_CTXPATH)
				+ ConstantsWeb.URL_UI_EXTJS);

	}
}
