package seava.lib.j4e.web.controller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.api.presenter.service.IReportService;
import seava.lib.j4e.api.web.IServiceLocatorWeb;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * Service locator for the web layer. It is responsible to locate and return a
 * service bean instance from the Spring context given the spring bean alias or
 * the class of the presenter model
 * 
 * @author amathe
 * 
 */
public class ServiceLocatorWeb implements IServiceLocatorWeb {

	final static Logger logger = LoggerFactory
			.getLogger(ServiceLocatorWeb.class);

	private ApplicationContext applicationContext;

	/**
	 * Find a data-source service given the data-source class
	 */
	@Override
	public <M, F, P> IDsService<M, F, P> findDsService(Class<?> modelClass)
			throws ManagedException {
		try {
			return this.findDsService((String) modelClass.getField("ALIAS")
					.get(null));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getLocalizedMessage());
		}
	}

	/**
	 * Find a data-source service given the data-source name
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <M, F, P> IDsService<M, F, P> findDsService(String dsName)
			throws ManagedException {
		if (logger.isDebugEnabled()) {
			logger.debug("Looking for ds-service `" + dsName + "`");
		}
		IDsService<M, F, P> srv = null;

		try {
			srv = (IDsService<M, F, P>) this.applicationContext.getBean(dsName);
			if (srv == null && this.applicationContext.getParent() != null) {
				srv = (IDsService<M, F, P>) this.applicationContext.getParent()
						.getBean(dsName);
			}
		} catch (NoSuchBeanDefinitionException e) {
			// Ignore it for the moment
		} catch (BeansException e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Ds-service `" + dsName + "` not found !");
		}
		return srv;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <M, F, P> IAsgnService<M, F, P> findAsgnService(String asgnName)
			throws ManagedException {
		if (logger.isDebugEnabled()) {
			logger.debug("Looking for assignment service `" + asgnName + "`");
		}

		IAsgnService<M, F, P> srv = (IAsgnService<M, F, P>) this.applicationContext
				.getBean(asgnName);
		if (srv == null && this.applicationContext.getParent() != null) {
			srv = (IAsgnService<M, F, P>) this.applicationContext.getParent()
					.getBean(asgnName);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Assignment service `" + asgnName + "` not found !");
		}
		return srv;
	}

	/**
	 * Find a report service given its spring bean alias factories.
	 * 
	 * @param reportServiceAlias
	 * @return
	 * @throws Exception
	 */
	@Override
	public IReportService findReportService(String reportServiceAlias)
			throws ManagedException {
		if (logger.isDebugEnabled()) {
			logger.debug("Looking for ds-service `" + reportServiceAlias + "`");
		}
		IReportService srv = (IReportService) this.applicationContext
				.getBean(reportServiceAlias);
		if (srv == null && this.applicationContext.getParent() != null) {
			srv = (IReportService) this.applicationContext.getParent().getBean(
					reportServiceAlias);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Report-service `" + reportServiceAlias + "` not found !");
		}
		return srv;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
