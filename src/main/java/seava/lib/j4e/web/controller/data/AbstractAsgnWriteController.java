package seava.lib.j4e.web.controller.data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.web.ConstantsWeb;

public abstract class AbstractAsgnWriteController<M, F, P> extends
		AbstractAsgnReadController<M, F, P> {

	private final static Logger logger = LoggerFactory
			.getLogger(AbstractAsgnWriteController.class);

	/**
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_MOVE_LEFT)
	@ResponseBody
	public String moveLeft(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			@RequestParam(value = "p_selected_ids", required = true) String selectedIds,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_MOVE_LEFT });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={}, selectedIds={} ",
						new Object[] { objectId, selectionId, selectedIds });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "update");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			service.moveLeft(selectionId, this.selectedIdsAsList(selectedIds));
			stopWatch.stop();
			return "";
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_MOVE_RIGHT)
	@ResponseBody
	public String moveRight(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			@RequestParam(value = "p_selected_ids", required = true) String selectedIds,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_MOVE_RIGHT });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={}, selectedIds={} ",
						new Object[] { objectId, selectionId, selectedIds });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "update");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			service.moveRight(selectionId, this.selectedIdsAsList(selectedIds));
			stopWatch.stop();

			return "";
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param dataString
	 * @param paramString
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_MOVE_LEFT_ALL)
	@ResponseBody
	public String moveLeftAll(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			@RequestParam(value = "data", required = false, defaultValue = "{}") String dataString,
			@RequestParam(value = "params", required = false, defaultValue = "{}") String paramString,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_MOVE_LEFT_ALL });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={} data={}",
						new Object[] { objectId, selectionId, dataString });
				logger.debug("  --> request-params: {} ",
						new Object[] { paramString });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "update");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			IDsMarshaller<M, F, P> marshaller = service
					.createMarshaller(dataFormat);

			F filter = marshaller.readFilterFromString(dataString);
			P params = marshaller.readParamsFromString(paramString);

			service.moveLeftAll(selectionId, filter, params);
			stopWatch.stop();

			return "";
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param dataString
	 * @param paramString
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_MOVE_RIGHT_ALL)
	@ResponseBody
	public String moveRightAll(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			@RequestParam(value = "data", required = false, defaultValue = "{}") String dataString,
			@RequestParam(value = "params", required = false, defaultValue = "{}") String paramString,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_MOVE_RIGHT_ALL });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={} data={}",
						new Object[] { objectId, selectionId, dataString });
				logger.debug("  --> request-params: {} ",
						new Object[] { paramString });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "update");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			IDsMarshaller<M, F, P> marshaller = service
					.createMarshaller(dataFormat);

			F filter = marshaller.readFilterFromString(dataString);
			P params = marshaller.readParamsFromString(paramString);

			service.moveRightAll(selectionId, filter, params);
			stopWatch.stop();

			return "";
		} finally {
			this.finishRequest();
		}

	}

	/**
	 * Cancel changes
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_RESET)
	@ResponseBody
	public String reset(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_RESET });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={} ",
						new Object[] { objectId, selectionId });
			}

			this.prepareRequest(request, response);

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			service.reset(selectionId, objectId);
			stopWatch.stop();

			return "";
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * Save changes
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param objectId
	 * @param selectionId
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_SAVE)
	@ResponseBody
	public String save(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_OBJECT_ID, required = true) String objectId,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_SAVE });
			}

			if (logger.isDebugEnabled()) {
				logger.debug(
						"  --> request-filter: objectId={}, selectionId={} ",
						new Object[] { objectId, selectionId });
			}

			this.prepareRequest(request, response);

			this.authorizeAsgnAction(resourceName, "update");

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			service.save(selectionId, objectId);
			stopWatch.stop();

			return "";
		} finally {
			this.finishRequest();
		}
	}

	/**
	 * 
	 * @param resourceName
	 * @param dataFormat
	 * @param selectionId
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST, params = ConstantsWeb.REQUEST_PARAM_ACTION
			+ "=" + ConstantsWeb.ASGN_ACTION_CLEANUP)
	@ResponseBody
	public String delete(
			@PathVariable String resourceName,
			@PathVariable String dataFormat,
			@RequestParam(value = ConstantsWeb.REQUEST_PARAM_ASGN_SELECTION_ID, required = true) String selectionId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {

			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			if (logger.isInfoEnabled()) {
				logger.info("Processing request: {}.{} -> action = {} ",
						new Object[] { resourceName, dataFormat,
								ConstantsWeb.ASGN_ACTION_CLEANUP });
			}

			if (logger.isDebugEnabled()) {
				logger.debug("  --> request-filter: selectionId={} ",
						new Object[] { selectionId });
			}

			this.prepareRequest(request, response);

			IAsgnService<M, F, P> service = this.findAsgnService(this
					.serviceNameFromResourceName(resourceName));

			service.cleanup(selectionId);
			stopWatch.stop();

			return "";
		} finally {
			this.finishRequest();
		}
	}

}
