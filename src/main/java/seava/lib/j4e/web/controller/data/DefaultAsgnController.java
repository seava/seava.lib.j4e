/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.web.controller.data;

import seava.lib.j4e.web.ConstantsWeb;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = ConstantsWeb.SUBPATH_DATA_ASGN
		+ "/{resourceName}.{dataFormat}")
public class DefaultAsgnController<M, F, P> extends
		AbstractAsgnController<M, F, P> {

}
