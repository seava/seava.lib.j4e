package seava.lib.j4e.web.controller.ui.chart.config;

public class DataSet {

	private String xval;

	private Object yval1;
	private Object yval2;
	private Object yval3;
	private Object yval4;

	public String getXval() {
		return xval;
	}

	public void setXval(String xval) {
		this.xval = xval;
	}

	public Object getYval1() {
		return yval1;
	}

	public void setYval1(Object yval1) {
		this.yval1 = yval1;
	}

	public Object getYval2() {
		return yval2;
	}

	public void setYval2(Object yval2) {
		this.yval2 = yval2;
	}

	public Object getYval3() {
		return yval3;
	}

	public void setYval3(Object yval3) {
		this.yval3 = yval3;
	}

	public Object getYval4() {
		return yval4;
	}

	public void setYval4(Object yval4) {
		this.yval4 = yval4;
	}

}
