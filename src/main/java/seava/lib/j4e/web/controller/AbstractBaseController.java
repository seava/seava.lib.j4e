/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.web.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import seava.lib.j4e.api.base.exceptions.IErrorCode;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.ISessionUser;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.web.IServiceLocatorWeb;
import seava.lib.j4e.api.web.security.IAuthorizationFactory;
import seava.lib.j4e.base.AbstractBase;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.web.exceptions.ErrorCodeWeb;
import seava.lib.j4e.web.request.RequestValidatorRemoteAgent;
import seava.lib.j4e.web.request.RequestValidatorRemoteIp;

/**
 * Root of the abstract controllers hierarchy.
 * 
 * @author amathe
 * 
 */
public abstract class AbstractBaseController extends AbstractBase {

	/**
	 * Presenter service locator. May be null, use the getter.
	 */
	private IServiceLocatorWeb serviceLocator;

	/**
	 * Authorization factory. May be null, use the getter.
	 */
	private IAuthorizationFactory authorizationFactory;

	/**
	 * Default transfer buffer size.
	 */
	protected final static int FILE_TRANSFER_BUFFER_SIZE = 4 * 1024;

	/**
	 * Logger
	 */
	final static Logger logger = LoggerFactory
			.getLogger(AbstractBaseController.class);

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws ManagedException
	 */
	protected void prepareRequest(HttpServletRequest request,
			HttpServletResponse response) throws ManagedException {

		ISessionUser sessionUser = null;
		IUser user = null;

		try {
			sessionUser = (ISessionUser) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();

			if (sessionUser.isSessionLocked()) {
				throw new ManagedException(ErrorCodeWeb.SEC_SESSION_LOCKED,
						"Session has been locked.");
			}
			user = sessionUser.getUser();
			if (logger.isDebugEnabled()) {
				if (user.isSystemUser()) {
					logger.debug("Working with system user, no client");
				} else {
					logger.debug("Working with clientId = "
							+ user.getClient().getId());
				}

			}
		} catch (ClassCastException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Not authenticated request denied.");
			}
			throw new ManagedException(ErrorCodeWeb.SEC_SESSION_EXPIRED,
					"<b>Session expired.</b>"
							+ "<br> Logout from application and login again.");
		}

		Session.user.set(user);

		(new RequestValidatorRemoteIp()).validate(request, sessionUser,
				this.getSettings());
		(new RequestValidatorRemoteAgent()).validate(request, sessionUser,
				this.getSettings());

	}

	/**
	 * 
	 * @throws ManagedException
	 */
	protected void finishRequest() throws ManagedException {
		Session.user.set(null);
	}

	/**
	 * Collect parameters from request.
	 * 
	 * ATTENTION!: Only the first value is considered.
	 * 
	 * @param request
	 * @param prefix
	 *            Collect only parameters which start with this prefix
	 * @param suffix
	 *            Collect only parameters which ends with this suffix
	 * @return
	 */
	protected Map<String, String> collectParams(HttpServletRequest request,
			String prefix, String suffix) {

		Map<String, String[]> paramMap = (Map<String, String[]>) request
				.getParameterMap();
		Map<String, String> result = new HashMap<String, String>();
		for (Map.Entry<String, String[]> e : paramMap.entrySet()) {
			String k = e.getKey();
			if (prefix != null && k.startsWith(prefix)) {
				if (suffix != null && k.endsWith(suffix)) {
					result.put(k, e.getValue()[0]);
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	protected ObjectMapper getJsonMapper() throws ManagedException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS,
				false);
		mapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(
				DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

	/* ================= GETTERS - SETTERS ================== */

	/**
	 * Get service locator. If it is null attempts to retrieve it from Spring
	 * context.
	 * 
	 * @return
	 */
	public IServiceLocatorWeb getServiceLocator() {
		if (this.serviceLocator == null) {
			this.serviceLocator = this.getApplicationContext().getBean(
					IServiceLocatorWeb.class);
		}
		return serviceLocator;
	}

	/**
	 * Set presenter service locator.
	 * 
	 * @param serviceLocator
	 */
	public void setServiceLocator(IServiceLocatorWeb serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	/**
	 * Get authorization factory. If it is null attempts to retrieve it from
	 * Spring context.
	 * 
	 * @return
	 * @throws Exception
	 */
	public IAuthorizationFactory getAuthorizationFactory() {
		if (this.authorizationFactory == null) {
			this.authorizationFactory = this.getApplicationContext().getBean(
					IAuthorizationFactory.class);
		}
		return authorizationFactory;
	}

	/**
	 * Set authorization factory.
	 * 
	 * @param authorizationFactory
	 */
	public void setAuthorizationFactory(
			IAuthorizationFactory authorizationFactory) {
		this.authorizationFactory = authorizationFactory;
	}

	/* ================ SEND - FILE ======================== */

	/**
	 * 
	 * @param file
	 * @param outputStream
	 * @throws ManagedException
	 */
	protected void sendFile(File file, ServletOutputStream outputStream)
			throws ManagedException {
		this.sendFile(file, outputStream, FILE_TRANSFER_BUFFER_SIZE);
	}

	/**
	 * 
	 * @param file
	 * @param outputStream
	 * @param bufferSize
	 * @throws ManagedException
	 */
	protected void sendFile(File file, ServletOutputStream outputStream,
			int bufferSize) throws ManagedException {
		try {
			InputStream in = null;
			try {
				in = new BufferedInputStream(new FileInputStream(file));
				byte[] buf = new byte[bufferSize];
				int bytesRead;
				while ((bytesRead = in.read(buf)) != -1) {
					outputStream.write(buf, 0, bytesRead);
				}
			} finally {
				if (in != null)
					in.close();
			}
			outputStream.flush();
		} catch (IOException e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage());
		}
	}

	/**
	 * 
	 * @param inputStream
	 * @param outputStream
	 * @throws ManagedException
	 */
	protected void sendFile(InputStream inputStream,
			ServletOutputStream outputStream) throws ManagedException {
		this.sendFile(inputStream, outputStream, FILE_TRANSFER_BUFFER_SIZE);
	}

	/**
	 * 
	 * @param inputStream
	 * @param outputStream
	 * @param bufferSize
	 * @throws ManagedException
	 */
	protected void sendFile(InputStream inputStream,
			ServletOutputStream outputStream, int bufferSize)
			throws ManagedException {
		try {
			try {
				byte[] buf = new byte[bufferSize];
				int bytesRead;
				while ((bytesRead = inputStream.read(buf)) != -1) {
					outputStream.write(buf, 0, bytesRead);
				}
			} finally {
				if (inputStream != null)
					inputStream.close();
			}
			outputStream.flush();
		} catch (IOException e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage());
		}
	}

	/* ================ EXCEPTION - MANAGEMENT ======================== */

	/**
	 * Generic exception handler
	 * 
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	protected String handleException(Exception e, HttpServletResponse response)
			throws IOException {
		return this.handleManagedException(
				ErrorCodeCommons.RUNTIME_ERROR,
				new ManagedException(ErrorCodeCommons.RUNTIME_ERROR, e
						.getMessage(), e), response);
	}

	/**
	 * Generic exception handler
	 * 
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ExceptionHandler(value = ManagedException.class)
	@ResponseBody
	protected String handleException(ManagedException e,
			HttpServletResponse response) throws IOException {
		return this.handleManagedException(e.getErrorCode(), e, response);
	}

	//
	//
	// /**
	// * Not authenticated
	// *
	// * @param e
	// * @param response
	// * @return
	// * @throws IOException
	// */
	// @ResponseBody
	// protected String handleNotAuthorizedRequestException(
	// NotAuthorizedRequestException e, HttpServletResponse response)
	// throws IOException {
	// response.setStatus(403);
	// if (e.getCause() != null) {
	// response.getOutputStream().print(e.getCause().getMessage());
	// } else {
	// response.getOutputStream().print(e.getMessage());
	// }
	// return null;
	// }

	/**
	 * 
	 * @param errorCode
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	protected String handleManagedException(IErrorCode errorCode,
			ManagedException e, HttpServletResponse response)
			throws IOException {
		return this.handleManagedException(errorCode, e, response, "txt");
	}

	/**
	 * 
	 * @param errorCode
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	protected String handleManagedExceptionAsText(IErrorCode errorCode,
			ManagedException e, HttpServletResponse response)
			throws IOException {
		return this.handleManagedException(errorCode, e, response, "txt");
	}

	/**
	 * 
	 * @param errorCode
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	protected String handleManagedExceptionAsHtml(IErrorCode errorCode,
			ManagedException e, HttpServletResponse response)
			throws IOException {
		return this.handleManagedException(errorCode, e, response, "html");
	}

	/**
	 * 
	 * @param errorCode
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	protected String handleManagedExceptionAsJson(IErrorCode errorCode,
			ManagedException e, HttpServletResponse response)
			throws IOException {
		return this.handleManagedException(errorCode, e, response, "json");
	}

	/**
	 * Exception
	 * 
	 * @param e
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	protected String handleManagedException(IErrorCode errorCode,
			ManagedException e, HttpServletResponse response, String outputType)
			throws IOException {

		IErrorCode err = errorCode;
		Map<String, String> result = new HashMap<String, String>();

		if (err == null) {
			err = ErrorCodeCommons.RUNTIME_ERROR;
			e.printStackTrace();
		}

		result.put("err_group", err.getErrGroup());
		result.put("err_no", err.getErrNo() + "");
		result.put("err_msg", err.getErrMsg());

		// --------------------

		StringBuffer _msg = new StringBuffer();

		Throwable t = e;
		while (t != null) {
			if (t instanceof InvocationTargetException) {
				t = ((InvocationTargetException) t).getTargetException();
				if (t instanceof TransactionSystemException) {
					t = ((TransactionSystemException) t)
							.getApplicationException();
				}
			}
			_msg.append(t.getMessage() + "\n");
			t = t.getCause();
		}

		// ---------------------

		if (!result.containsKey("msg")) {
			result.put("msg", _msg.toString());
		} else {
			result.put("details", _msg.toString());
		}

		StringBuffer sb = new StringBuffer();

		if (outputType.matches("txt")) {
			sb.append(result.get("err_group") + "-" + result.get("err_no")
					+ "\n||\n");
			sb.append(result.get("err_msg") + "\n||\n");
			sb.append(result.get("msg") + "\n||\n");
			sb.append(result.get("details") + "\n||\n");
		} else if (outputType.matches("html")) {
			sb.append("<html><body><div>" + result.get("msg")
					+ "</div></body></html>");
		}

		response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
		response.getOutputStream().print(sb.toString());
		return null;
	}

}
