package seava.lib.j4e.web.controller.ui.chart;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.marshall.JsonMarshallerFactory;
import seava.lib.j4e.web.controller.ui.chart.config.DefaultChartConfig;

@Controller
@RequestMapping(value = "/chart/d3")
public class D3ChartController extends AbstractChartController {

	@RequestMapping(value = "/{chartType}")
	protected ModelAndView home(@PathVariable String chartType,
			@RequestParam(value = "config", required = true) String config,
			HttpServletRequest request, HttpServletResponse response)
			throws ManagedException {

		Map<String, Object> model = new HashMap<String, Object>();
		this.prepareRequest(request, response);

		// -------------------------------

		// this.authorizeDsAction(resourceName, Constants.DS_ACTION_QUERY,
		// null);
		DefaultChartConfig cfg;
		try {
			cfg = JsonMarshallerFactory.getInstance().readValue(config,
					DefaultChartConfig.class);
		} catch (Exception e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Invalid json expression", e);
		}

		String title = cfg.getTitle();

		String resourceName = "SimbolValue_Ds";
		String filterString = "{\"simbol\":\"GOLD\",\"eventDate_From\":\"2001-01-01 00:00:00\",\"eventDate_To\":\"2002-01-01 00:00:00\"}";
		String paramString = "";
		String orderBy = "[{\"property\":\"eventDate\",\"direction\":\"ASC\"}]";
		String orderByCol = "";
		String orderBySense = "";
		String filterRulesString = "";
		String xField = "eventDate";
		String yField = "closeValue";

		// DefaultChartConfig cfg =

		IDsService<Object, Object, Object> service = this.getServiceLocator()
				.findDsService(resourceName);

		IDsMarshaller<Object, Object, Object> marshaller = service
				.createMarshaller(IDsMarshaller.JSON);

		Object filter = marshaller.readFilterFromString(filterString);
		Object params = marshaller.readParamsFromString(paramString);

		IQueryBuilder<Object, Object, Object> builder;

		builder = service.createQueryBuilder().addFetchLimit(0, 4500)
				.addFilter(filter).addParams(params);

		if (orderBy != null && !orderBy.equals("")) {
			List<ISortRule> sortTokens = marshaller.readSortRules(orderBy);
			builder.addSortInfo(sortTokens);
		} else {
			builder.addSortInfo(orderByCol, orderBySense);
		}

		if (filterRulesString != null && !filterRulesString.equals("")) {
			List<IFilterRule> filterRules = marshaller
					.readFilterRules(filterRulesString);
			builder.addFilterRules(filterRules);
		}

		List<?> list = service.find(builder);

		model.put("dataList", list);

		model.put("xField", xField);
		model.put("yField", yField);
		model.put("title", title);

		// -------------------------------

		this.finishRequest();
		return new ModelAndView(this.getViewName(), model);

	}

}
