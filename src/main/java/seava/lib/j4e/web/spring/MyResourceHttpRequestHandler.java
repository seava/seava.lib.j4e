package seava.lib.j4e.web.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

/**
 * Temporary override as workaround for bug: SPR-10999 <br>
 * <br>
 * mvc:resources should support multiple locations by placeholder <br>
 * <br>
 * 
 * https://jira.spring.io/browse/SPR-10999
 * 
 * @author amathe
 * 
 */
public class MyResourceHttpRequestHandler extends ResourceHttpRequestHandler {

	/**
	 * Set a {@code List} of {@code Resource} paths to use as sources for
	 * serving static resources.
	 */
	public void setLocations(List<Resource> locations) {
		List<Resource> processed = new ArrayList<Resource>();

		if (locations != null) {
			for (Resource location : locations) {
				try {
					if (location.getURL().toString().contains(",")) {
						String[] tokens = location.getURL().toString()
								.split(",");
						for (int i = 0; i < tokens.length; i++) {

							Resource _location = new UrlResource(tokens[i]);
							processed.add(_location);
						}
					} else {
						processed.add(location);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				// System.out.println(location.getFilename());
			}
		}
		super.setLocations(processed);
	}
}
