package seava.lib.j4e.web.exceptions;

import seava.lib.j4e.api.base.exceptions.IErrorCode;

/**
 * 
 * @author amathe
 * 
 */
public enum ErrorCodeWeb implements IErrorCode {

	/* ====================== GENERAL 1-899 ============================ */

	//
	// // file access
	//
	FILE_NOT_UPLOADED(17),
	//
	// /* ====================== SECURITY 900-999 ============================
	// */
	//
	// SRV_FILEUPLOAD_SRV_NOT_FOUND(901), SRV_PRESENTER_SRV_NOT_FOUND(902),
	// SRV_BUSINESS_SRV_NOT_FOUND(
	// 903),

	/* ====================== SECURITY 1000-1999 ============================ */

	SEC_NOT_AUTHENTICATED(1001), SEC_NOT_AUTHORIZED(1001),

	SEC_SESSION_LOCKED(1011), SEC_SESSION_EXPIRED(1012),

	SEC_DIFFERENT_IP(1021), SEC_DIFFERENT_USER_AGENT(1022);

	private final int errNo;

	private ErrorCodeWeb(int errNo) {
		this.errNo = errNo;
	}

	public int getErrNo() {
		return errNo;
	}

	public String getErrGroup() {
		return "J4E-WEB";
	}

	public String getErrMsg() {
		return this.name();
	}

}