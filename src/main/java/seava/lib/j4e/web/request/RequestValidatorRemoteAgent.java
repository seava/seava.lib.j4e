package seava.lib.j4e.web.request;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.enums.SysParam;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.ISessionUser;
import seava.lib.j4e.web.exceptions.ErrorCodeWeb;

/**
 * 
 * @author amathe
 * 
 */
public class RequestValidatorRemoteAgent {

	/**
	 * 
	 */
	final static Logger logger = LoggerFactory
			.getLogger(RequestValidatorRemoteAgent.class);

	/**
	 * 
	 * @param request
	 * @param sessionUser
	 * @throws ManagedException
	 */
	public void validate(HttpServletRequest request, ISessionUser sessionUser,
			ISettings settings) throws ManagedException {

		boolean checkAgent = settings
				.getParamAsBoolean(SysParam.CORE_SESSION_CHECK_USER_AGENT
						.name());

		if (checkAgent) {
			if (logger.isDebugEnabled()) {
				logger.debug(SysParam.CORE_SESSION_CHECK_USER_AGENT.name()
						+ " enabled, checking user-agent agianst login user-agent...");
			}
			String agent = request.getHeader("User-Agent");
			if (!sessionUser.getUserAgent().equals(agent)) {
				logger.debug("Request comes from different user-agent as expected. Expected: "
						+ sessionUser.getUserAgent() + ", real " + agent);
				throw new ManagedException(
						ErrorCodeWeb.SEC_DIFFERENT_USER_AGENT,
						"Security settings do not allow to process request. Check log file for details.");
			}
		}
	}
}
