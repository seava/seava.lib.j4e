package seava.lib.j4e.web.request;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.enums.SysParam;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.ISessionUser;
import seava.lib.j4e.web.exceptions.ErrorCodeWeb;

/**
 * 
 * @author amathe
 * 
 */
public class RequestValidatorRemoteIp {

	/**
	 * Logger
	 */
	final static Logger logger = LoggerFactory
			.getLogger(RequestValidatorRemoteIp.class);

	/**
	 * 
	 * @param request
	 * @param sessionUser
	 * @throws ManagedException
	 */
	public void validate(HttpServletRequest request, ISessionUser sessionUser,
			ISettings settings) throws ManagedException {

		boolean checkIp = settings
				.getParamAsBoolean(SysParam.CORE_SESSION_CHECK_IP.name());

		if (checkIp) {
			if (logger.isDebugEnabled()) {
				logger.debug(SysParam.CORE_SESSION_CHECK_IP.name()
						+ " enabled, checking IP against login IP...");
			}
			String ip = request.getHeader("X-Forwarded-For");
			if (ip != null && !"".equals(ip)) {
				ip = ip.substring(0, ip.indexOf(","));
			} else {
				ip = request.getRemoteAddr();
			}

			if (!sessionUser.getRemoteIp().equals(ip)) {
				logger.debug("Request comes from different IP as expected. Expected: "
						+ sessionUser.getRemoteIp() + ", real " + ip);
				throw new ManagedException(
						ErrorCodeWeb.SEC_DIFFERENT_IP,
						"Security settings do not allow to process request. Check log file for details.");
			}
		}

	}
}
