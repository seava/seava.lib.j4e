/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.web;

/**
 * @author amathe
 * 
 */
public class ConstantsWeb {

	/* Servlet paths defined in web.xml */

	public static final String SERVLETPATH_SESSION = "/session";
	public static final String SERVLETPATH_UI = "/ui";
	public static final String SERVLETPATH_DATA = "/data";
	public static final String SERVLETPATH_STATICS = "/statics";
	public static final String SERVLETPATH_UPLOAD = "/upload";
	public static final String SERVLETPATH_DOWNLOAD = "/download";

	/* Sub-servlet paths */

	public static final String SUBPATH_DATA_ASGN = "/asgn";
	public static final String SUBPATH_DATA_DS = "/ds";
	public static final String SUBPATH_UI_EXTJS = "/extjs";
	public static final String SUBPATH_UI_STOUCH = "/stouch";

	/* URL fragments */
	public static final String URL_SESSION = SERVLETPATH_SESSION;
	public static final String URL_UI = SERVLETPATH_UI;
	public static final String URL_UI_EXTJS = SERVLETPATH_UI + SUBPATH_UI_EXTJS;
	public static final String URL_UI_STOUCH = SERVLETPATH_UI + SUBPATH_UI_STOUCH;
	public static final String URL_DATA_DS = SERVLETPATH_DATA + SUBPATH_DATA_DS;
	public static final String URL_DATA_ASGN = SERVLETPATH_DATA + SUBPATH_DATA_ASGN;
	public static final String URL_UPLOAD = SERVLETPATH_UPLOAD;
	public static final String URL_DOWNLOAD = SERVLETPATH_DOWNLOAD;

	/* =========================================================== */
	/* ====================== cookie name ======================== */
	/* =========================================================== */

	public static final String COOKIE_NAME_THEME = "app-theme";
	public static final String COOKIE_NAME_LANG = "app-lang";

	/* =========================================================== */
	/* =================== request parameters ==================== */
	/* =========================================================== */

	public static final String REQUEST_PARAM_THEME = "theme";
	public static final String REQUEST_PARAM_LANG = "lang";
	public static final String REQUEST_PARAM_ACTION = "action";
	public static final String REQUEST_PARAM_DATA = "data";
	public static final String REQUEST_PARAM_FILTER = "data";
	public static final String REQUEST_PARAM_ADVANCED_FILTER = "filter";
	public static final String REQUEST_PARAM_PARAMS = "params";
	public static final String REQUEST_PARAM_SORT = "orderByCol";
	public static final String REQUEST_PARAM_SENSE = "orderBySense";
	public static final String REQUEST_PARAM_START = "resultStart";
	public static final String REQUEST_PARAM_SIZE = "resultSize";
	public static final String REQUEST_PARAM_ORDERBY = "orderBy";
	public static final String REQUEST_PARAM_EXPORT_INFO = "export_info";
	public static final String REQUEST_PARAM_EXPORT_DOWNLOAD = "download";

	public static final String REQUEST_PARAM_SERVICE_NAME_PARAM = "rpcName";
	public static final String REQUEST_PARAM_ASGN_OBJECT_ID = "objectId";
	public static final String REQUEST_PARAM_ASGN_SELECTION_ID = "selectionId";

	/* =========================================================== */
	/* =============== Request parameter action ================== */
	/* =========================================================== */

	public static final String DS_ACTION_INFO = "info";
	public static final String DS_ACTION_QUERY = "find";
	public static final String DS_ACTION_INSERT = "insert";
	public static final String DS_ACTION_UPDATE = "update";
	public static final String DS_ACTION_DELETE = "delete";
	public static final String DS_ACTION_SAVE = "save";
	public static final String DS_ACTION_IMPORT = "import";
	public static final String DS_ACTION_EXPORT = "export";
	public static final String DS_ACTION_PRINT = "print";
	public static final String DS_ACTION_RPC = "rpc";
	public static final String DS_ACTION_RPC_TYPE = "rpcType";
	public static final String DS_ACTION_RPC_TYPE_DATA = "data";
	public static final String DS_ACTION_RPC_TYPE_FILTER = "filter";
	public static final String DS_ACTION_RPC_TYPE_DATALIST = "dataList";
	public static final String DS_ACTION_RPC_TYPE_IDLIST = "idList";

	public static final String ASGN_ACTION_QUERY_LEFT = "findLeft";
	public static final String ASGN_ACTION_QUERY_RIGHT = "findRight";
	public static final String ASGN_ACTION_MOVE_LEFT = "moveLeft";
	public static final String ASGN_ACTION_MOVE_RIGHT = "moveRight";
	public static final String ASGN_ACTION_MOVE_LEFT_ALL = "moveLeftAll";
	public static final String ASGN_ACTION_MOVE_RIGHT_ALL = "moveRightAll";
	public static final String ASGN_ACTION_SETUP = "setup";
	public static final String ASGN_ACTION_RESET = "reset";
	public static final String ASGN_ACTION_SAVE = "save";
	public static final String ASGN_ACTION_CLEANUP = "cleanup";

	public static final String SESSION_ACTION_SHOW_LOGIN = "login";
	public static final String SESSION_ACTION_LOGIN = "doLogin";
	public static final String SESSION_ACTION_AJAXLOGIN = "doAjaxLogin";
	public static final String SESSION_ACTION_LOGOUT = "doLogout";
	public static final String SESSION_ACTION_LOCK = "doLock";
	public static final String SESSION_ACTION_CHANGEPASSWORD = "changePassword";

}
