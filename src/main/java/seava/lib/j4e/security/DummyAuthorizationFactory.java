/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.security;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.web.security.IAuthorization;
import seava.lib.j4e.api.web.security.IAuthorizationFactory;

/**
 * 
 * @author amathe
 * 
 */
public class DummyAuthorizationFactory implements IAuthorizationFactory,
		ApplicationContextAware {

	/**
	 * Spring application context
	 */
	private ApplicationContext applicationContext;

	/**
	  * 
	  */
	public IAuthorization getAsgnAuthorizationProvider() {
		return (IAuthorization) this.applicationContext
				.getBean(DummyAuthorizationForAsgn.class);
	}

	/**
	 * 
	 */
	public IAuthorization getDsAuthorizationProvider() {
		return (IAuthorization) this.applicationContext
				.getBean(DummyAuthorizationForDs.class);
	}

	/**
	 * 
	 */
	public IAuthorization getJobAuthorizationProvider() {
		return (IAuthorization) this.applicationContext
				.getBean(DummyAuthorizationForJob.class);
	}

	/**
	 * 
	 * @return
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * 
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
