/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.security;

import java.util.List;

/**
 * 
 * @author amathe
 * 
 */
public class SystemAdministratorUser {

	/**
	 * User code
	 */
	private final String code;

	/**
	 * User full name
	 */
	private final String name;

	/**
	 * Login name
	 */
	private final String loginName;

	/**
	 * Password
	 */
	private final String password;

	/**
	 * List of roles assigned to user
	 */
	private List<String> roles;

	public SystemAdministratorUser(String code, String name, String loginName,
			String password) {
		super();
		this.code = code;
		this.name = name;
		this.loginName = loginName;
		this.password = password;

	}

	public String getCode() {
		return code;
	}

	public String getLoginName() {
		return loginName;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
