/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.security;

import java.util.Date;

import seava.lib.j4e.api.Constants;

import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.IClient;
import seava.lib.j4e.api.base.session.ISessionUser;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.IUserProfile;
import seava.lib.j4e.api.base.session.IUserSettings;
import seava.lib.j4e.api.base.session.IWorkspace;
import seava.lib.j4e.api.web.security.IAuthenticationSystemUserService;
import seava.lib.j4e.api.web.security.ILoginParams;
import seava.lib.j4e.api.web.security.LoginParamsHolder;
import seava.lib.j4e.base.session.AppClient;
import seava.lib.j4e.base.session.AppUser;
import seava.lib.j4e.base.session.AppUserProfile;
import seava.lib.j4e.base.session.AppUserSettings;
import seava.lib.j4e.base.session.AppWorkspace;
import seava.lib.j4e.base.session.SessionUser;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Authenticates a system user from the {@link SystemAdministratorUsers}
 * 
 * @author amathe
 * 
 */
public class DefaultAuthenticationSystemUserService implements
		IAuthenticationSystemUserService {

	/**
	 * System users repository
	 */
	private SystemAdministratorUsers repository;

	/**
	 * Application settings provider
	 */
	private ISettings settings;

	/**
	 * 
	 */
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		SystemAdministratorUser u = this.repository.findByUserName(username);
		if (u == null) {
			throw new UsernameNotFoundException("Bad credentials");
		}
		ILoginParams lp = LoginParamsHolder.params.get();

		IClient client = new AppClient(null, null, null);
		IUserSettings settings;
		try {
			settings = AppUserSettings.newInstance(this.settings);
		} catch (ManagedException e) {
			throw new UsernameNotFoundException(e.getMessage());
		}
		IUserProfile profile = new AppUserProfile(true, u.getRoles(), false,
				false, false);

		String workspacePath = this.getSettings().get(Constants.PROP_WORKSPACE);

		IWorkspace ws = new AppWorkspace(workspacePath);

		IUser user = new AppUser(u.getCode(), u.getName(), u.getLoginName(),
				u.getPassword(), client, settings, profile, ws, true);

		ISessionUser su = new SessionUser(user, lp.getUserAgent(), new Date(),
				lp.getRemoteHost(), lp.getRemoteIp());
		return su;
	}

	/* ================== GETTERS - SETTERS ================= */

	/**
	 * 
	 * @return
	 */
	public SystemAdministratorUsers getRepository() {
		return repository;
	}

	/**
	 * 
	 * @param repository
	 */
	public void setRepository(SystemAdministratorUsers repository) {
		this.repository = repository;
	}

	/**
	 * 
	 * @return
	 */
	public ISettings getSettings() {
		return settings;
	}

	/**
	 * 
	 * @param settings
	 */
	public void setSettings(ISettings settings) {
		this.settings = settings;
	}

}
