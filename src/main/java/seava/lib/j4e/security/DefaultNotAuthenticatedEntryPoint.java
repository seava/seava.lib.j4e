/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.base.AbstractBase;
import seava.lib.j4e.web.ConstantsWeb;

/**
 * Handle not authenticated requests.
 * 
 * @author amathe
 * 
 */
public class DefaultNotAuthenticatedEntryPoint extends AbstractBase implements
		AuthenticationEntryPoint {

	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {

		String servletPath = request.getServletPath();
		String pathInfo = request.getPathInfo();

		boolean redirect = servletPath.matches(ConstantsWeb.SERVLETPATH_UI)
				&& (pathInfo == null || pathInfo.matches("/")
						|| pathInfo.matches(ConstantsWeb.SUBPATH_UI_EXTJS) || pathInfo
							.matches(ConstantsWeb.SUBPATH_UI_EXTJS + "/"));

		if (redirect) {
			response.sendRedirect(this.getSettings().get(
					Constants.PROP_LOGIN_PAGE));
		} else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.getWriter().write("Not authenticated");
			response.flushBuffer();
		}

	}
}
