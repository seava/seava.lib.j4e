/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.security;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.web.security.IAuthorization;

/**
 * 
 * @author amathe
 * 
 */
public class DummyAuthorizationForAsgn implements IAuthorization {

	/**
	 * 
	 */
	public void authorize(String dsName, String action, String rpcMethod)
			throws ManagedException {
		// If it doesn't throw exception is authorized
	}

}
