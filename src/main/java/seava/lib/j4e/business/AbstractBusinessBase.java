/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.IServiceLocatorBusiness;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.base.AbstractBase;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.business.service.AbstractBusinessDelegate;

/**
 * Root abstract class for business service hierarchy. It provides support for
 * the sub-classes with the generally needed elements like an
 * applicationContext, system configuration parameters, workflow services etc.
 * 
 * 
 * @author amathe
 * 
 */
public abstract class AbstractBusinessBase extends
		AbstractBase {

	private IServiceLocatorBusiness serviceLocator;

	// private ProcessEngine workflowEngine;

	@PersistenceContext
	@Autowired
	private EntityManager entityManager;

	/**
	 * Lookup an entity service.
	 * 
	 * @param <T>
	 * @param entityClass
	 * @return
	 * @throws ManagedException
	 */
	public <T> IEntityService<T> findEntityService(Class<T> entityClass)
			throws ManagedException {
		return this.getServiceLocator().findEntityService(entityClass);
	}

	/**
	 * Return a new instance of a business delegate by the given class.
	 * 
	 * @param <T>
	 * @param clazz
	 * @return
	 * @throws ManagedException
	 */
	public <T extends AbstractBusinessDelegate> T getBusinessDelegate(
			Class<T> clazz) throws ManagedException {
		T delegate;
		try {
			delegate = clazz.newInstance();
		} catch (Exception e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot create a new instance of  "
							+ clazz.getCanonicalName(), e);
		}
		delegate.setApplicationContext(this.getApplicationContext());
		delegate.setEntityManager(this.getEntityManager());
		return delegate;
	}

	/**
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	/**
	 * @param entityManager
	 *            the entity manager to set
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Get business service locator. If it is null attempts to retrieve it
	 * 
	 * @return
	 */
	public IServiceLocatorBusiness getServiceLocator() {
		if (this.serviceLocator == null) {
			this.serviceLocator = this.getApplicationContext().getBean(
					IServiceLocatorBusiness.class);
		}
		return serviceLocator;
	}

	/**
	 * Set business service locator.
	 * 
	 * @param serviceLocator
	 */
	public void setServiceLocator(IServiceLocatorBusiness serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	/**
	 * 
	 * @param to
	 * @param content
	 */
	protected void sendMessage(String to, Object content) {
		Message<Object> message = MessageBuilder.withPayload(content).build();
		this.getApplicationContext().getBean(to, MessageChannel.class)
				.send(message);
	}

	/**
	 * 
	 * @param entities
	 * @return
	 */
	protected List<Object> collectIds(List<? extends IModelWithId<?>> entities) {
		List<Object> result = new ArrayList<Object>();
		for (IModelWithId<?> e : entities) {
			result.add(e.getId());
		}
		return result;
	}

	//
	// public ProcessEngine getWorkflowEngine() throws ManagedException {
	// if (this.workflowEngine == null) {
	// try {
	// this.workflowEngine = (ProcessEngine) this
	// .getApplicationContext()
	// .getBean(IActivitiProcessEngineHolder.class)
	// .getProcessEngine();
	// } catch (Exception e) {
	// throw new ManagedException(ErrorCode.G_RUNTIME_ERROR,
	// "Cannot get the Activiti workflow engine.", e);
	// }
	// }
	// return this.workflowEngine;
	// }
	//
	// public RuntimeService getWorkflowRuntimeService() throws ManagedException
	// {
	// return this.getWorkflowEngine().getRuntimeService();
	// }
	//
	// public TaskService getWorkflowTaskService() throws ManagedException {
	// return this.getWorkflowEngine().getTaskService();
	// }
	//
	// public RepositoryService getWorkflowRepositoryService()
	// throws ManagedException {
	// return this.getWorkflowEngine().getRepositoryService();
	// }
	//
	// public HistoryService getWorkflowHistoryService() throws ManagedException
	// {
	// return this.getWorkflowEngine().getHistoryService();
	// }
	//
	// public FormService getWorkflowFormService() throws ManagedException {
	// return this.getWorkflowEngine().getFormService();
	// }
	//
	// public void doStartWfProcessInstanceByKey(String processDefinitionKey,
	// String businessKey, Map<String, Object> variables)
	// throws ManagedException {
	// this.getWorkflowRuntimeService().startProcessInstanceByKey(
	// processDefinitionKey, businessKey, variables);
	// }
	//
	// public void doStartWfProcessInstanceById(String processDefinitionId,
	// String businessKey, Map<String, Object> variables)
	// throws ManagedException {
	// this.getWorkflowRuntimeService().startProcessInstanceById(
	// processDefinitionId, businessKey, variables);
	// }
	//
	// public void doStartWfProcessInstanceByMessage(String messageName,
	// String businessKey, Map<String, Object> processVariables)
	// throws ManagedException {
	// this.getWorkflowRuntimeService().startProcessInstanceByMessage(
	// messageName, businessKey, processVariables);
	// }

}
