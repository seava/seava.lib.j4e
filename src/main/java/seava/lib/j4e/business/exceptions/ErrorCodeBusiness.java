package seava.lib.j4e.business.exceptions;

import seava.lib.j4e.api.base.exceptions.IErrorCode;

public enum ErrorCodeBusiness implements IErrorCode {

	RUNTIME_ERROR(1);

	private final int errNo;

	private ErrorCodeBusiness(int errNo) {
		this.errNo = errNo;
	}

	public int getErrNo() {
		return errNo;
	}

	public String getErrGroup() {
		return "J4E-WEB";
	}

	public String getErrMsg() {
		return this.name();
	}
}
