/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service;

import seava.lib.j4e.business.AbstractBusinessBase;

/**
 * Root abstract class for business service hierarchy. It provides support for
 * the sub-classes with the generally needed elements like an
 * applicationContext, system configuration parameters, workflow services etc.
 * 
 * 
 * @author amathe
 * 
 */
public abstract class AbstractBusinessBaseService extends AbstractBusinessBase {

}
