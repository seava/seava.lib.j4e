/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.IServiceLocatorBusiness;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * Service locator utility methods.
 * 
 * @author amathe
 */
public class ServiceLocatorBusiness implements IServiceLocatorBusiness {

	final static Logger logger = LoggerFactory
			.getLogger(ServiceLocatorBusiness.class);

	/**
	 * Spring application context
	 */
	private ApplicationContext applicationContext;

	/**
	 * Find an entity service given the entity class.
	 * 
	 * @param <E>
	 * @param entityClass
	 * @return
	 * @throws ManagedException
	 */
	@SuppressWarnings("unchecked")
	public <E> IEntityService<E> findEntityService(Class<E> entityClass)
			throws ManagedException {

		String serviceAlias = entityClass.getSimpleName();

		try {
			serviceAlias = (String) entityClass.getField("ALIAS").get(null);
		} catch (Exception e) {
			// ignore, proceed with class simple name
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Looking for entity-service `" + serviceAlias + "`");
		}

		IEntityService<E> srv = (IEntityService<E>) this.applicationContext
				.getBean(serviceAlias);
		if (srv == null && this.applicationContext.getParent() != null) {
			srv = (IEntityService<E>) this.applicationContext.getParent()
					.getBean(serviceAlias);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.SERVICE_NOT_FOUND,
					"Entity service `" + serviceAlias + "` not found",
					new String[] { serviceAlias });
		}
		return srv;
	}

	/**
	 * Getter for the spring application context.
	 * 
	 * @return
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * Setter for the spring application context.
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
