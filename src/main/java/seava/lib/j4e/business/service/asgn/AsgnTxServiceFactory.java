/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service.asgn;

import seava.lib.j4e.api.business.service.IAsgnTxService;
import seava.lib.j4e.api.business.service.IAsgnTxServiceFactory;
import seava.lib.j4e.base.AbstractBase;

/**
 * 
 * @author amathe
 * 
 */
public class AsgnTxServiceFactory extends AbstractBase
		implements IAsgnTxServiceFactory {

	private String name;

	@Override
	public <E> IAsgnTxService<E> create(String key) {
		@SuppressWarnings("unchecked")
		IAsgnTxService<E> s = (IAsgnTxService<E>) this.getApplicationContext()
				.getBean(key, IAsgnTxService.class);
		return s;
	}

	public <E> IAsgnTxService<E> create(Class<E> type) {
		@SuppressWarnings("unchecked")
		IAsgnTxService<E> s = (IAsgnTxService<E>) this.getApplicationContext()
				.getBean(type);
		return s;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
