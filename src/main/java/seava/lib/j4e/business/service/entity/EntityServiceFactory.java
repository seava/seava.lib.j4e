/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service.entity;

import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.business.service.IEntityServiceFactory;
import seava.lib.j4e.base.AbstractBase;

/**
 * 
 * @author amathe
 * 
 */
public class EntityServiceFactory extends AbstractBase
		implements IEntityServiceFactory {

	public <E> IEntityService<E> create(String key) {
		@SuppressWarnings("unchecked")
		IEntityService<E> s = (IEntityService<E>) this.getApplicationContext()
				.getBean(key);
		return s;
	}

	public <E> IEntityService<E> create(Class<E> type) {
		@SuppressWarnings("unchecked")
		IEntityService<E> s = (IEntityService<E>) this.getApplicationContext()
				.getBean(type);
		return s;
	}
}
