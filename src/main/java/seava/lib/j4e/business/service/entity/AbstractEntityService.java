/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service.entity;

import seava.lib.j4e.api.business.service.IEntityService;

 

/**
 * Top level abstract class for an entity service. Usually it is extended by
 * custom entity services to inherit all the standard functionality and just
 * customize the non-standard behavior.
 * 
 * See the super-classes for more details.
 * 
 * @author amathe
 * 
 * @param <E>
 */
public abstract class AbstractEntityService<E> extends
		AbstractEntityWriteService<E> implements IEntityService<E> {

}
