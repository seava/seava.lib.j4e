/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service.asgn;

import seava.lib.j4e.api.business.service.IAsgnTxService;

public class DefaultAsgnTxService<E> extends AbstractAsgnTxService<E> implements
		IAsgnTxService<E> {

	public DefaultAsgnTxService() {
	}

	public DefaultAsgnTxService(Class<E> entityClass) {
		this.setEntityClass(entityClass);
	}

}