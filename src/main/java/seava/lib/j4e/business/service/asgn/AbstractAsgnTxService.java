/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.business.service.asgn;

import java.util.List;
import java.util.UUID;

import org.springframework.transaction.annotation.Transactional;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.descriptor.IAsgnContext;
import seava.lib.j4e.business.service.AbstractBusinessBaseService;

public abstract class AbstractAsgnTxService<E> extends
		AbstractBusinessBaseService {

	private Class<E> entityClass;

	protected final String ASGN_TEMP_TABLE = "TEMP_ASGN";

	protected final String ASGNLINE_TEMP_TABLE = "TEMP_ASGN_LINE";

	protected boolean saveAsSqlInsert = true;

	/**
	 * Add the specified list of IDs to the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	@Transactional
	public void doMoveRight(IAsgnContext ctx, String selectionId,
			List<String> ids) throws ManagedException {
		StringBuffer sb = new StringBuffer("('-1'");
		for (String id : ids) {
			sb.append(",'" + id + "'");
		}
		sb.append(")");

		this.getEntityManager()
				.createNativeQuery(
						"insert into " + this.ASGNLINE_TEMP_TABLE
								+ " (selection_id, itemId)" + " select ?, "
								+ ctx.getLeftPkField() + " from "
								+ ctx.getLeftTable() + " r where r."
								+ ctx.getLeftPkField() + "  in "
								+ sb.toString()).setParameter(1, selectionId)
				.executeUpdate();
		this.getEntityManager().flush();
	}

	/**
	 * Add all the available values to the selected ones.
	 * 
	 * @throws ManagedException
	 */
	@Transactional
	public void doMoveRightAll(IAsgnContext ctx, String selectionId)
			throws ManagedException {
		doMoveLeftAll(ctx, selectionId);
		this.getEntityManager()
				.createNativeQuery(
						"insert into " + this.ASGNLINE_TEMP_TABLE
								+ " ( selection_id, itemId)" + " select  ?,  "
								+ ctx.getLeftPkField() + "  from "
								+ ctx.getLeftTable() + " ")
				.setParameter(1, selectionId).executeUpdate();
		this.getEntityManager().flush();
	}

	/**
	 * Remove the specified list of IDs from the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	@Transactional
	public void doMoveLeft(IAsgnContext ctx, String selectionId,
			List<String> ids) throws ManagedException {
		StringBuffer sb = new StringBuffer("('-1'");
		for (String id : ids) {
			sb.append(",'" + id + "'");
		}
		sb.append(")");
		this.getEntityManager()
				.createNativeQuery(
						"delete from " + this.ASGNLINE_TEMP_TABLE
								+ " WHERE  selection_id = ? and itemId in "
								+ sb.toString() + "")
				.setParameter(1, selectionId).executeUpdate();
		this.getEntityManager().flush();

	}

	/**
	 * Remove all the selected values.
	 * 
	 * @throws ManagedException
	 */
	@Transactional
	public void doMoveLeftAll(IAsgnContext ctx, String selectionId)
			throws ManagedException {
		this.getEntityManager()
				.createNativeQuery(
						"delete from " + this.ASGNLINE_TEMP_TABLE
								+ " WHERE selection_id = ?")
				.setParameter(1, selectionId).executeUpdate();
		this.getEntityManager().flush();
	}

	/**
	 * Initialize the temporary table with the existing selection. Creates a
	 * record in the TEMP_ASGN table and the existing selections in
	 * TEMP_ASGN_LINE.
	 * 
	 * @return the UUID of the selection
	 * @throws ManagedException
	 */
	@Transactional
	public String doSetup(IAsgnContext ctx, String asgnName, String objectId)
			throws ManagedException {
		String selectionId = UUID.randomUUID().toString();
		this.getEntityManager()
				.createNativeQuery(
						"insert into " + this.ASGN_TEMP_TABLE
								+ " (id, asgn) values( ? ,?  ) ")
				.setParameter(1, selectionId).setParameter(2, asgnName)
				.executeUpdate();
		this.getEntityManager().flush();
		this.doReset(ctx, selectionId, objectId);
		return selectionId;
	}

	/**
	 * Clean-up the temporary tables.
	 * 
	 * @throws ManagedException
	 */
	@Transactional
	public void doCleanup(IAsgnContext ctx, String selectionId)
			throws ManagedException {
		this.getEntityManager()
				.createNativeQuery(
						"delete from " + this.ASGNLINE_TEMP_TABLE
								+ "  WHERE   selection_id = ? ")
				.setParameter(1, selectionId).executeUpdate();
		this.getEntityManager()
				.createNativeQuery(
						"delete from " + this.ASGN_TEMP_TABLE
								+ "   WHERE id = ? ")
				.setParameter(1, selectionId).executeUpdate();
		this.getEntityManager().flush();
	}

	/**
	 * Restores all the changes made by the user in the TEMP_ASGN_LINE table to
	 * the initial state.
	 * 
	 * @throws ManagedException
	 */
	@Transactional
	public void doReset(IAsgnContext ctx, String selectionId, String objectId)
			throws ManagedException {
		this.getEntityManager()
				.createNativeQuery(
						"delete from " + this.ASGNLINE_TEMP_TABLE
								+ " where selection_id = ? ")
				.setParameter(1, selectionId).executeUpdate();
		this.getEntityManager().flush();

		this.getEntityManager()
				.createNativeQuery(
						"insert into " + this.ASGNLINE_TEMP_TABLE
								+ " (selection_id, itemId)" + " select ?, "
								+ ctx.getRightItemIdField() + " from "
								+ ctx.getRightTable() + " where "
								+ ctx.getRightObjectIdField() + " = ? ")
				.setParameter(1, selectionId).setParameter(2, objectId)
				.executeUpdate();
		this.getEntityManager().flush();
	}

	/**
	 * 
	 * @param ctx
	 * @param selectionId
	 * @param objectId
	 * @throws ManagedException
	 */
	@Transactional
	public void doSave(IAsgnContext ctx, String selectionId, String objectId)
			throws ManagedException {
		this.getEntityManager()
				.createNativeQuery(
						"delete from " + ctx.getRightTable() + " where  "
								+ ctx.getRightObjectIdField() + " = ? ")
				.setParameter(1, objectId).executeUpdate();
		this.getEntityManager().flush();
		if (this.saveAsSqlInsert) {
			this.getEntityManager()
					.createNativeQuery(
							"insert into " + ctx.getRightTable() + " ( "
									+ ctx.getRightObjectIdField() + ",  "
									+ ctx.getRightItemIdField() + " ) "
									+ " select ?, itemId from  "
									+ this.ASGNLINE_TEMP_TABLE + " "
									+ "  where selection_id = ? ")
					.setParameter(1, objectId).setParameter(2, selectionId)
					.executeUpdate();
		} else {
			@SuppressWarnings("unchecked")
			List<Long> list = this
					.getEntityManager()
					.createNativeQuery(
							" select itemId from  " + this.ASGNLINE_TEMP_TABLE
									+ " " + "  where selection_id = ? ")
					.setParameter(1, selectionId).getResultList();
			this.onSave(list);
		}
	}

	protected void onSave(List<Long> ids) throws ManagedException {
	}

	// ==================== getters- setters =====================

	public Class<E> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

}
