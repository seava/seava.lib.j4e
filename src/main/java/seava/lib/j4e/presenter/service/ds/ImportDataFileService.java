/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service.ds;

import seava.lib.j4e.api.base.descriptor.IImportDataFile;
import seava.lib.j4e.api.presenter.service.IImportDataFileService;
import seava.lib.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * 
 * @author amathe
 * 
 */
public class ImportDataFileService extends AbstractPresenterBaseService
		implements IImportDataFileService {

	/**
	 * Import one data-file from a data-package. Locate the proper ds-service to
	 * delegate the work to
	 */
	public void execute(IImportDataFile dataFile) throws Exception {

		String dsName = dataFile.getDs();
		String fileName = dataFile.getFile();

		if (dataFile.getUkFieldName() != null
				&& !dataFile.getUkFieldName().equals("")) {
			this.findDsService(dsName).doImport(fileName,
					dataFile.getUkFieldName(), 0, null);
		} else {
			this.findDsService(dsName).doImport(fileName, null);
		}
	}
}