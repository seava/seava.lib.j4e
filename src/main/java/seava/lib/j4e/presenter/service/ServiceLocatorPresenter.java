/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IAsgnTxService;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.IServiceLocatorPresenter;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * Service locator utility methods.
 * 
 * @author amathe
 */
public class ServiceLocatorPresenter implements IServiceLocatorPresenter {

	final static Logger logger = LoggerFactory
			.getLogger(ServiceLocatorPresenter.class);

	private ApplicationContext applicationContext;

	/**
	 * 
	 */
	@Override
	public <M, F, P> IDsService<M, F, P> findDsService(Class<?> modelClass)
			throws ManagedException {
		try {
			return this.findDsService((String) modelClass.getField("ALIAS")
					.get(null));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getLocalizedMessage());
		}
	}

	/**
	 * Find a data-source service given the data-source name
	 * 
	 * @param <M>
	 * @param <P>
	 * @param dsName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <M, F, P> IDsService<M, F, P> findDsService(String dsName)
			throws ManagedException {
		if (logger.isDebugEnabled()) {
			logger.debug("Looking for ds-service `" + dsName + "`");
		}
		IDsService<M, F, P> srv = (IDsService<M, F, P>) this.applicationContext
				.getBean(dsName);
		if (srv == null && this.applicationContext.getParent() != null) {
			srv = (IDsService<M, F, P>) this.applicationContext.getParent()
					.getBean(dsName);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Ds-service `" + dsName + "` not found !");
		}
		return srv;
	}

	// /**
	// * Find a report service given its spring bean alias factories.
	// *
	// * @param reportServiceAlias
	// * @return
	// * @throws Exception
	// */
	// @Override
	// public IReportService findReportService(String reportServiceAlias)
	// throws ManagedException {
	// if (logger.isDebugEnabled()) {
	// logger.debug("Looking for ds-service `" + reportServiceAlias + "`");
	// }
	// IReportService srv = (IReportService) this.applicationContext
	// .getBean(reportServiceAlias);
	// if (srv == null && this.applicationContext.getParent() != null) {
	// srv = (IReportService) this.applicationContext.getParent().getBean(
	// reportServiceAlias);
	// }
	//
	// if (srv == null) {
	// throw new Exception("Report-service `" + reportServiceAlias
	// + "` not found !");
	// }
	// return srv;
	// }

	/**
	 * Find an entity service given the entity class and a list of factories.
	 * 
	 * @param <E>
	 * @param entityClass
	 * @param factories
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <E> IEntityService<E> findEntityService(Class<E> entityClass)
			throws ManagedException {
		String serviceAlias = entityClass.getSimpleName();

		try {
			serviceAlias = (String) entityClass.getField("ALIAS").get(null);
		} catch (Exception e) {
			// ignore, proceed with class simple name
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Looking for entity-service `" + serviceAlias + "`");
		}

		IEntityService<E> srv = null;
		try {
			srv = (IEntityService<E>) this.applicationContext
					.getBean(serviceAlias);
			if (srv == null && this.applicationContext.getParent() != null) {
				srv = (IEntityService<E>) this.applicationContext.getParent()
						.getBean(serviceAlias);
			}
		} catch (NoSuchBeanDefinitionException e) {
			// Ignore it for the moment
		} catch (BeansException e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Entity service `" + serviceAlias + "` not found");
		}
		return srv;
	}

	/**
	 * Find an assignment service given the service name and the list of
	 * factories.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param asgnName
	 * @param factories
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <M, F, P> IAsgnService<M, F, P> findAsgnService(String asgnName)
			throws ManagedException {

		if (logger.isDebugEnabled()) {
			logger.debug("Looking for assignment service `" + asgnName + "`");
		}

		IAsgnService<M, F, P> srv = (IAsgnService<M, F, P>) this.applicationContext
				.getBean(asgnName);
		if (srv == null && this.applicationContext.getParent() != null) {
			srv = (IAsgnService<M, F, P>) this.applicationContext.getParent()
					.getBean(asgnName);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Assignment service `" + asgnName + "` not found !");
		}
		return srv;
	}

	/**
	 * 
	 * @param asgnName
	 * @return
	 * @throws ManagedException
	 */
	public <E> IAsgnTxService<E> findAsgnTxService(String asgnName)
			throws ManagedException {

		return this.findAsgnTxService(asgnName, null);
	}

	/**
	 * Find an business assignment service given the service name and the list
	 * of factories.
	 * 
	 * @param <M>
	 * @param <P>
	 * @param asgnName
	 * @param factories
	 * @return
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	@Override
	public <E> IAsgnTxService<E> findAsgnTxService(String asgnName,
			String factoryName) throws ManagedException {

		if (logger.isDebugEnabled()) {
			logger.debug("Looking for business assignment service `" + asgnName
					+ "`");
		}

		IAsgnTxService<E> srv = (IAsgnTxService<E>) this.applicationContext
				.getBean(asgnName, IAsgnTxService.class);
		if (srv == null && this.applicationContext.getParent() != null) {
			srv = (IAsgnTxService<E>) this.applicationContext.getParent()
					.getBean(asgnName, IAsgnTxService.class);
		}

		if (srv == null) {
			throw new ManagedException(ErrorCodeCommons.SERVICE_NOT_FOUND,
					"Business assignment service `" + asgnName
							+ "` not found !");
		}
		return srv;
	}

	/**
	 * Getter for the spring application context.
	 * 
	 * @return
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * Setter for the spring application context.
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
