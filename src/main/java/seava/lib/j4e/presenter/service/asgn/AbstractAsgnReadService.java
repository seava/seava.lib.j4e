package seava.lib.j4e.presenter.service.asgn;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.query.AbstractQueryBuilder;
import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.lib.j4e.presenter.model.AbstractAsgnModel;

public class AbstractAsgnReadService<M extends AbstractAsgnModel<E>, F, P, E>
		extends AbstractAsgnBaseService<M, F, P, E> {

	private Class<? extends AbstractQueryBuilder<M, F, P>> leftQueryBuilderClass;

	private Class<? extends AbstractQueryBuilder<M, F, P>> rightQueryBuilderClass;

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public List<M> findLeft(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException {
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		bld.addFilterCondition(" e.clientId = :clientId and e."
				+ this.getCtx().getLeftPkField()
				+ " not in (select x.itemId from TempAsgnLine x where x.selectionId = :selectionId)");

		bld.setFilter(filter);
		bld.setParams(params);

		List<M> result = new ArrayList<M>();
		TypedQuery<E> q = bld.createQuery(this.getEntityClass());
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		List<E> list = q.setFirstResult(bld.getResultStart())
				.setMaxResults(bld.getResultSize()).getResultList();
		for (E e : list) {
			M m;
			try {
				m = this.getModelClass().newInstance();
			} catch (Exception e1) {
				e1.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e1.getLocalizedMessage());
			}
			entityToModel(e, m);
			result.add(m);
		}
		return result;
	}

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public List<M> findRight(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException {
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		bld.addFilterCondition(" e.clientId = :clientId and e."
				+ this.getCtx().getLeftPkField()
				+ " in (select x.itemId from TempAsgnLine x where x.selectionId = :selectionId)");
		bld.setFilter(filter);
		bld.setParams(params);

		List<M> result = new ArrayList<M>();

		TypedQuery<E> q = bld.createQuery(this.getEntityClass());
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		List<E> list = q.setFirstResult(bld.getResultStart())
				.setMaxResults(bld.getResultSize()).getResultList();
		for (E e : list) {
			M m;
			try {
				m = this.getModelClass().newInstance();
			} catch (Exception e1) {
				e1.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e1.getLocalizedMessage());
			}
			entityToModel(e, m);
			result.add(m);
		}
		return result;
	}

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Long countLeft(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException {
		return this.count_(selectionId, filter, params, builder);
	}

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Long countRight(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException {
		return this.count_(selectionId, filter, params, builder);
	}

	/**
	 * 
	 * @param selectionId
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	protected Long count_(String selectionId, F filter, P params,
			IQueryBuilder<M, F, P> builder) throws ManagedException {
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		Query q = bld.createQueryCount();
		q.setParameter("clientId", Session.user.get().getClient().getId());
		q.setParameter("selectionId", selectionId);
		Object count = q.getSingleResult();
		if (count instanceof Integer) {
			return ((Integer) count).longValue();
		} else {
			return (Long) count;
		}
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> createLeftQueryBuilder()
			throws ManagedException {
		IQueryBuilder<M, F, P> qb = null;
		if (this.getLeftQueryBuilderClass() == null) {
			if (this.getQueryBuilderClass() == null) {
				qb = new QueryBuilderWithJpql<M, F, P>();
			} else {
				try {
					qb = (IQueryBuilder<M, F, P>) this.getQueryBuilderClass()
							.newInstance();
				} catch (Exception e) {
					e.printStackTrace();
					throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
							e.getLocalizedMessage());
				}
			}
		} else {
			try {
				qb = (IQueryBuilder<M, F, P>) this.getLeftQueryBuilderClass()
						.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getLocalizedMessage());
			}
		}
		this.prepareQueryBuilder(qb);
		return qb;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> createRightQueryBuilder()
			throws ManagedException {
		IQueryBuilder<M, F, P> qb = null;
		if (this.getRightQueryBuilderClass() == null) {
			if (this.getQueryBuilderClass() == null) {
				qb = new QueryBuilderWithJpql<M, F, P>();
			} else {
				try {
					qb = (IQueryBuilder<M, F, P>) this.getQueryBuilderClass()
							.newInstance();
				} catch (Exception e) {
					e.printStackTrace();
					throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
							e.getLocalizedMessage());
				}
			}
		} else {
			try {
				qb = (IQueryBuilder<M, F, P>) this.getRightQueryBuilderClass()
						.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getLocalizedMessage());
			}
		}
		this.prepareQueryBuilder(qb);
		return qb;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	protected void prepareQueryBuilder(IQueryBuilder<M, F, P> qb)
			throws ManagedException {
		qb.setFilterClass(this.getFilterClass());
		qb.setParamClass(this.getParamClass());
		qb.setDescriptor(this.getDescriptor());
		qb.setSettings(this.getSettings());
		if (qb instanceof QueryBuilderWithJpql) {
			QueryBuilderWithJpql<M, F, P> jqb = (QueryBuilderWithJpql<M, F, P>) qb;
			jqb.setEntityManager(this.getTxService().getEntityManager());
			String entityName = this.getEntityClass().getSimpleName();
			try {
				entityName = (String) this.getEntityClass().getField("ALIAS")
						.get(null);
			} catch (Exception e) {
				// ignore it , proceed with entity simple class name
			}
			jqb.setBaseEql("select e from " + entityName + " e");
			jqb.setBaseEqlCount("select count(1) from " + entityName + " e");

			if (this.getDescriptor().isWorksWithJpql()) {
				jqb.setDefaultWhere(this.getDescriptor().getJpqlDefaultWhere());
				jqb.setDefaultSort(this.getDescriptor().getJpqlDefaultSort());
			}
		}

	}

	// ==================== getters- setters =====================

	public Class<? extends AbstractQueryBuilder<M, F, P>> getLeftQueryBuilderClass() {
		return leftQueryBuilderClass;
	}

	public void setLeftQueryBuilderClass(
			Class<? extends AbstractQueryBuilder<M, F, P>> leftQueryBuilderClass) {
		this.leftQueryBuilderClass = leftQueryBuilderClass;
	}

	public Class<? extends AbstractQueryBuilder<M, F, P>> getRightQueryBuilderClass() {
		return rightQueryBuilderClass;
	}

	public void setRightQueryBuilderClass(
			Class<? extends AbstractQueryBuilder<M, F, P>> rightQueryBuilderClass) {
		this.rightQueryBuilderClass = rightQueryBuilderClass;
	}
}
