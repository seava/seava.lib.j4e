/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service;

 
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.query.AbstractQueryBuilder;

/**
 * Abstract base class for query-enabled presenter-service hierarchy. It works
 * with a model-type(M) which represents the returned data, optionally using a
 * filter-type (F) and a parameter-type(P) input.
 * 
 * @author amathe
 * 
 * @param <M>
 *            returned data-type
 * @param <F>
 *            optional filter-type, can be the same as the model-type
 * @param <P>
 *            optional parameter-type
 */
public abstract class AbstractPresenterReadService<M, F, P> extends
		AbstractPresenterBaseService {

	/**
	 * Model data class
	 */
	private Class<M> modelClass;

	/**
	 * Filter data class
	 */
	private Class<F> filterClass;

	/**
	 * Parameters data class
	 */
	private Class<P> paramClass;

	/**
	 * 
	 */
	private Class<? extends AbstractQueryBuilder<M, F, P>> queryBuilderClass;

	/**
	 * 
	 * @return
	 */
	public Class<M> getModelClass() {
		return modelClass;
	}

	/**
	 * 
	 * @param modelClass
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void setModelClass(Class<M> modelClass) {
		this.modelClass = modelClass;
		if (this.filterClass == null) {
			this.filterClass = (Class<F>) modelClass;
		}
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public M newModelInstance() throws ManagedException {
		try {
			return this.getModelClass().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot instantiate class", e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Class<F> getFilterClass() {
		return filterClass;
	}

	/**
	 * 
	 * @param filterClass
	 */
	public void setFilterClass(Class<F> filterClass) {
		this.filterClass = filterClass;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public F newFilterInstance() throws ManagedException {
		try {
			return this.getFilterClass().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot instantiate class", e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Class<P> getParamClass() {
		return paramClass;
	}

	/**
	 * 
	 * @param paramClass
	 */
	public void setParamClass(Class<P> paramClass) {
		this.paramClass = paramClass;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public P newParamInstance() throws ManagedException {
		try {
			return this.getParamClass().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot instantiate class", e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Class<? extends AbstractQueryBuilder<M, F, P>> getQueryBuilderClass() {
		return queryBuilderClass;
	}

	/**
	 * 
	 * @param queryBuilderClass
	 */
	public void setQueryBuilderClass(
			Class<? extends AbstractQueryBuilder<M, F, P>> queryBuilderClass) {
		this.queryBuilderClass = queryBuilderClass;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> newQueryBuilderInstance()
			throws ManagedException {
		try {
			return this.getQueryBuilderClass().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot instantiate class", e);
		}
	}

}
