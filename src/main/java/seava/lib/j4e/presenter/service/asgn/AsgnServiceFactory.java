/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service.asgn;

import java.util.List;

import seava.lib.j4e.api.business.service.IAsgnTxServiceFactory;
import seava.lib.j4e.api.presenter.service.IAsgnService;
import seava.lib.j4e.api.presenter.service.IAsgnServiceFactory;
import seava.lib.j4e.base.AbstractBase;

/**
 * 
 * @author amathe
 * 
 */
public class AsgnServiceFactory extends AbstractBase implements
		IAsgnServiceFactory {

	private List<IAsgnTxServiceFactory> asgnTxServiceFactories;

	@SuppressWarnings("unchecked")
	@Override
	public <M, F, P> IAsgnService<M, F, P> create(String key) {
		IAsgnService<M, F, P> s = (IAsgnService<M, F, P>) this
				.getApplicationContext().getBean(key, IAsgnService.class);
		s.setAsgnTxServiceFactories(asgnTxServiceFactories);
		return s;
	}

	public List<IAsgnTxServiceFactory> getAsgnTxServiceFactories() {
		return asgnTxServiceFactories;
	}

	public void setAsgnTxServiceFactories(
			List<IAsgnTxServiceFactory> asgnTxServiceFactories) {
		this.asgnTxServiceFactories = asgnTxServiceFactories;
	}

}
