/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service.ds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.jpa.JpaQuery;
import org.eclipse.persistence.queries.Cursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsExport;
import seava.lib.j4e.api.presenter.descriptor.IQueryBuilder;
import seava.lib.j4e.api.presenter.service.IDsReadService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.lib.j4e.presenter.model.AbstractDsModel;

/**
 * Implements the read actions for an entity-ds. See the super-classes for more
 * details.
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsReadService<M extends AbstractDsModel<E>, F, P, E>
		extends AbstractEntityDsBaseService<M, F, P, E> implements
		IDsReadService<M, F, P> {

	private static final int DEFAULT_RESULT_START = 0;
	private static final int DEFAULT_RESULT_SIZE = 500;

	final static Logger logger = LoggerFactory
			.getLogger(AbstractEntityDsReadService.class);

	private Map<String, String> summaryRules;

	/* ========================== QUERY =========================== */

	/**
	 * Count results for the given query context.
	 * 
	 * @param filter
	 * @param params
	 * @param builder
	 * @return
	 * @throws ManagedException
	 */
	public Long count(IQueryBuilder<M, F, P> builder) throws ManagedException {
		if (builder == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot run a count query with null query builder.");
		}

		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		Object count = bld.createQueryCount().getSingleResult();
		if (count instanceof Integer) {
			return ((Integer) count).longValue();
		} else {
			return (Long) count;
		}
	}

	/**
	 * 
	 */
	public Map<String, Object> summaries(IQueryBuilder<M, F, P> builder)
			throws ManagedException {
		if (builder == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot run a summary query with null query builder.");
		}
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		Map<String, Object> sum = new HashMap<String, Object>();
		if (bld.getBaseEqlSummary() != null) {
			List<?> result = bld.createQuerySummary().getResultList();

			Object _r = result.get(0);
			Object[] o = {};
			if (_r instanceof Object[]) {
				o = (Object[]) _r;
			} else {
				o = new Object[] { _r };
			}
			// = (Object[]) result.get(0);
			int len = o.length;
			int i = 0;
			for (Map.Entry<String, String> entry : this.getSummaryRules()
					.entrySet()) {
				String key = entry.getKey();
				if (i < len) {
					sum.put(key, o[i]);
					i++;
				} else {
					break;
				}
			}
		}
		return sum;
	}

	public List<M> find(F filter) throws ManagedException {
		return this.find(filter, null, null, DEFAULT_RESULT_START,
				DEFAULT_RESULT_SIZE, null);
	}

	public List<M> find(F filter, int resultStart, int resultSize)
			throws ManagedException {
		return this.find(filter, null, null, resultStart, resultSize, null);
	}

	public List<M> find(F filter, P params) throws ManagedException {
		return this.find(filter, params, null, DEFAULT_RESULT_START,
				DEFAULT_RESULT_SIZE, null);
	}

	public List<M> find(F filter, P params, int resultStart, int resultSize)
			throws ManagedException {
		return this.find(filter, params, null, resultStart, resultSize, null);
	}

	public List<M> find(F filter, List<IFilterRule> filterRules)
			throws ManagedException {
		return this.find(filter, null, filterRules, DEFAULT_RESULT_START,
				DEFAULT_RESULT_SIZE, null);
	}

	public List<M> find(F filter, List<IFilterRule> filterRules,
			int resultStart, int resultSize) throws ManagedException {
		return this.find(filter, null, filterRules, resultStart, resultSize,
				null);
	}

	public List<M> find(F filter, P params, List<IFilterRule> filterRules)
			throws ManagedException {
		return this.find(filter, params, filterRules, DEFAULT_RESULT_START,
				DEFAULT_RESULT_SIZE, null);
	}

	public List<M> find(F filter, P params, List<IFilterRule> filterRules,
			int resultStart, int resultSize) throws ManagedException {
		return this.find(filter, params, filterRules, resultStart, resultSize,
				null);
	}

	public List<M> find(F filter, P params, List<IFilterRule> filterRules,
			List<ISortRule> sortRules) throws ManagedException {
		return this.find(filter, params, filterRules, DEFAULT_RESULT_START,
				DEFAULT_RESULT_SIZE, sortRules);
	}

	public List<M> find(F filter, P params, List<IFilterRule> filterRules,
			int resultStart, int resultSize, List<ISortRule> sortRules)
			throws ManagedException {
		QueryBuilderWithJpql<M, F, P> bld = null;
		bld = (QueryBuilderWithJpql<M, F, P>) this.createQueryBuilder();
		bld.setFilter(filter);
		bld.setParams(params);
		bld.setFilterRules(filterRules);
		bld.addFetchLimit(resultStart, resultSize);
		return this.find(bld);
	}

	public List<M> find(IQueryBuilder<M, F, P> builder) throws ManagedException {
		return this.find(builder, null);
	}

	public List<M> find(IQueryBuilder<M, F, P> builder, List<String> fieldNames)
			throws ManagedException {

		if (builder == null) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot run a query with null query builder.");
		}
		this.preFind(builder);
		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;

		List<E> entities;
		try {
			entities = bld.createQuery(this.getEntityClass())
					.setFirstResult(bld.getResultStart())
					.setMaxResults(bld.getResultSize()).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getLocalizedMessage());
		}

		if (logger.isDebugEnabled()) {
			logger.debug(
					"Found {} results. Applying entity-to-model conversion ( {} -> {} ) for the result.",
					new Object[] { entities.size() + "",
							this.getEntityClass().getSimpleName(),
							this.getModelClass().getSimpleName() });
		}

		List<M> result = this.getConverter().entitiesToModels(entities,
				bld.getEntityManager(), fieldNames);

		this.postFind(builder, result);
		return result;
	}

	/**
	 * Template method for pre-query.
	 * 
	 * @param filter
	 * @param params
	 * @param builder
	 * @throws ManagedException
	 */
	protected void preFind(IQueryBuilder<M, F, P> builder)
			throws ManagedException {
	}

	protected void postFind(IQueryBuilder<M, F, P> builder, List<M> result)
			throws ManagedException {
	}

	/**
	 * Find one result by ID
	 * 
	 * @param id
	 * @return
	 * @throws ManagedException
	 */
	public M findById(Object id) throws ManagedException {
		return this.findById(id, this.newParamInstance());
	}

	/**
	 * Find one result by ID
	 * 
	 * @param id
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public M findById(Object id, P params) throws ManagedException {

		F filter = this.newFilterInstance();

		if (IModelWithId.class.isAssignableFrom(filter.getClass())) {
			((IModelWithId) filter).setId(id);
		}

		List<M> result = this.find(filter, params, null);
		if (result.size() == 0) {
			return null;
		} else {
			return result.get(0);
		}
	}

	/**
	 * Find results by a list of IDs
	 * 
	 * @param ids
	 * @return
	 * @throws ManagedException
	 */
	public List<M> findByIds(List<Object> ids) throws ManagedException {
		// TODO Implement me !
		return null;
	}

	/* ========================== EXPORT =========================== */
	/**
	 * Export data
	 * 
	 * @param filter
	 * @param params
	 * @param builder
	 * @param writer
	 * @throws ManagedException
	 */
	@SuppressWarnings("unchecked")
	public void doExport(IQueryBuilder<M, F, P> builder, IDsExport<M> writer)
			throws ManagedException {

		QueryBuilderWithJpql<M, F, P> bld = (QueryBuilderWithJpql<M, F, P>) builder;
		bld.setForExport(true);

		EntityManager em = bld.getEntityManager().getEntityManagerFactory()
				.createEntityManager();
		bld.setEntityManager(em);

		try {

			Query q = bld.createQuery(this.getEntityClass())
					.setHint(QueryHints.CURSOR, true)
					.setHint(QueryHints.CURSOR_INITIAL_SIZE, 100)
					.setHint(QueryHints.CURSOR_PAGE_SIZE, 100)
					.setHint(QueryHints.READ_ONLY, HintValues.TRUE)
					.setFirstResult(bld.getResultStart())
					.setMaxResults(bld.getResultSize());

			Cursor c = q.unwrap(JpaQuery.class).getResultCursor();

			M ds;
			writer.begin();
			boolean isFirst = true;
			while (c.hasMoreElements()) {
				ds = newModelInstance();
				this.getConverter().entityToModel((E) c.nextElement(), ds,
						bld.getEntityManager(), null);
				writer.write(ds, isFirst);
				isFirst = false;
			}
			writer.end();
			c.close();
		} finally {
			em.close();
		}

	}

	/**
	 * Create a new query-builder instance.
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IQueryBuilder<M, F, P> createQueryBuilder() throws ManagedException {
		IQueryBuilder<M, F, P> qb = null;
		if (this.getQueryBuilderClass() == null) {
			qb = new QueryBuilderWithJpql<M, F, P>();
		} else {
			qb = newQueryBuilderInstance();
		}
		this._prepareQueryBuilder(qb);
		return qb;
	}

	/**
	 * Prepare the query builder injecting necessary dependencies.
	 * 
	 * @param qb
	 * @throws ManagedException
	 */
	private void _prepareQueryBuilder(IQueryBuilder<M, F, P> qb)
			throws ManagedException {
		qb.setModelClass(this.getModelClass());
		qb.setFilterClass(this.getFilterClass());
		qb.setParamClass(this.getParamClass());
		qb.setDescriptor(this.getDescriptor());
		qb.setSettings(this.getSettings());
		if (qb instanceof QueryBuilderWithJpql) {
			QueryBuilderWithJpql<M, F, P> jqb = (QueryBuilderWithJpql<M, F, P>) qb;
			jqb.setEntityManager(this.getEntityService().getEntityManager());
			String entityName = this.getEntityClass().getSimpleName();
			// Field x;
			try {
				entityName = (String) this.getEntityClass().getField("ALIAS")
						.get(null);
			} catch (Exception e) {
				// ignore it , proceed with entity simple class name
			}

			jqb.setBaseEql("select e from " + entityName + " e");
			jqb.setBaseEqlCount("select count(1) from " + entityName + " e");

			StringBuffer sumFields = new StringBuffer();
			Map<String, String> sRules = this.getSummaryRules();

			if (sRules != null) {
				for (Map.Entry<String, String> entry : sRules.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					if (sumFields.length() > 0) {
						sumFields.append(", ");
					}
					sumFields.append(value + " " + key);
				}
			}
			if (sumFields.length() > 0) {
				jqb.setBaseEqlSummary("select " + sumFields.toString()
						+ " from " + entityName + " e");
			} else {
				jqb.setBaseEqlSummary(null);
			}

			if (this.getDescriptor().isWorksWithJpql()) {
				jqb.setDefaultWhere(this.getDescriptor().getJpqlDefaultWhere());
				jqb.setDefaultSort(this.getDescriptor().getJpqlDefaultSort());
			}
		}
	}

	public IDsExport<M> createExporter(String dataFormat)
			throws ManagedException {
		return null;
	}

	public Map<String, String> getSummaryRules() {
		return summaryRules;
	}

	public void setSummaryRules(Map<String, String> summaryRules) {
		this.summaryRules = summaryRules;
	}
}
