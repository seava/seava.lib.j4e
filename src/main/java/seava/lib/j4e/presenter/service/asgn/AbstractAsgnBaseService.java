package seava.lib.j4e.presenter.service.asgn;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.descriptor.IAsgnContext;
import seava.lib.j4e.api.business.service.IAsgnTxService;
import seava.lib.j4e.api.business.service.IAsgnTxServiceFactory;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.presenter.descriptor.marshall.JsonMarshaller;
import seava.lib.j4e.presenter.descriptor.viewmodel.AsgnDescriptor;
import seava.lib.j4e.presenter.descriptor.viewmodel.ViewModelDescriptorManager;
import seava.lib.j4e.presenter.model.AbstractAsgnModel;
import seava.lib.j4e.presenter.service.AbstractPresenterReadService;

public class AbstractAsgnBaseService<M extends AbstractAsgnModel<E>, F, P, E>
		extends AbstractPresenterReadService<M, F, P> {

	/**
	 * Source entity type it works with.
	 */
	private Class<E> entityClass;

	private AsgnDescriptor<M> descriptor;

	private List<IAsgnTxServiceFactory> asgnTxServiceFactories;

	private IAsgnContext ctx;

	private String asgnTxFactoryName;

	private IAsgnTxService<E> txService;

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	private IAsgnTxService<E> findAsgnTxService() throws ManagedException {
		IAsgnTxService<E> s = this.getServiceLocator().findAsgnTxService(
				Constants.SPRING_DEFAULT_ASGN_TX_SERVICE,
				this.asgnTxFactoryName);
		s.setEntityClass(entityClass);
		return s;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IAsgnTxService<E> getTxService() throws ManagedException {
		if (this.txService == null) {
			this.txService = findAsgnTxService();
		}
		return this.txService;
	}

	/**
	 * 
	 * @param e
	 * @param m
	 * @throws ManagedException
	 */
	public void entityToModel(E e, M m) throws ManagedException {
		ExpressionParser parser = new SpelExpressionParser();
		StandardEvaluationContext context = new StandardEvaluationContext(e);
		Map<String, String> refpaths = this.getDescriptor().getE2mConv();
		Method[] methods = this.getModelClass().getMethods();
		for (Method method : methods) {
			if (!method.getName().equals("set__clientRecordId__")
					&& method.getName().startsWith("set")) {
				String fn = StringUtils.uncapitalize(method.getName()
						.substring(3));
				try {
					method.invoke(m, parser.parseExpression(refpaths.get(fn))
							.getValue(context));
				} catch (Exception exc) {

				}
			}
		}
	}

	/**
	 * 
	 * @param dataFormat
	 * @return
	 * @throws ManagedException
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat)
			throws ManagedException {
		IDsMarshaller<M, F, P> marshaller = null;
		if (dataFormat.equals(IDsMarshaller.JSON)) {
			marshaller = new JsonMarshaller<M, F, P>(this.getModelClass(),
					this.getFilterClass(), this.getParamClass());
		}
		return marshaller;
	}

	/**
	 * Initialize the component's temporary workspace.
	 * 
	 * @return the UUID of the selection
	 * @throws ManagedException
	 */
	public String setup(String asgnName, String objectId)
			throws ManagedException {
		return this.getTxService().doSetup(this.ctx, asgnName, objectId);
	}

	/**
	 * Clean-up the temporary selections.
	 * 
	 * @throws ManagedException
	 */
	public void cleanup(String selectionId) throws ManagedException {
		this.getTxService().doCleanup(this.ctx, selectionId);
	}

	/**
	 * Restores all the changes made by the user to the initial state.
	 * 
	 * @throws ManagedException
	 */
	public void reset(String selectionId, String objectId)
			throws ManagedException {
		this.getTxService().doReset(this.ctx, selectionId, objectId);
	}

	// ==================== getters- setters =====================

	public Class<E> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	public List<IAsgnTxServiceFactory> getAsgnTxServiceFactories() {
		return asgnTxServiceFactories;
	}

	public void setAsgnTxServiceFactories(
			List<IAsgnTxServiceFactory> asgnTxServiceFactories) {
		this.asgnTxServiceFactories = asgnTxServiceFactories;
	}

	public IAsgnContext getCtx() {
		return ctx;
	}

	public void setCtx(IAsgnContext ctx) {
		this.ctx = ctx;
	}

	public String getAsgnTxFactoryName() {
		return asgnTxFactoryName;
	}

	public void setAsgnTxFactoryName(String asgnTxFactoryName) {
		this.asgnTxFactoryName = asgnTxFactoryName;
	}

	public AsgnDescriptor<M> getDescriptor() throws ManagedException {
		if (this.descriptor == null) {
			boolean useCache = this.getSettings()
					.get(Constants.PROP_WORKING_MODE)
					.equals(Constants.PROP_WORKING_MODE_PROD);
			this.descriptor = ViewModelDescriptorManager.getAsgnDescriptor(
					this.getModelClass(), useCache);
		}
		return descriptor;
	}

	public void setDescriptor(AsgnDescriptor<M> descriptor) {
		this.descriptor = descriptor;
	}

}
