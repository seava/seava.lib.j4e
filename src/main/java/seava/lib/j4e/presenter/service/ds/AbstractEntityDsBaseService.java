/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service.ds;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.descriptor.IDsConverter;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.converter.AbstractDsConverter;
import seava.lib.j4e.presenter.converter.DefaultDsConverter;
import seava.lib.j4e.presenter.descriptor.marshall.JsonMarshaller;
import seava.lib.j4e.presenter.descriptor.marshall.XmlMarshaller;
import seava.lib.j4e.presenter.descriptor.viewmodel.DsDescriptor;
import seava.lib.j4e.presenter.descriptor.viewmodel.ViewModelDescriptorManager;
import seava.lib.j4e.presenter.model.AbstractDsModel;
import seava.lib.j4e.presenter.service.AbstractPresenterReadService;

/**
 * Base abstract class for entity based data-source service hierarchy. An
 * entity-data-source(referred to as entity-ds) is a specialized data-source
 * which provides view-model perspective(M) from a given persistence
 * perspective(E). <br>
 * Subclasses implement standard functionality for standard read actions (query,
 * export ), write actions (insert, update, delete, import) and remote procedure
 * call like method invocation (rpc).
 * 
 * Adds to its super-class an entity-type information.
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsBaseService<M extends AbstractDsModel<E>, F, P, E>
		extends AbstractPresenterReadService<M, F, P> {

	/**
	 * Source entity type it works with.
	 */
	private Class<E> entityClass;

	/**
	 * Converter class to be used for entity-to-ds and ds-to-entity conversions.
	 */
	private Class<? extends AbstractDsConverter<M, E>> converterClass;

	/**
	 * DS descriptor.
	 */
	private DsDescriptor<M> descriptor;

	/**
	 * DS <-> Entity converter
	 */
	private AbstractDsConverter<M, E> converter;

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public DsDescriptor<M> getDescriptor() throws ManagedException {
		try {
			if (this.descriptor == null) {
				boolean useCache = this.getSettings()
						.get(Constants.PROP_WORKING_MODE)
						.equals(Constants.PROP_WORKING_MODE_PROD);
				this.descriptor = ViewModelDescriptorManager.getDsDescriptor(
						this.getModelClass(), useCache);
			}
			return descriptor;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @param descriptor
	 */
	public void setDescriptor(DsDescriptor<M> descriptor) {
		this.descriptor = descriptor;
	}

	/**
	 * 
	 * @param dataFormat
	 * @return
	 * @throws ManagedException
	 */
	public IDsMarshaller<M, F, P> createMarshaller(String dataFormat)
			throws ManagedException {
		try {
			IDsMarshaller<M, F, P> marshaller = null;
			if (dataFormat.equals(IDsMarshaller.JSON)) {
				marshaller = new JsonMarshaller<M, F, P>(this.getModelClass(),
						this.getFilterClass(), this.getParamClass());
			} else if (dataFormat.equals(IDsMarshaller.XML)) {
				marshaller = new XmlMarshaller<M, F, P>(this.getModelClass(),
						this.getFilterClass(), this.getParamClass());
			}
			return marshaller;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public Class<E> getEntityClass() {
		return entityClass;
	}

	/**
	 * 
	 * @param entityClass
	 */
	public void setEntityClass(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * 
	 * @return
	 */
	public Class<? extends AbstractDsConverter<M, E>> getConverterClass() {
		return converterClass;
	}

	/**
	 * 
	 * @param converterClass
	 */
	public void setConverterClass(
			Class<? extends AbstractDsConverter<M, E>> converterClass) {
		this.converterClass = converterClass;
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	@SuppressWarnings("unchecked")
	protected IDsConverter<M, E> getConverter() throws ManagedException {

		try {
			if (this.converter != null) {
				return this.converter;
			}

			if (this.converterClass != null) {
				this.converter = this.converterClass.newInstance();
			} else {
				this.converter = DefaultDsConverter.class.newInstance();
			}

			this.converter.setApplicationContext(this.getApplicationContext());
			this.converter.setDescriptor(this.getDescriptor());
			this.converter.setEntityClass(this.getEntityClass());
			this.converter.setModelClass(this.getModelClass());
			this.converter.setServiceLocator(this.getServiceLocator());

			return this.converter;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @return
	 * @throws ManagedException
	 */
	public IEntityService<E> getEntityService() throws ManagedException {
		// if (this.entityService == null) {
		return this.findEntityService(this.getEntityClass());
		// }
		// return this.entityService;
	}

}
