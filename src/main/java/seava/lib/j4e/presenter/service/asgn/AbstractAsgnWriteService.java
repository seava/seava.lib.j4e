package seava.lib.j4e.presenter.service.asgn;

import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.presenter.model.AbstractAsgnModel;

/**
 * 
 * @author amathe
 *
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public class AbstractAsgnWriteService<M extends AbstractAsgnModel<E>, F, P, E>
		extends AbstractAsgnReadService<M, F, P, E> {

	/**
	 * Add the specified list of IDs to the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	public void moveRight(String selectionId, List<String> ids)
			throws ManagedException {
		this.getTxService().doMoveRight(this.getCtx(), selectionId, ids);
	}

	/**
	 * Add all the available values to the selected ones.
	 * 
	 * @throws ManagedException
	 */
	public void moveRightAll(String selectionId, F filter, P params)
			throws ManagedException {
		// TODO: send the filter also to move all according to filter
		this.getTxService().doMoveRightAll(this.getCtx(), selectionId);
	}

	/**
	 * Remove the specified list of IDs from the selected ones.
	 * 
	 * @param ids
	 * @throws ManagedException
	 */
	public void moveLeft(String selectionId, List<String> ids)
			throws ManagedException {
		this.getTxService().doMoveLeft(this.getCtx(), selectionId, ids);
	}

	/**
	 * Remove all the selected values.
	 * 
	 * @throws ManagedException
	 */
	public void moveLeftAll(String selectionId, F filter, P params)
			throws ManagedException {
		// TODO: send the filter also to move all according to filter
		this.getTxService().doMoveLeftAll(this.getCtx(), selectionId);
	}

	/**
	 * 
	 * @param selectionId
	 * @param objectId
	 * @throws ManagedException
	 */
	public void save(String selectionId, String objectId)
			throws ManagedException {
		this.getTxService().doSave(this.getCtx(), selectionId, objectId);
	}
}
