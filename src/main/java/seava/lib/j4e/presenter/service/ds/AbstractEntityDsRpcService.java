/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service.ds;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;

import org.springframework.transaction.TransactionSystemException;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.viewmodel.RpcDefinition;
import seava.lib.j4e.presenter.model.AbstractDsModel;
import seava.lib.j4e.presenter.service.AbstractPresenterBaseService;

/**
 * Implements the rpc(remote-procedure call) actions for an entity-ds. See the
 * super-classes for more details.
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 * @param <E>
 */
public abstract class AbstractEntityDsRpcService<M extends AbstractDsModel<E>, F, P, E>
		extends AbstractEntityDsWriteService<M, F, P, E> {

	private Map<String, RpcDefinition> rpcData = new HashMap<String, RpcDefinition>();
	private Map<String, RpcDefinition> rpcFilter = new HashMap<String, RpcDefinition>();

	// ======================== RPC ===========================

	/**
	 * Execute an arbitrary service method with the data object.
	 */
	public void rpcData(String procedureName, M ds, P params)
			throws ManagedException {

		if (!rpcData.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: `" + procedureName + "`");
		}

		RpcDefinition def = rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), getModelClass());
		try {
			if (rpcMethod.isWithParams()) {
				rpcMethod.getMethod().invoke(delegate, ds, params);
			} else {
				rpcMethod.getMethod().invoke(delegate, ds);
			}
		} catch (Exception e1) {
			Throwable t = e1;
			e1.printStackTrace();
			if (e1 instanceof InvocationTargetException) {
				t = ((InvocationTargetException) e1).getTargetException();
				if (t instanceof TransactionSystemException) {
					if (((TransactionSystemException) t)
							.getApplicationException() != null) {
						t = ((TransactionSystemException) t)
								.getApplicationException();
					} else if (t.getCause() != null) {
						t = t.getCause();
					}
				}
			}

			if (t instanceof RollbackException) {
				t = t.getCause();
			}

			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot call `" + procedureName + "`.", t);
		}

		if (def.getReloadFromEntity()) {
			if (ds instanceof IModelWithId) {
				EntityManager em = this.getEntityService().getEntityManager();
				if (((IModelWithId<?>) ds).getId() != null) {
					E e = (E) em.find(this.getEntityClass(),
							((IModelWithId<?>) ds).getId());
					this.getConverter().entityToModel(e, ds, em, null);
				}
			}
		}
		// delegate.execute(ds);
	}

	/**
	 * Execute an arbitrary service method with the data object returning a
	 * stream as result.
	 * 
	 * @param procedureName
	 * @param ds
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcDataStream(String procedureName, M ds, P params)
			throws ManagedException {

		if (!rpcData.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), getModelClass());

		InputStream result = null;
		try {
			if (rpcMethod.isWithParams()) {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						ds, params);
			} else {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						ds);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e);
		}
		return result;
	}

	/**
	 * Execute an arbitrary service method with the filter object.
	 * 
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcFilter(String procedureName, F filter, P params)
			throws ManagedException {

		if (!rpcFilter.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcFilter.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), getFilterClass());

		try {
			if (rpcMethod.isWithParams()) {
				rpcMethod.getMethod().invoke(delegate, filter, params);
			} else {
				rpcMethod.getMethod().invoke(delegate, filter);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e);
		}
	}

	/**
	 * Execute an arbitrary service method with the filter object returning a
	 * stream as result.
	 * 
	 * @param procedureName
	 * @param filter
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcFilterStream(String procedureName, F filter, P params)
			throws ManagedException {

		if (!rpcFilter.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcFilter.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), getFilterClass());

		InputStream result = null;
		try {
			if (rpcMethod.isWithParams()) {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						filter, params);
			} else {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						filter);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e);
		}

		return result;
	}

	/**
	 * Execute an arbitrary service method with a list of data objects.
	 * Contributed by Jan Fockaert
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcData(String procedureName, List<M> list, P params)
			throws ManagedException {

		if (!rpcData.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), List.class);
		try {
			if (rpcMethod.isWithParams()) {
				rpcMethod.getMethod().invoke(delegate, list, params);
			} else {
				rpcMethod.getMethod().invoke(delegate, list);
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e1);
		}

		if (def.getReloadFromEntity()) {
			for (M ds : list) {
				if (ds instanceof IModelWithId) {
					Object _id = ((IModelWithId<?>) ds).getId();
					if (_id != null && !"".equals(_id)) {
						EntityManager em = this.getEntityService()
								.getEntityManager();
						E e = (E) em.find(this.getEntityClass(), _id);
						this.getConverter().entityToModel(e, ds, em, null);
					}
				}
			}
		}
	}

	/**
	 * Execute an arbitrary service method with a list of data objects returning
	 * a stream as result.
	 * 
	 * Contributed by Jan Fockaert
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcDataStream(String procedureName, List<M> list,
			P params) throws ManagedException {

		if (!rpcData.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), List.class);

		InputStream result = null;
		try {
			if (rpcMethod.isWithParams()) {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						list, params);
			} else {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						list);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e);
		}

		return result;
	}

	/**
	 * Execute an arbitrary service method with a list of data IDs.
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @throws ManagedException
	 */
	public void rpcIds(String procedureName, List<Object> list, P params)
			throws ManagedException {

		if (!rpcData.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), List.class);
		try {
			if (rpcMethod.isWithParams()) {
				rpcMethod.getMethod().invoke(delegate, list, params);
			} else {
				rpcMethod.getMethod().invoke(delegate, list);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e);
		}
	}

	/**
	 * Execute an arbitrary service method with a list of data IDs returning a
	 * stream as result.
	 * 
	 * 
	 * @param procedureName
	 * @param list
	 * @param params
	 * @return
	 * @throws ManagedException
	 */
	public InputStream rpcIdsStream(String procedureName, List<Object> list,
			P params) throws ManagedException {

		if (!rpcData.containsKey(procedureName)) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName);
		}

		RpcDefinition def = rpcData.get(procedureName);
		AbstractPresenterBaseService delegate = this.getDelegate(def
				.getDelegateClass());
		RpcMethod rpcMethod = this.getRpcMethod(def.getDelegateClass(),
				def.getMethodName(), List.class);

		InputStream result = null;
		try {
			if (rpcMethod.isWithParams()) {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						list, params);
			} else {
				result = (InputStream) rpcMethod.getMethod().invoke(delegate,
						list);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No such procedure defined: " + procedureName, e);
		}

		return result;
	}

	/**
	 * Helper method to find the rpc method to be invoked.
	 * 
	 * @param claz
	 * @param methodName
	 * @param dataClass
	 * @return
	 * @throws ManagedException
	 */
	protected RpcMethod getRpcMethod(
			Class<? extends AbstractPresenterBaseService> claz,
			String methodName, Class<?> dataClass) throws ManagedException {
		Method m = null;
		boolean withParams = false;
		try {
			m = claz.getMethod(methodName, dataClass, getParamClass());
			withParams = true;
		} catch (NoSuchMethodException e) {
			try {
				m = claz.getMethod(methodName, dataClass);
			} catch (NoSuchMethodException ex) {
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						"Delegate class " + claz.getCanonicalName()
								+ " does not implement a `" + methodName + "("
								+ dataClass.getSimpleName() + ")` method.");
			}
		}

		return new RpcMethod(m, withParams);
	}

	public Map<String, RpcDefinition> getRpcData() {
		return rpcData;
	}

	public void setRpcData(Map<String, RpcDefinition> rpcData) {
		this.rpcData = rpcData;
	}

	public Map<String, RpcDefinition> getRpcFilter() {
		return rpcFilter;
	}

	public void setRpcFilter(Map<String, RpcDefinition> rpcFilter) {
		this.rpcFilter = rpcFilter;
	}

	private class RpcMethod {
		private Method method;
		private boolean withParams;

		public RpcMethod(Method method, boolean withParams) {
			super();
			this.method = method;
			this.withParams = withParams;
		}

		public Method getMethod() {
			return method;
		}

		public boolean isWithParams() {
			return withParams;
		}

	}
}
