/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.service;

import java.util.ArrayList;
import java.util.List;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.AbstractPresenterBase;

/**
 * Root abstract class for presenter service hierarchy. It provides support for
 * the sub-classes with the generally needed elements like an
 * applicationContext, system configuration parameters, workflow services etc.
 * 
 * Usually an application developer shouldn't sub-class directly this class but
 * choose one of the more specialized sub-class based on the particular need.
 * 
 * @author amathe
 * 
 */
public abstract class AbstractPresenterBaseService extends
		AbstractPresenterBase {

	// private ProcessEngine workflowEngine;

	/**
	 * Lookup a data-source service.
	 * 
	 * @param dsName
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IDsService<M, F, P> findDsService(String dsName)
			throws ManagedException {
		return this.getServiceLocator().findDsService(dsName);
	}

	/**
	 * Lookup a data-source service based on the model class
	 * 
	 * @param dsModelClass
	 * @return
	 * @throws ManagedException
	 */
	public <M, F, P> IDsService<M, F, P> findDsService(Class<M> dsModelClass)
			throws ManagedException {
		try {
			return this.findDsService((String) dsModelClass.getField("ALIAS")
					.get(null));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * Lookup an entity service.
	 * 
	 * @param <E>
	 * @param entityClass
	 * @return
	 * @throws ManagedException
	 */
	public <E> IEntityService<E> findEntityService(Class<E> entityClass)
			throws ManagedException {
		return this.getServiceLocator().findEntityService(entityClass);
	}

	/**
	 * Prepare a data-source delegate. Inject all required dependencies marked
	 * with <code>@Autowired</code> for which there is no attempt to auto-load
	 * on-demand from the spring-context.
	 */
	protected <M, F, P> void prepareDelegate(
			AbstractPresenterBaseService delegate) {
		delegate.setApplicationContext(this.getApplicationContext());
	}

	/**
	 * Get a presenter delegate
	 * 
	 * @param claz
	 * @return
	 * @throws ManagedException
	 */
	protected <T extends AbstractPresenterBaseService> T getDelegate(
			Class<T> claz) throws ManagedException {
		T delegate;
		try {
			delegate = claz.newInstance();
		} catch (Exception e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot instantiate class" + claz.getCanonicalName(), e);
		}
		this.prepareDelegate(delegate);
		return delegate;
	}

	/**
	 * 
	 * @param list
	 * @return
	 */
	protected List<Object> collectIds(List<?> list) {
		List<Object> result = new ArrayList<Object>();
		for (Object e : list) {
			try {
				result.add(((IModelWithId<?>) e).getId());
			} catch (ClassCastException exc) {
				// ignore it , go to next in list
			}
		}
		return result;
	}

	// /**
	// * Get the workflow engine
	// *
	// * @return
	// * @throws ManagedException
	// */
	// public ProcessEngine getWorkflowEngine() throws ManagedException {
	// if (this.workflowEngine == null) {
	// this.workflowEngine = (ProcessEngine) this.getApplicationContext()
	// .getBean(IActivitiProcessEngineHolder.class)
	// .getProcessEngine();
	// }
	// return this.workflowEngine;
	// }
	//
	// public RuntimeService getWorkflowRuntimeService() throws ManagedException
	// {
	// return this.getWorkflowEngine().getRuntimeService();
	// }
	//
	// public TaskService getWorkflowTaskService() throws ManagedException {
	// return this.getWorkflowEngine().getTaskService();
	// }
	//
	// public RepositoryService getWorkflowRepositoryService() throws
	// ManagedException
	// {
	// return this.getWorkflowEngine().getRepositoryService();
	// }
	//
	// public HistoryService getWorkflowHistoryService() throws ManagedException
	// {
	// return this.getWorkflowEngine().getHistoryService();
	// }
	//
	// public FormService getWorkflowFormService() throws ManagedException {
	// return this.getWorkflowEngine().getFormService();
	// }

}
