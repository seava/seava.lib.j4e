/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.IServiceLocatorPresenter;
import seava.lib.j4e.base.AbstractBase;

/**
 * 
 * @author amathe
 * 
 */
public abstract class AbstractPresenterBase extends AbstractBase {

	/**
	 * Presenter level service locator
	 */
	private IServiceLocatorPresenter serviceLocator;

	/**
	 * Get presenter service locator. If it is null attempts to retrieve it from
	 * Spring context.
	 * 
	 * @return
	 */
	public IServiceLocatorPresenter getServiceLocator() {
		if (this.serviceLocator == null) {
			this.serviceLocator = this.getApplicationContext().getBean(
					IServiceLocatorPresenter.class);
		}
		return serviceLocator;
	}

	/**
	 * Set presenter service locator.
	 * 
	 * @param serviceLocator
	 */
	public void setServiceLocator(IServiceLocatorPresenter serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

	/**
	 * Lookup an entity service.
	 * 
	 * @param <E>
	 * @param entityClass
	 * @return
	 * @throws Exception
	 */
	public <T> IEntityService<T> findEntityService(Class<T> entityClass)
			throws ManagedException {
		return this.getServiceLocator().findEntityService(entityClass);
	}
}
