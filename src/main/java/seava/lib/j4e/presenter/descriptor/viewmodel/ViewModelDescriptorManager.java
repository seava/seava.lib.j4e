/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.viewmodel;

import java.util.HashMap;
import java.util.Map;

import seava.lib.j4e.api.base.exceptions.ManagedException;

/**
 * 
 * @author amathe
 * 
 */
public final class ViewModelDescriptorManager {

	private static Map<String, AbstractViewModelDescriptor<?>> store = new HashMap<String, AbstractViewModelDescriptor<?>>();

	@SuppressWarnings("unchecked")
	public static <M> DsDescriptor<M> getDsDescriptor(Class<M> modelClass,
			boolean useCache) throws ManagedException {

		if (useCache) {
			String key = modelClass.getCanonicalName();
			if (!store.containsKey(key)) {
				store.put(key, new DsDescriptor<M>(modelClass));
			}
			return (DsDescriptor<M>) store.get(key);
		} else {
			return new DsDescriptor<M>(modelClass);
		}

	}

	@SuppressWarnings("unchecked")
	public static <M> AsgnDescriptor<M> getAsgnDescriptor(Class<M> modelClass,
			boolean useCache) throws ManagedException {
		if (useCache) {
			String key = modelClass.getCanonicalName();
			if (!store.containsKey(key)) {
				store.put(key, new AsgnDescriptor<M>(modelClass));
			}
			return (AsgnDescriptor<M>) store.get(key);
		} else {
			return new AsgnDescriptor<M>(modelClass);
		}

	}

	public static void remove(AbstractViewModelDescriptor<?> descriptor) {
		store.remove(descriptor.getModelClass().getCanonicalName());
	}

}
