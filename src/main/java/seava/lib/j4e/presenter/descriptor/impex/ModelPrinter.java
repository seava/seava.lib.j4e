/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.impex;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import seava.lib.j4e.api.base.session.Session;

/**
 * 
 * @author amathe
 *
 */
public class ModelPrinter {

	private Map<String, SimpleDateFormat> dateFormats;

	/**
	 * 
	 */
	public ModelPrinter() {
		this.dateFormats = new HashMap<String, SimpleDateFormat>();
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(String v) {
		return v;
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(Boolean v) {
		return (v) ? "yes" : "no";
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(Integer v) {
		return v.toString();
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(Long v) {
		return v.toString();
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(Float v) {
		return v.toString();
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(BigDecimal v) {
		return v.toString();
	}

	/**
	 * 
	 * @param v
	 * @return
	 */
	public String print(Date v) {
		return this.getDateFormat("DATE_FORMAT").format(v);
	}

	/**
	 * 
	 * @param v
	 * @param mask
	 * @return
	 */
	public String print(Date v, String mask) {
		return this.getDateFormat(mask).format(v);
	}

	/**
	 * 
	 * @param mask
	 * @return
	 */
	private SimpleDateFormat getDateFormat(String mask) {
		if (!this.dateFormats.containsKey(mask)) {
			SimpleDateFormat fmt = Session.user.get().getSettings()
					.getDateFormat("JAVA_" + mask);
			this.dateFormats.put(mask, fmt);
		}
		return this.dateFormats.get(mask);
	}
}
