/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.impex;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsExport;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 */
public class DsXmlExport<M> extends AbstractDsExport<M> implements IDsExport<M> {

	private String rootTag = "records";

	public DsXmlExport() {
		super();
		this.outFileExtension = "xml";
	}

	@Override
	public void write(M data, boolean isFirst) throws ManagedException {

		List<ExportField> columns = ((ExportInfo) this.getExportInfo())
				.getColumns();
		Iterator<ExportField> it = columns.iterator();

		StringBuffer sb = new StringBuffer();
		sb.append("<record>");
		while (it.hasNext()) {
			ExportField k = it.next();

			Object x;
			try {
				x = k._getFieldGetter().invoke(data);
			} catch (Exception e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getMessage(), e);
			}
			String tag = k.getName();

			if (x != null) {
				if (x instanceof Date) {
					sb.append("<" + tag + ">"
							+ this.getServerDateFormat().format(x) + "</" + tag
							+ ">");
				} else {
					sb.append("<" + tag + ">" + x.toString() + "</" + tag + ">");
				}
			} else {
				sb.append("<" + tag + "></" + tag + ">");
			}

		}
		sb.append("</record>");
		try {
			this.writer.write(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}

	}

	/**
	 * 
	 */
	@Override
	protected void beginContent() throws ManagedException {

		try {
			this.writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			this.writer.write("<" + rootTag + ">");
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	@Override
	protected void endContent() throws ManagedException {
		try {
			this.writer.write("</" + rootTag + ">");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getRootTag() {
		return rootTag;
	}

	/**
	 * 
	 * @param rootTag
	 */
	public void setRootTag(String rootTag) {
		this.rootTag = rootTag;
	}

}
