/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.viewmodel;

public class DsDescriptor<M> extends AbstractViewModelDescriptor<M> {

	public DsDescriptor(Class<M> modelClass) {
		super(modelClass);
	}
}
