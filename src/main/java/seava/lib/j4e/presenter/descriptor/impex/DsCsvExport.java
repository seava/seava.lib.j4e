/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.impex;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsExport;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

public class DsCsvExport<M> extends AbstractDsExport<M> implements IDsExport<M> {

	private char csvSeparator = ',';

	public DsCsvExport() {
		super();
		this.outFileExtension = "csv";
	}

	@Override
	public void write(M data, boolean isFirst) throws ManagedException {
		List<ExportField> columns = ((ExportInfo) this.getExportInfo())
				.getColumns();
		Iterator<ExportField> it = columns.iterator();

		StringBuffer sb = new StringBuffer();
		sb.append("\n");
		while (it.hasNext()) {
			ExportField k = it.next();

			Object x;
			try {
				x = k._getFieldGetter().invoke(data);
			} catch (Exception e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getMessage(), e);
			}
			if (x != null) {
				String v = null;
				if (x instanceof Date) {
					v = this.getServerDateFormat().format(x);
				} else {
					v = x.toString();
				}
				sb.append(StringEscapeUtils.escapeCsv(v));
			}
			if (it.hasNext()) {
				sb.append(this.csvSeparator);
			}

		}
		try {
			this.writer.write(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}

	}

	@Override
	protected void beginContent() throws ManagedException {
		boolean isFirst = true;
		List<ExportField> columns = ((ExportInfo) this.getExportInfo())
				.getColumns();
		for (ExportField column : columns) {
			if (!isFirst) {
				try {
					this.writer.write(this.csvSeparator);
				} catch (IOException e) {
					e.printStackTrace();
					throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
							e.getMessage(), e);
				}
			}
			try {
				this.writer
						.write(StringEscapeUtils.escapeCsv(column.getName()));
			} catch (IOException e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getMessage(), e);
			}
			isFirst = false;
		}
	}

	@Override
	protected void endContent() {
		// TODO Auto-generated method stub

	}

	public char getCsvSeparator() {
		return csvSeparator;
	}

	public void setCsvSeparator(char csvSeparator) {
		this.csvSeparator = csvSeparator;
	}

}
