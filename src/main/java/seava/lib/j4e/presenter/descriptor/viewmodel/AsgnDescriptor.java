/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.viewmodel;

public class AsgnDescriptor<M> extends AbstractViewModelDescriptor<M> {

	public AsgnDescriptor(Class<M> modelClass) {
		super(modelClass);
	}
}
