/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.impex;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsExport;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 */
public class DsJsonExport<M> extends AbstractDsExport<M> implements
		IDsExport<M> {

	public DsJsonExport() {
		super();
		this.outFileExtension = "json";
	}

	/**
	 * 
	 */
	@Override
	public void write(M data, boolean isFirst) throws ManagedException {
		List<ExportField> columns = ((ExportInfo) this.getExportInfo())
				.getColumns();
		Iterator<ExportField> it = columns.iterator();

		StringBuffer sb = new StringBuffer();
		if (!isFirst) {
			sb.append(',');
		}
		sb.append("{");
		while (it.hasNext()) {
			ExportField k = it.next();

			Object x;
			try {
				x = k._getFieldGetter().invoke(data);
			} catch (Exception e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getMessage(), e);
			}
			if (x != null) {
				if (x instanceof Date) {
					sb.append('"' + k.getName() + '"' + ':' + '"'
							+ this.getServerDateFormat().format(x) + '"');
				} else {
					sb.append('"' + k.getName() + '"' + ':' + '"'
							+ x.toString() + '"');
				}
			} else {
				sb.append('"' + k.getName() + '"' + ":null");
			}

			if (it.hasNext()) {
				sb.append(',');
			}
		}
		sb.append("}");
		try {
			this.writer.write(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}

	}

	/**
	 * 
	 */
	@Override
	protected void beginContent() throws ManagedException {
		try {
			this.writer.write("[");
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	@Override
	protected void endContent() throws ManagedException {
		try {
			this.writer.write("]");
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		}
	}

}
