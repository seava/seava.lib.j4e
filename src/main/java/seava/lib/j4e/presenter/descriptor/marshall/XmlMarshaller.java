/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.marshall;

import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IExportInfo;
import seava.lib.j4e.api.presenter.result.IActionResultDelete;
import seava.lib.j4e.api.presenter.result.IActionResultFind;
import seava.lib.j4e.api.presenter.result.IActionResultRpc;
import seava.lib.j4e.api.presenter.result.IActionResultSave;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * 
 * @author amathe
 * 
 * @param <M>
 * @param <F>
 * @param <P>
 */
public class XmlMarshaller<M, F, P> extends AbstractMarshaller<M, F, P>
		implements IDsMarshaller<M, F, P> {

	public XmlMarshaller(Class<M> modelClass, Class<F> filterClass,
			Class<P> paramClass) throws ManagedException {

		this.modelClass = modelClass;
		this.filterClass = filterClass;
		this.paramClass = paramClass;

	}

	/**
	 * 
	 */
	public Object getDelegate() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public IExportInfo readExportInfo(String source) throws ManagedException {
		return null;
	}

	/**
	 * 
	 */
	public List<ISortRule> readSortRules(String source) throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public List<IFilterRule> readFilterRules(String source)
			throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public <T> T readDataFromString(String source, Class<T> type)
			throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public M readDataFromString(String source) throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public List<M> readListFromString(String source) throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public <T> List<T> readListFromString(String source, Class<T> type)
			throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public F readFilterFromString(String source) throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public P readParamsFromString(String source) throws ManagedException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public String writeDataToString(M m) throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(), m, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeListToString(List<M> list) throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(), list, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeFilterToString(F f) throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(), f, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeParamsToString(P p) throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(), p, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultFind result)
			throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultSave result)
			throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultDelete result)
			throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultRpc result)
			throws ManagedException {
		StringWriter writer = new StringWriter();
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, writer);
		return writer.toString();
	}

	/**
	 * 
	 */
	public void writeDataToStream(M m, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(), m, out);
	}

	/**
	 * 
	 */
	public void writeListToStream(List<M> list, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(), list, out);
	}

	/**
	 * 
	 */
	public void writeFilterToStream(F f, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(), f, out);
	}

	/**
	 * 
	 */
	public void writeParamsToStream(P p, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(), p, out);
	}

	/**
	 * 
	 */
	public void writeResultToStream(IActionResultFind result, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, out);
	}

	/**
	 * 
	 */
	public void writeResultToStream(IActionResultSave result, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, out);
	}

	/**
	 * 
	 */
	public void writeResultToStream(IActionResultRpc result, OutputStream out)
			throws ManagedException {
		this._marshall(createMarshaller(new Class[] { result.getClass() }),
				result, out);
	}

	/**
	 * 
	 * @param m
	 * @param result
	 * @param out
	 * @throws ManagedException
	 */
	private void _marshall(Marshaller m, Object result, OutputStream out)
			throws ManagedException {
		try {
			m.marshal(result, out);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot marshall xml", e);
		}
	}

	/**
	 * 
	 * @param m
	 * @param result
	 * @param out
	 * @throws ManagedException
	 */
	private void _marshall(Marshaller m, Object result, Writer out)
			throws ManagedException {
		try {
			m.marshal(result, out);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot marshall xml", e);
		}
	}

	/**
	 * 
	 * @param classesToBeBound
	 * @return
	 * @throws ManagedException
	 */
	public Marshaller createMarshaller(Class<?>... classesToBeBound)
			throws ManagedException {

		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (classesToBeBound != null) {
			for (int i = 0; i < classesToBeBound.length; i++) {
				classes.add(classesToBeBound[i]);
			}
		}

		classes.add(this.filterClass);
		classes.add(this.modelClass);
		if (this.paramClass != null) {
			classes.add(this.paramClass);
		}

		Class<?>[] x = classes.toArray(new Class<?>[] {});

		try {
			JAXBContext context = JAXBContext.newInstance(x);
			Marshaller m = context.createMarshaller();
			m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "");
			return m;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot create xml parser", e);
		}
	}

}
