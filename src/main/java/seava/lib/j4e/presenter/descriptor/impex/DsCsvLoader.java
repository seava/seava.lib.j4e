/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.impex;

import java.beans.PropertyEditorManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.base.propertyeditors.BigDecimalEditor;
import seava.lib.j4e.base.propertyeditors.BooleanEditor;
import seava.lib.j4e.base.propertyeditors.DateEditor;
import seava.lib.j4e.base.propertyeditors.FloatEditor;
import seava.lib.j4e.base.propertyeditors.IntegerEditor;
import seava.lib.j4e.base.propertyeditors.LongEditor;
import seava.lib.j4e.presenter.libextensions.HeaderColumnNameMappingStrategy_Dnet;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class DsCsvLoader {

	private ConfigCsvImport config;

	public DsCsvLoader() {

		config = new ConfigCsvImport();
		// TODO: move this from here

		PropertyEditorManager.registerEditor(java.lang.Boolean.class,
				BooleanEditor.class);
		PropertyEditorManager.registerEditor(java.lang.Long.class,
				LongEditor.class);
		PropertyEditorManager.registerEditor(java.lang.Integer.class,
				IntegerEditor.class);
		PropertyEditorManager.registerEditor(java.lang.Float.class,
				FloatEditor.class);
		PropertyEditorManager.registerEditor(java.util.Date.class,
				DateEditor.class);
		PropertyEditorManager.registerEditor(java.math.BigDecimal.class,
				BigDecimalEditor.class);
	}

	/**
	 * 
	 * @param file
	 * @param dsClass
	 * @param columns
	 * @return
	 * @throws ManagedException
	 */
	public <M> List<M> run(File file, Class<M> dsClass, String[] columns)
			throws ManagedException {
		return this.run_(file, dsClass, columns).getResult();
	}

	/**
	 * 
	 * @param file
	 * @param dsClass
	 * @param columns
	 * @return
	 * @throws ManagedException
	 */
	public <M> DsCsvLoaderResult<M> run2(File file, Class<M> dsClass,
			String[] columns) throws ManagedException {
		return this.run_(file, dsClass, columns);
	}

	/**
	 * 
	 * @param inputStream
	 * @param dsClass
	 * @param columns
	 * @param sourceName
	 * @return
	 * @throws ManagedException
	 */
	public <M> List<M> run(InputStream inputStream, Class<M> dsClass,
			String[] columns, String sourceName) throws ManagedException {
		return this.run_(inputStream, dsClass, columns, sourceName).getResult();
	}

	/**
	 * 
	 * @param inputStream
	 * @param dsClass
	 * @param columns
	 * @param sourceName
	 * @return
	 * @throws ManagedException
	 */
	public <M> DsCsvLoaderResult<M> run2(InputStream inputStream,
			Class<M> dsClass, String[] columns, String sourceName)
			throws ManagedException {
		return this.run_(inputStream, dsClass, columns, sourceName);
	}

	/**
	 * 
	 * @param file
	 * @param dsClass
	 * @param columns
	 * @return
	 * @throws ManagedException
	 */
	protected <M> DsCsvLoaderResult<M> run_(File file, Class<M> dsClass,
			String[] columns) throws ManagedException {
		try {
			InputStream inputStream = new FileInputStream(file);
			return this.run_(inputStream, dsClass, columns,
					file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.FILE_NOT_FOUND,
					"File not found ", e);
		}
	}

	/**
	 * 
	 * @param inputStream
	 * @param dsClass
	 * @param columns
	 * @param sourceName
	 * @return
	 * @throws ManagedException
	 */
	protected <M> DsCsvLoaderResult<M> run_(InputStream inputStream,
			Class<M> dsClass, String[] columns, String sourceName)
			throws ManagedException {

		InputStreamReader inputStreamReader = null;
		CSVReader csvReader = null;
		try {
			List<M> list = null;
			DsCsvLoaderResult<M> result = new DsCsvLoaderResult<M>();
			// file = new File(this.path + "/"+ this.fileName);
			inputStreamReader = new InputStreamReader(inputStream,
					Charset.forName(this.config.getEncoding()));
			if (columns != null) {
				csvReader = new CSVReader(inputStreamReader,
						this.config.getSeparator(), this.config.getQuoteChar(),
						1);
				CsvToBean<M> csv = new CsvToBean<M>();
				ColumnPositionMappingStrategy<M> strategy = new ColumnPositionMappingStrategy<M>();
				strategy.setType(dsClass);
				strategy.setColumnMapping(columns);
				list = csv.parse(strategy, csvReader);
				result.setHeader(columns);
				result.setResult(list);
			} else {
				csvReader = new CSVReader(inputStreamReader,
						this.config.getSeparator(), this.config.getQuoteChar());
				CsvToBean<M> csv = new CsvToBean<M>();
				HeaderColumnNameMappingStrategy_Dnet<M> strategy = new HeaderColumnNameMappingStrategy_Dnet<M>();
				strategy.setType(dsClass);
				list = csv.parse(strategy, csvReader);
				result.setHeader(strategy.getHeader());
				result.setResult(list);
			}
			return result;
		} catch (Exception e) {
			String msg = "Error loading data from source: " + sourceName
					+ ". \n Reason is: ";
			String details = null;
			if (e.getCause() != null) {
				details = e.getCause().getLocalizedMessage();
			} else {
				details = e.getLocalizedMessage();
			}
			if (details != null && !"".equals(details)) {
				msg = msg + details;
			} else {
				msg = msg + e.getStackTrace()[0];
			}
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR, msg, e);
		} finally {
			try {
				if (csvReader != null) {
					csvReader.close();
				}
				if (inputStreamReader != null) {
					inputStreamReader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						e.getMessage(), e);
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public ConfigCsvImport getConfig() {
		return config;
	}

	/**
	 * 
	 * @param config
	 */
	public void setConfig(ConfigCsvImport config) {
		this.config = config;
	}

}
