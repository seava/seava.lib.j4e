package seava.lib.j4e.presenter.descriptor.marshall;

import java.text.SimpleDateFormat;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import seava.lib.j4e.api.Constants;

public class JsonMarshallerFactory {

	public static ObjectMapper getInstance() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS,
				false);
		mapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(
				DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(
				DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
				false);

		SimpleDateFormat sdf = new SimpleDateFormat(
				Constants.get_server_datetime_format());

		mapper.setDateFormat(sdf);
		return mapper;
	}
}
