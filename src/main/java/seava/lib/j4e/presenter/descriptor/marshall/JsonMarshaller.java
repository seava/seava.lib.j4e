/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.presenter.descriptor.marshall;

import java.io.OutputStream;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;

import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.descriptor.ISortRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.descriptor.IDsMarshaller;
import seava.lib.j4e.api.presenter.descriptor.IExportInfo;
import seava.lib.j4e.api.presenter.result.IActionResultDelete;
import seava.lib.j4e.api.presenter.result.IActionResultFind;
import seava.lib.j4e.api.presenter.result.IActionResultRpc;
import seava.lib.j4e.api.presenter.result.IActionResultSave;
import seava.lib.j4e.base.descriptor.FilterRule;
import seava.lib.j4e.base.descriptor.SortRule;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.presenter.descriptor.impex.ExportInfo;

public class JsonMarshaller<M, F, P> extends AbstractMarshaller<M, F, P>
		implements IDsMarshaller<M, F, P> {

	private ObjectMapper mapper;

	public JsonMarshaller(Class<M> modelClass, Class<F> filterClass,
			Class<P> paramClass) {
		this.modelClass = modelClass;
		this.filterClass = filterClass;
		this.paramClass = paramClass;
		this.mapper = JsonMarshallerFactory.getInstance();
	}

	/**
	 * 
	 */
	public IExportInfo readExportInfo(String source) throws ManagedException {
		try {
			return this.mapper.readValue(source, ExportInfo.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public List<ISortRule> readSortRules(String source) throws ManagedException {
		try {
			return this.mapper.readValue(source,
					new TypeReference<List<SortRule>>() {
					});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public List<IFilterRule> readFilterRules(String source)
			throws ManagedException {
		try {
			return this.mapper.readValue(source,
					new TypeReference<List<FilterRule>>() {
					});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public <T> T readDataFromString(String source, Class<T> type)
			throws ManagedException {
		try {
			return this.mapper.readValue(source, type);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public M readDataFromString(String source) throws ManagedException {
		try {
			return this.mapper.readValue(source, getModelClass());
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public List<M> readListFromString(String source) throws ManagedException {
		JavaType type = mapper.getTypeFactory().constructCollectionType(
				List.class, this.getModelClass());
		try {
			return this.mapper.readValue(source, type);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public F readFilterFromString(String source) throws ManagedException {
		try {
			return this.mapper.readValue(source, getFilterClass());
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public P readParamsFromString(String source) throws ManagedException {
		if (getParamClass() == null) {
			return null;
		} else {
			try {
				return this.mapper.readValue(source, getParamClass());
			} catch (Exception e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						"Cannot parse json ", e);
			}
		}
	}

	/**
	 * 
	 */
	public <T> List<T> readListFromString(String source, Class<T> type)
			throws ManagedException {
		try {
			return this.mapper.readValue(source, new TypeReference<List<T>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot parse json ", e);
		}
	}

	/**
	 * 
	 */
	public String writeDataToString(M m) throws ManagedException {
		return this._writeValueAsString(m);
	}

	/**
	 * 
	 */
	public String writeListToString(List<M> list) throws ManagedException {
		return this._writeValueAsString(list);
	}

	/**
	 * 
	 */
	public String writeFilterToString(F f) throws ManagedException {
		return this._writeValueAsString(f);
	}

	/**
	 * 
	 */
	public String writeParamsToString(P p) throws ManagedException {
		return this._writeValueAsString(p);
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultFind result)
			throws ManagedException {
		return this._writeValueAsString(result);
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultSave result)
			throws ManagedException {
		return this._writeValueAsString(result);
	}

	/**
 * 
 */
	public String writeResultToString(IActionResultDelete result)
			throws ManagedException {
		return this._writeValueAsString(result);
	}

	/**
	 * 
	 */
	public String writeResultToString(IActionResultRpc result)
			throws ManagedException {
		return this._writeValueAsString(result);
	}

	/**
	 * 
	 */
	public void writeDataToStream(M m, OutputStream out)
			throws ManagedException {
		this._writeValue(out, m);
	}

	/**
	 * 
	 */
	public void writeListToStream(List<M> list, OutputStream out)
			throws ManagedException {
		this._writeValue(out, list);
	}

	/**
	 * 
	 */
	public void writeFilterToStream(F f, OutputStream out)
			throws ManagedException {
		this._writeValue(out, f);
	}

	/**
	 * 
	 */
	public void writeParamsToStream(P p, OutputStream out)
			throws ManagedException {
		this._writeValue(out, p);
	}

	/**
	 * 
	 */
	public void writeResultToStream(IActionResultFind result, OutputStream out)
			throws ManagedException {
		this._writeValue(out, result);
	}

	/**
	 * 
	 */
	public void writeResultToStream(IActionResultSave result, OutputStream out)
			throws ManagedException {
		this._writeValue(out, result);
	}

	/**
	 * 
	 */
	public void writeResultToStream(IActionResultRpc result, OutputStream out)
			throws ManagedException {
		this._writeValue(out, result);
	}

	/**
	 * 
	 * @param out
	 * @param result
	 * @throws ManagedException
	 */
	private void _writeValue(OutputStream out, Object result)
			throws ManagedException {
		try {
			this.mapper.writeValue(out, result);
		} catch (Exception e) {

			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot json-encode data ", e);
		}
	}

	/**
	 * 
	 * @param result
	 * @return
	 * @throws ManagedException
	 */
	private String _writeValueAsString(Object result) throws ManagedException {
		try {
			return this.mapper.writeValueAsString(result);
		} catch (Exception e) {

			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Cannot json-encode data ", e);
		}
	}

	/**
	 * 
	 */
	public ObjectMapper getDelegate() {
		return this.mapper;
	}

}
