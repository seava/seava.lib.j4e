/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import seava.lib.j4e.api.base.descriptor.IImportDataFile;

/**
 * 
 * @author amathe
 * 
 */
public class DataFile implements IImportDataFile {

	private String ds;
	private String file;

	private String ukFieldName;

	public String getDs() {
		return ds;
	}

	public void setDs(String ds) {
		this.ds = ds;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getUkFieldName() {
		return ukFieldName;
	}

	public void setUkFieldName(String ukFieldName) {
		this.ukFieldName = ukFieldName;
	}

}
