/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import seava.lib.j4e.api.base.descriptor.IImportDataFile;
import seava.lib.j4e.api.base.descriptor.IImportDataSet;

/**
 * 
 * @author amathe
 * 
 */
@XmlRootElement(name = "initData")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataSet implements IImportDataSet {

	@XmlElementWrapper(name = "files")
	@XmlElement(name = "file", type = DataFile.class)
	private List<IImportDataFile> dataFiles;

	public List<IImportDataFile> getDataFiles() {
		return dataFiles;
	}

	public void setDataFiles(List<IImportDataFile> dataFiles) {
		this.dataFiles = dataFiles;
	}

	public void addToList(IImportDataFile dataFile) {
		if (this.dataFiles == null) {
			this.dataFiles = new ArrayList<IImportDataFile>();
		}
		this.dataFiles.add(dataFile);
	}

}
