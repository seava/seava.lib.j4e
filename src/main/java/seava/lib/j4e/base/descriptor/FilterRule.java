/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import seava.lib.j4e.api.base.descriptor.IFilterRule;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

/**
 * Describes one filter rule.
 * 
 * @author amathe
 * 
 */
public class FilterRule implements IFilterRule {

	String fieldName;
	String operation;
	String value1;
	String value2;

	Class<?> dataType;
	String dataTypeFQN;

	public FilterRule() {
		super();
	}

	public FilterRule(String fieldName, String operation, String value1,
			String value2) {
		super();
		this.fieldName = fieldName;
		this.operation = operation;
		this.value1 = value1;
		this.value2 = value2;

	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getDataTypeFQN() {
		return dataTypeFQN;
	}

	public void setDataTypeFQN(String dataTypeFQN)
			throws ClassNotFoundException {
		this.dataTypeFQN = dataTypeFQN;
		if (dataType == null) {
			dataType = Class.forName(dataTypeFQN);
		}
	}

	public Object getConvertedValue1() throws ManagedException {
		return this._convertValue(this.value1);
	}

	public Object getConvertedValue2() throws ManagedException {
		return this._convertValue(this.value2);
	}

	private Object _convertValue(String v) throws ManagedException {
		if (v == null) {
			return null;
		}
		if (dataType == Integer.class) {
			return Integer.parseInt(v);
		}
		if (dataType == Float.class) {
			return Float.parseFloat(v);
		}
		if (dataType == Long.class) {
			return Long.parseLong(v);
		}
		if (dataType == Boolean.class) {
			return Boolean.parseBoolean(v);
		}
		if (dataType == Date.class) {
			String mask = "yyyy-MM-dd";
			SimpleDateFormat df = new SimpleDateFormat(mask);
			try {
				return df.parse(v);
			} catch (ParseException e) {
				e.printStackTrace();
				throw new ManagedException(ErrorCodeCommons.INVALID_DATE,
						e.getMessage(), new String[] { v, mask });
			}
		}

		return v;
	}
}
