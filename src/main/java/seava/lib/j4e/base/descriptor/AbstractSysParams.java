/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import seava.lib.j4e.api.base.descriptor.ISysParamDefinition;
import seava.lib.j4e.api.base.descriptor.ISysParamDefinitions;

/**
 * 
 * @author amathe
 * 
 */
public abstract class AbstractSysParams implements ISysParamDefinitions {

	private Collection<ISysParamDefinition> params;
	private Map<String, String> defaultValues;

	/**
	 * 
	 * @param params
	 */
	abstract protected void initParams(Collection<ISysParamDefinition> params);

	/**
	 * 
	 */
	public Collection<ISysParamDefinition> getSysParamDefinitions() {
		if (this.params == null) {
			synchronized (this) {
				if (this.params == null) {
					this.params = new ArrayList<ISysParamDefinition>();
					this.initParams(this.params);
				}
			}
		}
		return params;
	}

	public String getDefaultValue(String name) {
		return this.defaultValues.get(name);
	}

	public Map<String, String> getDefaultValues() {
		return defaultValues;
	}

	public void setDefaultValues(Map<String, String> defaultValues) {
		this.defaultValues = defaultValues;
	}

}
