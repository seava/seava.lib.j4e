/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import seava.lib.j4e.api.base.descriptor.IFieldDefinition;

/**
 * 
 * @author amathe
 *
 */
public class FieldDefinition implements IFieldDefinition {

	private String name;
	private String className;

	public FieldDefinition() {
		super();
	}

	public FieldDefinition(String name, String className) {
		super();
		this.name = name;
		this.className = className;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}
