/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import seava.lib.j4e.api.base.descriptor.IImportDataPackage;
import seava.lib.j4e.api.base.descriptor.IImportDataSet;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

 

@XmlRootElement(name = "package")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataPackage implements IImportDataPackage {

	@XmlElementWrapper(name = "sets")
	@XmlElement(name = "set", type = DataSet.class)
	private List<IImportDataSet> dataSets;

	private String location;

	/**
	 * 
	 * @param location
	 * @return
	 * @throws ManagedException
	 */
	public static IImportDataPackage forIndexFile(String location)
			throws ManagedException {
		File file = new File(location);
		IImportDataPackage dp;
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(DataPackage.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			dp = (IImportDataPackage) jaxbUnmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getLocalizedMessage(), e);
		}
		String path;
		try {
			path = file.getParentFile().getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getLocalizedMessage(), e);
		}
		dp.setLocation(path);
		return dp;
	}

	/**
	 * 
	 * @param dataFile
	 * @return
	 * @throws ManagedException
	 */
	public static IImportDataPackage forDataFile(DataFile dataFile)
			throws ManagedException {
		IImportDataPackage dp = new DataPackage();
		DataSet set = new DataSet();
		set.addToList(dataFile);
		dp.addToList(set);
		return dp;
	}

	/* =============== GETTERS-SETTERS =============== */

	public void addToList(IImportDataSet dataSet) {
		if (this.dataSets == null) {
			this.dataSets = new ArrayList<IImportDataSet>();
		}
		this.dataSets.add(dataSet);
	}

	public List<IImportDataSet> getDataSets() {
		return dataSets;
	}

	public void setDataSets(List<IImportDataSet> dataSets) {
		this.dataSets = dataSets;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
