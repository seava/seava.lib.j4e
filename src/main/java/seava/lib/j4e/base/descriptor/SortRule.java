/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.descriptor;

import seava.lib.j4e.api.base.descriptor.ISortRule;

/**
 * 
 * @author amathe
 * 
 */
public class SortRule implements ISortRule {

	private String property;
	private String direction;

	public SortRule() {
	}

	public SortRule(String property) {
		this.property = property;
	}

	public SortRule(String property, String direction) {
		this.property = property;
		this.direction = direction;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
