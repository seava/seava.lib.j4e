/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.descriptor.ISysParamDefinition;
import seava.lib.j4e.api.base.descriptor.ISysParamDefinitions;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.presenter.service.ISysParamValueProvider;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

public class Settings implements ISettings, ApplicationContextAware {

	private ApplicationContext applicationContext;

	/**
	 * System properties defined in the application properties file.
	 */
	private final Map<String, String> properties;

	/**
	 * List of system parameter definitions. Each business module can declare
	 * and export system-parameter definitions. All of them are collected in
	 * this list.
	 * 
	 */
	private List<ISysParamDefinitions> paramDefinitions;

	/**
	 * System parameters default values. Provided at startup
	 */
	private Map<String, String> paramDefaultValues;

	/**
	 * Client level values for system parameters.
	 */
	private Map<String, Map<String, String>> paramValues;

	/**
	 * Product-name.
	 */
	private String productName;

	/**
	 * Product-description.
	 */
	private String productDescription;

	/**
	 * Product-vendor
	 */
	private String productVendor;

	/**
	 * Product-url
	 */
	private String productUrl;

	/**
	 * Product-version
	 */
	private String productVersion;

	public Settings(Map<String, String> properties) {

		this.properties = properties;

		Constants.set_server_date_format(this.properties
				.get(Constants.PROP_SERVER_DATE_FORMAT));
		Constants.set_server_time_format(this.properties
				.get(Constants.PROP_SERVER_TIME_FORMAT));
		Constants.set_server_datetime_format(this.properties
				.get(Constants.PROP_SERVER_DATETIME_FORMAT));
		Constants.set_server_alt_formats(this.properties
				.get(Constants.PROP_SERVER_ALT_FORMATS));
	}

	public String get(String key) {
		return this.properties.get(key);
	}

	public boolean getAsBoolean(String key) {
		return Boolean.valueOf(this.get(key));
	}

	public String getParam(String paramName) throws ManagedException {
		checkParam(paramName);
		checkParamValue(paramName);
		IUser usr = Session.user.get();
		if (usr != null) {
			if (!usr.isSystemUser()) {
				String clientId = usr.getClient().getId();
				if (this.paramValues.get(clientId).containsKey(paramName)) {
					return this.paramValues.get(clientId).get(paramName);
				}
			}
		}
		return this.paramDefaultValues.get(paramName);
	}

	public boolean getParamAsBoolean(String paramName) throws ManagedException {
		return Boolean.valueOf(getParam(paramName));
	}

	public void reloadParams() {
		this.populateParams();
	}

	public void reloadParamValues() throws ManagedException {
		this.populateParamValues();
	}

	private void checkParam(String paramName) throws ManagedException {
		if (this.paramDefaultValues == null) {
			this.populateParams();
		}
		if (!this.paramDefaultValues.containsKey(paramName)) {
			// maybe it's a bundle plugged in after first populate ...
			// populate it once again
			this.populateParams();
			if (!this.paramDefaultValues.containsKey(paramName)) {
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						"Parameter `" + paramName + "` has no value defined");
			}
		}
	}

	private void checkParamValue(String paramName) throws ManagedException {
		IUser usr = Session.user.get();
		if (usr != null) {

			if (!usr.isSystemUser()) {
				String clientId = Session.user.get().getClient().getId();
				if (this.paramValues == null
						|| (clientId != null && !"".equals(clientId) && this.paramValues
								.get(clientId) == null)) {
					this.populateParamValues();
				}
			}

		}
	}

	private void populateParams() {
		this.paramDefaultValues = new HashMap<String, String>();
		for (ISysParamDefinitions pd : this.getParamDefinitions()) {
			for (ISysParamDefinition p : pd.getSysParamDefinitions()) {
				String name = p.getName();
				this.paramDefaultValues.put(name, p.getDefaultValue());
			}
		}
	}

	private void populateParamValues() throws ManagedException {
		if (this.paramValues == null) {
			this.paramValues = new HashMap<String, Map<String, String>>();
		}

		IUser usr = Session.user.get();
		if (usr != null) {
			if (!usr.isSystemUser()) {
				this.paramValues.put(Session.user.get().getClient().getId(),
						new HashMap<String, String>());

				try {
					Map<String, String> values = this.applicationContext
							.getBean(ISysParamValueProvider.class)
							.getParamValues(new Date());

					this.paramValues.put(
							Session.user.get().getClient().getId(), values);
				} catch (Exception e) {
					throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
							e.getMessage(), e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<ISysParamDefinitions> getParamDefinitions() {
		if (this.paramDefinitions == null) {
			this.paramDefinitions = (List<ISysParamDefinitions>) this.applicationContext
					.getBean(Constants.SPRING_OSGI_SYSPARAM_DEFINITIONS);
			if (this.get(Constants.PROP_DEPLOYMENT).equals(
					Constants.PROP_DEPLOYMENT_JEE)) {
				// TODO: use a flag to check if already populated
				Map<String, ISysParamDefinitions> beans = this.applicationContext
						.getBeansOfType(ISysParamDefinitions.class);
				for (ISysParamDefinitions b : beans.values()) {
					this.paramDefinitions.add(b);
				}
			}
		}
		return this.paramDefinitions;
	}

	public void setParamDefinitions(List<ISysParamDefinitions> paramDefinitions) {
		this.paramDefinitions = paramDefinitions;
	}

	public Map<String, String> getParams() {
		return paramDefaultValues;
	}

	public void setParams(Map<String, String> params) {
		this.paramDefaultValues = params;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public String getProductUrl() {
		return productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
