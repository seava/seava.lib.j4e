/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base;

import java.util.Collection;

import seava.lib.j4e.api.base.descriptor.ISysParamDefinition;
import seava.lib.j4e.api.base.enums.SysParam;
import seava.lib.j4e.base.descriptor.AbstractSysParams;
import seava.lib.j4e.base.descriptor.SysParamDefinition;

/**
 * 
 * @author amathe
 * 
 */
public class SysParams_Core extends AbstractSysParams {

	protected void initParams(Collection<ISysParamDefinition> params) {
		SysParam[] list = SysParam.values();
		int l = list.length;
		for (int i = 0; i < l; i++) {
			SysParam p = list[i];
			params.add(new SysParamDefinition(p.name(), p.getTitle(), p
					.getDescription(), p.getDataType(), this.getDefaultValue(p
					.name()), p.getListOfValues()));
		}
	}
}
