/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.exceptions;

import seava.lib.j4e.api.base.exceptions.IErrorCode;

public enum ErrorCodeCommons implements IErrorCode {
	RUNTIME_ERROR(1),

	SERVICE_NOT_FOUND(2), // ACTION_NOT_ALLOWED(3),
	CLIENT_MISMATCH(3),
	// ----------------
	INVALID_DATE_FORMAT_MASK(10), INVALID_NUMBER_FORMAT_MASK(11), INVALID_DATE(
			12), INVALID_NUMBER(13),

	// ----------------
	OPERATION_NOT_ALLOWED(109), INSERT_NOT_ALLOWED(110), UPDATE_NOT_ALLOWED(111), DELETE_NOT_ALLOWED(
			112), IMPORT_NOT_ALLOWED(112), EXPORT_NOT_ALLOWED(112),

	// ----------------
	INVALID_FILE_LOCATION(210), FILE_NOT_FOUND(213), FILE_NOT_CREATABLE(214), FILE_NOT_READABLE(
			215), FILE_NOT_WRITABLE(216);

	;

	private final int errNo;

	private ErrorCodeCommons(int errNo) {
		this.errNo = errNo;
	}

	public int getErrNo() {
		return errNo;
	}

	public String getErrGroup() {
		return "J4E-WEB";
	}

	public String getErrMsg() {
		return this.name();
	}

}