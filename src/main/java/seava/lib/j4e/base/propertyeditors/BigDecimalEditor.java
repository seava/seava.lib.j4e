/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.propertyeditors;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;

/**
 * Property editor support for big decimal types
 * 
 * @author amathe
 * 
 */
public class BigDecimalEditor extends PropertyEditorSupport {

	public BigDecimalEditor() {
		super();
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		String input = (text != null ? text.trim() : null);
		if (input == null || input.equals("")) {
			setValue(null);
		} else {
			try {
				setValue(new BigDecimal(text));
			} catch (Exception e) {
				throw new IllegalArgumentException(
						"Invalid big-decimal value [" + text + "]");
			}
		}
	}
}
