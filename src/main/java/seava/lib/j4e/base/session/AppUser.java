/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.session;

import java.io.Serializable;

import seava.lib.j4e.api.base.session.IClient;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.IUserProfile;
import seava.lib.j4e.api.base.session.IUserSettings;
import seava.lib.j4e.api.base.session.IWorkspace;

/**
 * 
 * @author amathe
 * 
 */
public class AppUser implements IUser, Serializable {

	private static final long serialVersionUID = -9131543374115237340L;
	private final String code;
	private final String name;

	private final String loginName;
	private char[] password;

	private final IClient client;
	private final IUserSettings settings;
	private final IUserProfile profile;
	private final IWorkspace workspace;

	private final boolean systemUser;

	public AppUser(String code, String name, String loginName, String password,
			IClient client, IUserSettings settings, IUserProfile profile,
			IWorkspace workspace, boolean systemUser) {

		super();

		this.code = code;
		this.name = name;
		this.loginName = loginName;
		if (password != null) {
			this.password = password.toCharArray();
		}
		this.client = client;
		this.settings = settings;
		this.profile = profile;
		this.workspace = workspace;
		this.systemUser = systemUser;
	}

	public String getClientId() {
		if (this.client != null) {
			return this.client.getId();
		} else {
			return null;
		}
	}

	public String getClientCode() {
		if (this.client != null) {
			return this.client.getCode();
		} else {
			return null;
		}
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getLoginName() {
		return loginName;
	}

	public IClient getClient() {
		return client;
	}

	public IUserSettings getSettings() {
		return settings;
	}

	public IUserProfile getProfile() {
		return profile;
	}

	public IWorkspace getWorkspace() {
		return workspace;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public boolean isSystemUser() {
		return systemUser;
	}

}
