/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.session;

import seava.lib.j4e.api.web.security.ILoginParams;

/**
 * Login parameters holders used at authentication
 * 
 * @author amathe
 * 
 */
public class LoginParams implements ILoginParams {

	/**
	 * Client to connect to.
	 */
	private String clientCode;

	/**
	 * Language.
	 */
	private String language;

	/**
	 * Client agent
	 */
	private String userAgent;

	/**
	 * Client host
	 */
	private String remoteHost;

	/**
	 * Client IP address
	 */
	private String remoteIp;

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public String getRemoteIp() {
		return remoteIp;
	}

	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

}
