/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import seava.lib.j4e.api.base.session.ISessionUser;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.IUserProfile;

public class SessionUser implements ISessionUser, Serializable {

	private static final long serialVersionUID = 690259452784508198L;

	/**
	 * Authenticated user
	 */
	private final IUser user;

	/**
	 * Remote user-agent
	 */
	private final String userAgent;

	/**
	 * Log-in date
	 */
	private final Date loginDate;

	/**
	 * Host name of the remote client machine at login time
	 */
	private final String remoteHost;

	/**
	 * IP of the remote client machine at login time
	 */
	private final String remoteIp;

	/**
	 * Flag to lock the current session
	 */
	private boolean sessionLocked;

	/**
	 * List of roles granted to the current user
	 */
	private List<GrantedAuthority> authorities;

	/**
	 * 
	 * @param user
	 * @param userAgent
	 * @param loginDate
	 * @param remoteHost
	 * @param remoteIp
	 */
	public SessionUser(IUser user, String userAgent, Date loginDate,
			String remoteHost, String remoteIp) {
		super();
		this.user = user;
		this.userAgent = userAgent;
		this.loginDate = loginDate;
		this.remoteHost = remoteHost;
		this.remoteIp = remoteIp;
	}

	/**
	 * Get the user roles
	 */
	public Collection<? extends GrantedAuthority> getAuthorities() {
		if (this.authorities == null) {
			this.authorities = new ArrayList<GrantedAuthority>();
			for (String role : this.user.getProfile().getRoles()) {
				this.authorities.add(new SimpleGrantedAuthority(role));
			}
		}
		return this.authorities;
	}

	/**
	 * 
	 */
	public String getPassword() {
		return new String(user.getPassword());
	}

	/**
	 * 
	 */
	public String getUsername() {
		return user.getLoginName();
	}

	/**
	 * 
	 */
	public boolean isAccountNonExpired() {
		return !this.user.getProfile().isAccountExpired();
	}

	/**
	 * 
	 */
	public boolean isAccountNonLocked() {
		return !this.user.getProfile().isAccountLocked();
	}

	/**
	 * 
	 */
	public boolean isCredentialsNonExpired() {
		return !this.user.getProfile().isCredentialsExpired();
	}

	/**
	 * 
	 */
	public boolean isEnabled() {
		IUserProfile p = this.user.getProfile();
		return !p.isAccountExpired() && !p.isAccountLocked()
				&& !p.isCredentialsExpired();
	}

	/**
	 * 
	 */
	public IUser getUser() {
		return this.user;
	}

	/**
	 * 
	 */
	public String getUserAgent() {
		return this.userAgent;
	}

	/**
	 * 
	 */
	public Date getLoginDate() {
		return this.loginDate;
	}

	/**
	 * 
	 */
	public String getRemoteHost() {
		return this.remoteHost;
	}

	/**
	 * 
	 */
	public String getRemoteIp() {
		return this.remoteIp;
	}

	/**
	 * 
	 */
	public boolean isSessionLocked() {
		return sessionLocked;
	}

	/**
	 * 
	 * @param sessionLocked
	 */
	public void setSessionLocked(boolean sessionLocked) {
		this.sessionLocked = sessionLocked;
	}

	public void lockSession() {
		this.setSessionLocked(true);
	}

}
