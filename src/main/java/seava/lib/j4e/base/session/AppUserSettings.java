/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.session;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.ISettings;
import seava.lib.j4e.api.base.enums.DateFormatAttribute;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.IUserSettings;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;

public class AppUserSettings implements IUserSettings, Serializable {

	private static final long serialVersionUID = -9131543374115237340L;

	/**
	 * 
	 */
	private String language = Constants.DEFAULT_LANGUAGE;

	/**
	 * Date format masks to be used in java/extjs. The keys are the names in
	 * {@link DateFormatAttribute}.
	 */
	private Map<String, String> dateFormatMasks = new HashMap<String, String>();

	/**
	 * Date formats to be used in java. The keys are the names in
	 * {@link DateFormatAttribute}.
	 */
	private Map<String, SimpleDateFormat> dateFormats = new HashMap<String, SimpleDateFormat>();

	/**
	 * Pattern to highlight the separators: 0.000,00 or 0,000.00 etc
	 */
	private String numberFormat = Constants.DEFAULT_NUMBER_FORMAT;

	/**
	 * Derived from numberFormatMask
	 */
	private String decimalSeparator;

	/**
	 * Derived from numberFormatMask
	 */
	private String thousandSeparator;

	/**
	 * 
	 */
	private AppUserSettings() {

	}

	/**
	 * Create a new instance using values from the provided application settings
	 * or defaults.
	 * 
	 * @param settings
	 * @return
	 * @throws ManagedException
	 */
	public static AppUserSettings newInstance(ISettings settings)
			throws ManagedException {

		AppUserSettings usr = new AppUserSettings();

		for (DateFormatAttribute a : DateFormatAttribute.values()) {
			if (settings != null) {
				usr.setDateFormatMask(a.name(),
						settings.get(a.getPropertyFileKey()));
			} else {
				usr.setDateFormatMask(a.name(), a.getDefaultValue());
			}
		}

		if (settings != null) {
			usr.setLanguage(settings.get(Constants.PROP_LANGUAGE));
			usr.setNumberFormat(settings.get(Constants.PROP_NUMBER_FORMAT));
		}

		return usr;
	}

	/**
	 * 
	 * @param key
	 * @throws ManagedException
	 */
	private void validateDateFormatKey(String key) throws ManagedException {
		if (DateFormatAttribute.valueOf(key) == null) {
			throw new ManagedException(
					ErrorCodeCommons.INVALID_DATE_FORMAT_MASK, key
							+ " is not an accepted date-format-mask.",
					new String[] { key });
		}
	}

	/**
	 * 
	 * @param key
	 * @throws ManagedException
	 */
	private void validateNumberFormat(String key) throws ManagedException {

		if (key != null && key.length() == 8) {
			String k = key.replaceAll("0", "");
			if (k.length() == 2) {
				return;
			}
		}

		throw new ManagedException(
				ErrorCodeCommons.INVALID_NUMBER_FORMAT_MASK,
				key
						+ " is not an accepted number-format-mask. It should be a pattern similar to `0,000.00`  ",
				new String[] { key });
	}

	/* ================= GETTERS - SETTERS ================== */

	/**
	 * Get user language
	 * 
	 * @return
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Set user language
	 * 
	 * @param language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * 
	 * @return
	 */
	public String getNumberFormat() {
		return numberFormat;
	}

	/**
	 * 
	 * @param numberFormat
	 * @throws ManagedException
	 */
	public void setNumberFormat(String numberFormat) throws ManagedException {
		this.validateNumberFormat(numberFormat);
		this.numberFormat = numberFormat;
	}

	/**
	 * 
	 * @return
	 */
	public String getDecimalSeparator() {
		if (this.decimalSeparator == null) {
			this.decimalSeparator = this.numberFormat.replace("0", "")
					.substring(1, 2);
		}
		return decimalSeparator;
	}

	/**
	 * 
	 * @return
	 */
	public String getThousandSeparator() {
		if (this.thousandSeparator == null) {
			this.thousandSeparator = this.numberFormat.replace("0", "")
					.substring(0, 1);
		}
		return thousandSeparator;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	public String getDateFormatMask(String key) {
		return this.dateFormatMasks.get(key);
	}

	/**
	 * 
	 * @param key
	 * @param value
	 * @throws ManagedException
	 */
	public void setDateFormatMask(String key, String value)
			throws ManagedException {
		if (this.dateFormatMasks == null) {
			this.dateFormatMasks = new HashMap<String, String>();
		}
		this.validateDateFormatKey(key);
		this.dateFormatMasks.put(key, value);
	}

	/**
	 * Return a SimpleDateFormat according to the required mask
	 * 
	 * @param dfa
	 * @return
	 */
	public SimpleDateFormat getDateFormat(DateFormatAttribute dfa) {
		return this.getDateFormat(dfa.name());
	}

	/**
	 * Return a SimpleDateFormat according to the required mask
	 * 
	 * @param key
	 * @return
	 */
	public SimpleDateFormat getDateFormat(String key) {
		if (!this.dateFormats.containsKey(key)) {
			this.dateFormats.put(key,
					new SimpleDateFormat(this.dateFormatMasks.get(key)));
		}
		return this.dateFormats.get(key);
	}

}
