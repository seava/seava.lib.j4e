/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base.session;

import java.io.Serializable;
import java.util.List;

import seava.lib.j4e.api.base.session.IUserProfile;

/**
 * 
 * @author amathe
 * 
 */
public class AppUserProfile implements IUserProfile, Serializable {

	private static final long serialVersionUID = -9131543374115237340L;

	/**
	 * Flag to indicate and administrator user
	 */
	private final boolean administrator;

	/**
	 * List of roles granted to this user
	 */
	private final List<String> roles;

	/**
	 * Flag to indicate if the credentials are expired.
	 */
	private final boolean credentialsExpired;

	/**
	 * Flag to indicate if the user account is expired.
	 */
	private final boolean accountExpired;

	/**
	 * Flag to indicate if the user account is locked.
	 */
	private final boolean accountLocked;

	public AppUserProfile(boolean administrator, List<String> roles,
			boolean credentialsExpired, boolean accountExpired,
			boolean accountLocked) {
		super();
		this.administrator = administrator;
		this.roles = roles;
		this.credentialsExpired = credentialsExpired;
		this.accountExpired = accountExpired;
		this.accountLocked = accountLocked;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public List<String> getRoles() {
		return roles;
	}

	public boolean isCredentialsExpired() {
		return credentialsExpired;
	}

	public boolean isAccountExpired() {
		return accountExpired;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

}
