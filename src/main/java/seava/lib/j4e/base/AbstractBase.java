/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.lib.j4e.base;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.base.ISettings;

public abstract class AbstractBase implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	/**
	 * System configuration. May be null, use the getter.
	 */
	private ISettings settings;

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * Settings getter. If null, attempts to resolve it from spring context.
	 * 
	 * @return
	 */
	public ISettings getSettings() {
		if (this.settings == null) {
			this.settings = this.getApplicationContext().getBean(
					ISettings.class);
		}
		return settings;
	}

	/**
	 * Settings setter
	 * 
	 * @param settings
	 */
	public void setSettings(ISettings settings) {
		this.settings = settings;
	}
}
